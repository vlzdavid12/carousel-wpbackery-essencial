/* global jQuery, Slice */

/**
 * Slice Slider: Autosize Center View Addition.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker;

    /**
     * Requirement for all workers and runnables.
     */
    var requirements = {
        autosize: true,
        centerView: true
    };

    /**
     * Add workers and runnables for vertical autosize mode only.
     */
    factory.addRequirement(requirements);

    /**
     * Set min view size.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.minViewSize = stage.viewSize / 2;
    }, 20, '!fixedslides, !showFit');

    /**
     * Adjust slides coordinates.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        $.each(stage.coordinates, function (key) {
            stage.coordinates[key] += (stage.viewSize - cache.slidesSizes[stage.$slides.eq(key - 1)
                .data('sliceSizeIndex')]) / 2;
        });
    }, 86);

    /**
     * Calculate start and last slides.
     */
    addWorker('edges, slides, settings', function (stage, cache) {
        var spacing = stage.slideSpacing;
        stage.startSlide = fnSlice.fn.getFitCount(stage.viewSize / 2, cache.slidesSizes, stage.$slides.length, spacing);
        if (stage.startSlide > stage.slidesStep) {
            stage.startSlide -= stage.slidesStep;
        }
        if (stage.enoughSlides) {
            stage.coordinates[stage.startSlide] = -spacing;
        }
    }, 100, '!fixedslides, !loop, showFit');

    /**
     * Set slide position runnables.
     */
    factory.setRunnableType('setPosition');

    /**
     * Set slide position runnables.
     */
    factory.addRunnable('current, extraCurrent', function (stage, cache) {
        var startPos = cache.start,
            startCoords = stage.coordinates[cache.start] + stage.viewSize / 2,
            endPos = cache.start,
            endCoords = stage.coordinates[cache.start] - stage.viewSize / 2;
        while (startPos > 1 && stage.coordinates[startPos - 1] < startCoords) {
            --startPos;
        }
        while (stage.$slides.length > endPos && stage.coordinates[endPos + 1] > endCoords) {
            ++endPos;
        }
        if (startPos > 1 &&
            stage.coordinates[startPos] + stage.getSize(stage.$slides.eq(startPos - 1)) / 2 < startCoords) {
            --startPos;
        }
        if (stage.$slides.length > endPos &&
            stage.coordinates[endPos] - stage.getSize(stage.$slides.eq(endPos - 1)) / 2 > endCoords) {
            ++endPos;
        }
        cache.start = startPos;
        cache.end = endPos;
    }, 20, '!fixedslides');

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    factory.removeRequirement(requirements);
}));
