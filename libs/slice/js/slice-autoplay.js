/* global jQuery, Slice */

/**
 * Slice Slider: Autoplay Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Enable/disable autoplay
        autoplay: false,
        // Autoplay speed in milliseconds
        autoplaySpeed: 5000,
        // Enable/disable freeplay, i.e. scroll through all slides smoothly
        freeplay: false,
        // Pause autoplay when slider is focussed
        pauseOnFocus: false,
        // Pause autoplay on hover
        pauseOnHover: true,
        // Pause autoplay when a navigation (arrows/dots) is hovered
        pauseOnNavHover: true,
        // Enable/disable applying of slide status, i.e. current|active slides classes, etc.
        playLive: false,
        // Move in oposite direction.
        playReverse: false,
        // Go backwards when the boundary has reached, for no-loop sliders.
        playRewind: false,
        // Delay before moving in oposite direction.
        playRewindDelay: false,
        // Distance for autoplaySpeed to go, when freeplay is true
        playSpeedBase: 100,
        // Delay before starting autoplay.
        playStartDelay: false,
        // Freeplay number of frames per second, more slides more smoothly but heavier browser computing
        playfps: 60,
        // Extra delay for continuing autoplay after pauseOnNavHover, pauseOnFocus and pauseOnHover
        releaseDelay: 100,
        // Resume playing if user is inactive
        resumeOnInactive: true
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable,
        setRunnableType = factory.setRunnableType,
        setQuickRun = factory.setQuickRun,
        addRequirement = factory.addRequirement,
        removeRequirement = factory.removeRequirement;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'autoplay';

    /**
     * Autoplay plugin constructor.
     *
     * @class SliceAutoplay
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceAutoplay (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
        slice.state
            .add('playing', 'play')
            .add('paused', 'play, pauseplay, hold')
            .add('holdplay', 'pauseplay, hold')
            .add('forcerelease', true)
            .add('forcehold', 'pauseplay, hold');
    }

    /**
     * Setup settings.
     */
    SliceAutoplay.prototype.setup = function () {
        var slice = this.slice,
            settings = slice.settings,
            workers = settings.workers;
        workers.autoplay = !!settings.autoplay || !!settings.freeplay;
        if (workers.autoplay) {
            workers.pauseOnNavHover = !!settings.pauseOnNavHover;
            workers.playRewind = !!settings.playRewind;
            workers.playRewindDelay = !!settings.playRewindDelay;
            workers.resumeOnInactive = !!settings.resumeOnInactive;
            workers.playReverse = !!settings.playReverse;
            settings.autoplayEnd = settings.playReverse
                ? 'isFirstSlide'
                : 'isLastSlide';
            var autoplayTrigger = {
                fps: settings.playfps,
                repeat: true,
                repeatManualy: true,
                startManualy: true
            };
            autoplayTrigger.actions = [
                {
                    action: function () {
                        this.run('nextPlay');
                    }.bind(slice),
                    id: 'autoplay'
                }
            ];
            if (settings.freeplay) {
                workers.freeplay = true;
                settings.freeplayKeys = ['stageMoveFrom', 'final'];
                workers.playLive = !!settings.playLive;
                if (workers.playLive) {
                    settings.freeplayKeys.push('apply');
                }
                autoplayTrigger.speed = settings.playSpeedBase / settings.autoplaySpeed;
                autoplayTrigger.type = 'speed';
                autoplayTrigger.updateActions = [
                    {
                        action: function (trigger) {
                            if (trigger.state.is('playing')) {
                                this.run(this.settings.freeplayKeys, {
                                    distance: trigger.get('distancePass'),
                                    fromCoordinate: -this.stage.playFromCoordinate,
                                    reverse: this.settings.playReverse
                                }, 'move');
                            }
                        }.bind(slice),
                        id: 'freeplay'
                    }
                ];
            } else {
                autoplayTrigger.type = 'timer';
                autoplayTrigger.duration = settings.autoplaySpeed;
            }
            settings.autoplayTrigger = autoplayTrigger;
        }
    };

    /**
     * Setup autoplay.
     */
    addWorker('settings', function () {
        if (this.autoplay) {
            this.autoplay.remove();
        }
    }, 199, '!freeplay');

    /**
     * Add workers and runnables for autoplay mode only.
     */
    addRequirement(requirement);

    /**
     * Smooth move for rewind.
     */
    addWorker('settings', function () {
        this.settings.freeplayKeys.push('smooth');
    }, 0, 'freeplay, !loop, playRewind');

    /**
     * Set autoplay.
     */
    addWorker('settings', function () {
        this.run('apply', null, 'playInner');
    }, 199);

    /**
     * Restart timer for next slide.
     */
    addWorker('translated', function () {
        if (this.state.is('playing')) {
            this.run('update', null, 'playInner');
        }
    }, 190, 'freeplay');
    addWorker('translated', function () {
        if (this.state.is('playing')) {
            this.run('unforceHoldPlay');
        }
    }, 190);
    addWorker('translated', function () {
        if (this.state.is('playing')) {
            this.run('restartPlay');
        }
    }, 190, '!freeplay');
    addWorker('reset, move', function () {
        if (this.state.is('playing')) {
            this.run('forceHoldPlay');
        }
    }, 349);

    /**
     * Bind events to hold autoplay, for pauseOnHover and pauseOnFocus options.
     */
    addWorker('settings', function () {
        var context = this,
            settings = context.settings;

        /**
         * Release autoplay.
         */
        function releaseFn () {
            var slice = $(this).data('slice.slider');
            slice.run('releasePlay', {delay: slice.settings.releaseDelay});
        }

        /**
         * Hold autoplay.
         */
        function holdFn () {
            $(this).data('slice.slider')
                .run('holdPlay');
        }

        context.$element.off('.autoplay');
        if (settings.pauseOnHover) {
            context.$element.on({
                'mouseenter.slice.play': holdFn,
                'mouseleave.slice.play': releaseFn
            });
        }
        if (settings.pauseOnFocus) {
            context.$element.on({
                'focusin.slice.play': holdFn,
                'focusout.slice.play': releaseFn
            });
        }
    }, 190);

    /**
     * Bind events to hold autoplay when browser window is not active.
     */
    addWorker('settings', function () {
        var context = this,
            $document = $(document);
        if (context.autoplayVisibilityChange) {
            $document.off('visibilitychange.slice.play', context.autoplayVisibilityChange);
        } else {
            context.autoplayVisibilityChange = function () {
                if (document.hidden) {
                    this.run('forceHoldPlay');
                } else {
                    this.run('unforceHoldPlay');
                }
            }.bind(context);
        }
        $document.on('visibilitychange.slice.play', context.autoplayVisibilityChange);
        context.autoplayVisibilityChange();
    }, 199, 'visibilityChange');

    /**
     * Bind events to hold autoplay when pauseOnNavHover is enabled.
     */
    addWorker('slides, settings', function (stage) {
        var $nav = stage.$nav || $([]);

        /**
         * Release autoplay.
         */
        function releaseFn () {
            var slice = $(this).data('slice.slider');
            slice.run('releasePlay', {delay: slice.settings.releaseDelay});
        }

        /**
         * Hold autoplay.
         */
        function holdFn () {
            $(this).data('slice.slider')
                .run('holdPlay');
        }

        $nav.off('.autoplay');
        $nav.on({
            'mouseenter.slice.play': holdFn,
            'mouseleave.slice.play': releaseFn
        });
    }, 190, 'pauseOnNavHover');

    /**
     * Autoplay inner functionality.
     */
    setRunnableType('playInner');

    /**
     * Set autoplay trigger options.
     */
    addRunnable('update', function (stage, cache) {
        cache.autoplayTrigger = {};
    }, 0);
    addRunnable('apply', function (stage, cache) {
        cache.autoplayTrigger = this.settings.autoplayTrigger;
    }, 0);

    /**
     * Set positions.
     */
    addRunnable('apply, update', function (stage, cache) {
        cache.distanceData = {
            from: stage.startSlide,
            to: stage.lastSlide
        };
        cache.playStartPosition = stage.startSlide;
        cache.playEndPosition = stage.lastSlide;
    }, 0, 'freeplay');
    addRunnable('apply, update', function (stage, cache) {
        cache.distanceData.to = stage.slidesCount + stage.startSlide;
    }, 10, 'freeplay, loop');
    addRunnable('apply, update', function (stage, cache) {
        cache.distanceData.asCurrentFrom = true;
        cache.distanceData.asCurrentTo = true;
    }, 0, 'freeplay, playLive');
    addRunnable('apply, update', function (stage, cache) {
        cache.distanceData.checkFrom = true;
        cache.distanceData.checkTo = true;
    }, 0, 'freeplay, !playLive');

    /**
     * Set coordinates.
     */
    addRunnable('apply, update', function (stage, cache) {
        cache.distance = this.run('from, to', cache.distanceData, 'getDistance').distance;
    }, 20, 'freeplay');

    /**
     * Update playFromCoordinate.
     */
    addRunnable('apply, update', function (stage, cache) {
        cache.playFromCoordinate = stage.hasOwnProperty('playFromCoordinate')
            ? stage.playFromCoordinate
            : null;
    }, 30, 'freeplay');
    addRunnable('apply, update', function (stage, cache) {
        stage.playFromCoordinate = this.run('getSlideCoordinate', {
            position: cache.distanceData.from
        });
    }, 30, 'freeplay, !playReverse');
    addRunnable('apply, update', function (stage, cache) {
        stage.playFromCoordinate = this.run('getSlideCoordinate', {
            position: cache.distanceData.to
        });
    }, 30, 'freeplay, playReverse');

    /**
     * Apply: add autoplay or update it.
     */
    addRunnable('apply', function (stage, cache) {
        var context = this,
            autoplayTrigger = cache.autoplayTrigger;
        autoplayTrigger.distance = cache.distance;
        if (context.autoplayType) {
            if (context.autoplayType === autoplayTrigger.type) {
                context.autoplay.setOptions(autoplayTrigger);
                stage.autoplayAction = 'checkPlay';
                return;
            }
            context.autoplay.remove();
        }
        context.autoplayType = autoplayTrigger.type;
        context.autoplay = Slice.Trigger.create(autoplayTrigger, autoplayTrigger.type);
        cache.autoplayAction = 'startPlay';
    }, 40);

    /**
     * Update autoplay distance.
     */
    addRunnable('update', function (stage, cache) {
        var autoplayTrigger = this.settings.autoplayTrigger;
        if (cache.distance !== autoplayTrigger.distance) {
            autoplayTrigger.distance = cache.distance;
            cache.autoplayTrigger.distance = cache.distance;
            cache.update = true;
        }
    }, 40, 'freeplay');

    addRunnable('apply, update, distancepass', function (stage) {
        var distance = this.run('fromCoordinate', {
            fromCoordinate: -stage.playFromCoordinate,
            reverse: this.settings.playReverse
        }, 'getDistance');
        this.autoplay.setPassDistance(distance.distance);
    }, 40, 'freeplay');

    /**
     * Start autoplay.
     */
    addRunnable('apply', function (stage, cache) {
        var context = this,
            settings = context.settings;
        stage.playNext = 'get' +
            (settings.playReverse
                ? 'Prev'
                : 'Next') +
            (settings.playRewind
                ? 'Rewind'
                : 'Strict');
        if (cache.autoplayAction) {
            context.run(cache.autoplayAction);
        }
    }, 50);

    /**
     * Update autoplay.
     */
    addRunnable('update', function (stage, cache) {
        if (cache.update) {
            this.autoplay.setOptions(cache.autoplayTrigger);
        }
    }, 50);

    /**
     * Autoplay actions runnables.
     */
    setRunnableType('playAction');

    // Release play, used for hover specific events.
    setQuickRun('releasePlay', 'release, check');
    // Hold play, used for hover specific events.
    setQuickRun('holdPlay', 'hold, check');
    // Force release, even if hold.
    setQuickRun('forceReleasePlay', 'forcerelease, check');
    // Stop force release.
    setQuickRun('unboundReleasePlay', 'unboundrelease, check');
    // Force hold, hold play in any way even forcerelease.
    setQuickRun('forceHoldPlay', 'forcehold, check');
    // Stop force hold.
    setQuickRun('unforceHoldPlay', 'unforcehold, check');
    // Restart play.
    setQuickRun('restartPlay', 'restart, canStart');
    // Pause play.
    setQuickRun('pausePlay', 'pause, check');
    // Continue play.
    setQuickRun('continuePlay', 'continue, check');
    // Pause/continue play.
    setQuickRun('togglePlay', 'toggle, check');
    // Check if can play.
    setQuickRun('checkPlay', 'check');
    // Start play.
    setQuickRun('startPlay', 'start, canStart');
    // Stop play.
    setQuickRun('stopPlay', 'stop');
    // Reset play.
    setQuickRun('resetPlay', 'reset, canStart');
    // Go to next slide.
    setQuickRun('nextPlay', 'next, check');

    /**
     * Do nothing if not playable.
     */
    addRunnable('check, stop, restart, reset', function () {
        if (!this.state.is('play')) {
            return false;
        }
    }, -100);

    /**
     * Stop autoplay reached end, for non loop and non play rewind sliders.
     */
    addRunnable('check, canStart', function (stage) {
        if (stage[this.settings.autoplayEnd]) {
            this.run('stopPlay');
            return false;
        }
    }, -100, '!loop, !playRewind');

    /**
     * Update delay for start.
     */
    addRunnable('canStart', function (stage, cache) {
        if (stage[this.settings.autoplayEnd]) {
            cache.startOptions = {
                delay: this.settings.playRewindDelay,
                duration: 0
            };
        }
    }, -100, '!loop, playRewind, playRewindDelay');

    /**
     * Get next slide position.
     */
    addRunnable('next', function (stage, cache) {
        cache.moveToSlide = this.run(stage.playNext);
    }, 0, '!freeplay');
    addRunnable('next', function (stage, cache) {
        if (!cache.list.restart) {
            this.run('restartPlay');
        }
    }, 0, 'freeplay');

    /**
     * Manage release/holdplay.
     */
    addRunnable('release', function () {
        this.state.leave('holdplay');
    });
    addRunnable('hold', function () {
        this.state.enter('holdplay');
    });

    /**
     * Manage force hold/release.
     */
    addRunnable('forcerelease', function () {
        this.state.enter('forcerelease');
    });
    addRunnable('unboundrelease', function () {
        this.state.exit('forcerelease');
    });
    addRunnable('forcehold', function () {
        this.state.enter('forcehold');
    });
    addRunnable('unforcehold', function () {
        this.state.leave('forcehold');
    });

    /**
     * Restart autoplay.
     */
    addRunnable('restart', function (tage, cache) {
        this.autoplay.repeat(cache.startOptions);
    });

    /**
     * If already started than do nothing more.
     */
    addRunnable('start', function (stage, cache) {
        if (this.state.is('playing')) {
            return false;
        }
        cache.startOptions = $.extend(cache.startOptions || {}, {
            delay: this.settings.playStartDelay || 0
        });
    });

    /**
     * Manage start/stop/reset.
     */
    addRunnable('stop', function () {
        this.state.exit('playing');
    });
    addRunnable('reset, start, stop', function () {
        this.state.exit('paused');
    });
    addRunnable('reset', function (stage, cache) {
        this.autoplay.reset(cache.startOptions);
    });
    addRunnable('stop', function () {
        this.autoplay.stop();
        this.trigger('stop');
    });
    addRunnable('start', function (stage, cache) {
        this.state.enter('playing');
        this.trigger('play');
        this.autoplay.start(cache.startOptions);
    });

    /**
     * Manage pause/continue/toggle.
     */
    addRunnable('pause', function (stage, cache) {
        cache.play = false;
    });
    addRunnable('continue', function (stage, cache) {
        cache.play = true;
    });
    addRunnable('toggle, pause, continue', function (stage, cache) {
        var context = this,
            play = Slice.hasProperty(cache, 'play')
                ? !!cache.play
                : context.state.is('paused');
        if (context.state.is('playing') === play) {
            return;
        }
        if (play) {
            context.state
                .exit('paused')
                .enter('playing');
            context.trigger('continue');
        } else {
            context.state
                .exit('playing')
                .enter('paused');
            context.trigger('pause');
        }
    }, 100);

    /**
     * Check start/stop tick execution.
     */
    addRunnable('check, canStart', function (stage, cache) {
        var context = this;
        if (context.state.is('forcehold') || context.state.is('pauseplay') && context.state.not('forcerelease')) {
            context.autoplay.pause();
        } else {
            context.autoplay.continue(cache.delay);
        }
    }, 200);

    /**
     * Go to next slide.
     */
    addRunnable('next', function (stage, cache) {
        this.to(cache.moveToSlide);
    }, 300, '!freeplay');

    /**
     * Move stage runnables.
     */
    setRunnableType('move');

    /**
     * Check if speed is set.
     */
    addRunnable(true, function (stage, cache) {
        var context = this;
        if (context.state.is('playing') && cache.speed) {
            context.run('forceHoldPlay');
            var afterMove = typeof cache.afterMove === 'function'
                ? cache.afterMove
                : false;
            cache.afterMove = function () {
                context.run('unforceHoldPlay');
                if (afterMove) {
                    afterMove();
                }
            };
        }
    }, 91, 'freeplay');

    /**
     * User actions runnables.
     */
    setRunnableType('useraction');

    /**
     * Resume on inactive.
     */
    addRunnable('inactive', function () {
        if (this.state.is('holdplay')) {
            this.run('forceReleasePlay');
        }
    }, 0, 'resumeOnInactive');

    /**
     * Hold on inactive.
     */
    addRunnable('active', function () {
        if (this.state.is('forcerelease')) {
            this.run('unboundReleasePlay');
        }
    }, 0, 'resumeOnInactive');

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceAutoplay = SliceAutoplay;
}));
