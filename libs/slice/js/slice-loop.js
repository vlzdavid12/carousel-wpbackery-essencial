/* global jQuery, Slice */

/**
 * Slice Slider: Loop Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Create loop even when not enought slides.
        forceLoop: false,
        // Enable/disable infinite looping.
        loop: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable,
        setRunnableType = factory.setRunnableType,
        addRequirement = factory.addRequirement,
        removeRequirement = factory.removeRequirement;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'loop';

    /**
     * Requirements for some cases.
     */
    var tempRequirement;

    /**
     * Loop plugin constructor.
     *
     * @class SliceLoop
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceLoop (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceLoop.prototype.setup = function () {
        var context = this,
            settings = context.slice.settings;
        if (settings.loop) {
            settings.workers.loop = true;
            settings.workers.checkLoop = true;
            settings.workers.forceLoop = !!settings.forceLoop;
            settings.workers.rewind = false;
            settings.workers.checkSides = false;
        }
    };

    /**
     * Get slide (non cloned) slide position.
     *
     * @param {number} position - Position.
     * @param {number} slidesCount - Real slides count position.
     * @param {number} clonesCount - Clones count position.
     * @param {number} start - Real slides start position.
     * @param {number} end - Real slides end position.
     * @returns {number} - Slide position.
     */
    function getSlidePosition (position, slidesCount, clonesCount, start, end) {
        var slidePosition = position,
            resetPosition = false;
        if (slidePosition < start) {
            resetPosition = slidePosition + (slidesCount - clonesCount);
        } else if (slidePosition > end) {
            resetPosition = slidePosition - (clonesCount + slidesCount);
        }
        if (resetPosition) {
            resetPosition %= slidesCount;
            if (resetPosition === 0) {
                resetPosition = slidesCount;
            }
            slidePosition = resetPosition;
        } else {
            slidePosition -= clonesCount;
        }
        return slidePosition;
    }

    /**
     * Clone slides.
     *
     * @param {jQuery} $slides - Slides.
     * @param {number} start - Start position.
     * @param {number} end - End position.
     * @returns {jQuery} - Cloned slides.
     */
    function cloneSlides ($slides, start, end) {
        return $slides.slice(start, end).clone(true, true)
            .addClass('slide-clone');
    }

    /**
     * Add workers.
     *
     * Cleanup cloned slides.
     */
    addWorker('settings', function () {
        this.$stage.find('.slide-clone').remove();
    }, 0, '!loop');

    /**
     * Check if there is enought slides for loop and enabled or disabled it.
     */
    addWorker('size, slides, settings', function (stage) {
        var settings = this.settings;
        if (stage.enoughSlides !== settings.workers.loop) {
            settings.workers.loop = !!stage.enoughSlides;
            settings.workers.checkSides = !stage.enoughSlides;
            this.hireManager(settings.workers);
            return 're-work';
        }
    }, 50, 'checkLoop, !forceLoop');

    /**
     * Add workers and runnables for adaptive mode only.
     */
    addRequirement(requirement);

    /**
     * Store original slides.
     * Force to re-render cloned slides.
     */
    addWorker('slides, settings', function (stage, cache) {
        stage.$originalSlides = stage.$slides;
        cache.forceClone = true;
    }, 40);

    /**
     * Add workers only for fixed slide size.
     */
    tempRequirement = 'fixedsize';
    addRequirement(tempRequirement);

    /**
     * Calculate clones ammount.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.clonesCount = stage.viewportSize / cache.slideSize;
    }, 40);

    /**
     * Force loop if there is not enought slides.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        if (!stage.slidesCount) {
            var settings = this.settings;
            // If not enought slides - disable loop and re-update.
            settings.workers.loop = false;
            settings.workers.checkSides = true;
            this.hireManager(settings.workers);
            return 're-update';
        }
        if (!stage.enoughSlides) {
            cache.clonesCount += stage.slidesCount - stage.minSlides;
        }
    }, 50, 'forceLoop');

    /**
     * Normalize cloned slides ammount.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        stage.clonesCount = Math.ceil(cache.clonesCount) + 1;
    }, 55);
    removeRequirement(tempRequirement);

    /**
     * Check and clone slides.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        var context = this,
            $stage = context.$stage;
        // Check if need to create clones.
        if (!cache.forceClone && context.$stage.find('.slide-clone').length === stage.clonesCount * 2) {
            return;
        }
        var $originalSlides = stage.$originalSlides;
        // If somehow there is infinite number of clones set it to the number of slides.
        if (!isFinite(stage.clonesCount)) {
            stage.clonesCount = $originalSlides.length;
        }
        stage.enoughSlides = true;
        stage.clonesStart = stage.clonesCount + 1;
        stage.clonesEnd = stage.slidesCount + stage.clonesCount;
        context.$stage.find('.slide-clone').remove();
        // Add prepended clones
        var $slides = $([]),
            count = stage.clonesCount;
        while (count > stage.slidesCount) {
            $slides = $slides.add(cloneSlides($originalSlides, 0, stage.slidesCount));
            count -= stage.slidesCount;
        }
        $slides = cloneSlides($originalSlides, -count).add($slides);
        $slides.each(function (i, el) {
            $(el).attr('data-slice-index', i - stage.clonesCount + 1);
        });
        $stage.prepend($slides);
        // Add original slides
        stage.$slides = $slides.add($originalSlides);
        // Add appended clones
        $slides = $([]);
        count = stage.clonesCount;
        while (count > stage.slidesCount) {
            $slides = $slides.add(cloneSlides($originalSlides, 0, stage.slidesCount));
            count -= stage.slidesCount;
        }
        $slides = $slides.add(cloneSlides($originalSlides, 0, count));
        $slides.each(function (i, el) {
            $(el).attr('data-slice-index', i + stage.slidesCount + 1);
        });
        $stage.append($slides);
        stage.$slides = stage.$slides.add($slides);
    }, 56);

    /**
     * After translated, reset to non-cloned slide position if needed.
     */
    addWorker('translated', function (stage) {
        if (stage.resetPosition) {
            var realPosition = this.currentPosition;
            this.currentPosition = stage.fakePosition;
            this.reset(realPosition);
        }
    }, 190);

    /**
     * Adjust from slide position.
     */
    addWorker('reset, move', function (stage, cache) {
        cache.fromPosition += stage.clonesCount;
    }, 220);

    /**
     * Get slide position runnables.
     */
    setRunnableType('position');

    /**
     * Cache base position.
     */
    addRunnable(true, function (stage, cache) {
        if (!Slice.hasProperty(cache, 'position')) {
            if (stage.resetPosition) {
                cache.position = stage.fakePosition;
            } else {
                cache.position = this.currentPosition;
            }
        }
    });

    /**
     * Calculate slide position.
     */
    addRunnable(true, function (stage, cache) {
        var clonedPosition = cache.isReal
            ? cache.position
            : stage.clonesCount + cache.position;
        cache.position = getSlidePosition(
            clonedPosition, stage.slidesCount, stage.clonesCount,
            stage.clonesStart, stage.clonesEnd
        );
        cache.clonedDiff = clonedPosition - cache.position;
    }, 10);

    /**
     * Adjust real and current position.
     */
    addRunnable('real, current', function (stage, cache) {
        cache.real += cache.clonedDiff;
        cache.current += cache.clonedDiff;
    }, 71);

    /**
     * Adjust normalized position.
     */
    addRunnable('normalize', function (stage, cache) {
        cache.normalized += cache.clonedDiff - stage.clonesCount;
    }, 71);

    /**
     * Get previous/next slide position runnables.
     */
    setRunnableType('movePosition');

    /**
     * Store cloned difference.
     */
    addRunnable(true, function (stage, cache) {
        cache.clonedDiff = cache.positionData.clonedDiff;
    });

    /**
     * Check from slide position.
     */
    addRunnable('next', function (stage, cache) {
        if (cache.from >= stage.lastSlide) {
            cache.to = stage.startSlide;
            cache.clonedDiff += stage.slidesCount;
        }
    }, 30);
    addRunnable('prev', function (stage, cache) {
        if (cache.from <= stage.startSlide) {
            cache.to = stage.lastSlide;
            cache.clonedDiff -= stage.slidesCount;
        }
    }, 30);

    /**
     * Normalize to slide position.
     */
    addRunnable(true, function (stage, cache) {
        var clonedPosition = stage.clonesCount + cache.to;
        cache.to = getSlidePosition(
            clonedPosition, stage.slidesCount, stage.clonesCount,
            stage.clonesStart, stage.clonesEnd
        );
        cache.to += cache.clonedDiff - stage.clonesCount;
    }, 30);

    /**
     * Move stage runnables.
     */
    factory.setRunnableType('move');

    /**
     * Adjust positions.
     */
    addRunnable('apply', function (stage, cache) {
        cache.at += stage.clonesCount;
        cache.to += stage.clonesCount;
    }, 45);

    /**
     * Make sure current slide is styled.
     * Normalize move distance.
     */
    addRunnable(true, function (stage, cache) {
        // Do nothing if current slide will be applied anyway
        if (cache.list.apply) {
            return;
        }
        var currentPosition = this.run('getPosition'),
            position = stage.clonesCount + currentPosition,
            checkPos = [
                position,
                position - stage.slidesCount,
                stage.clonesEnd + currentPosition
            ];
        for (var i = 0; i < checkPos.length; i++) {
            if (this.run('currentIsVisible', {
                coordinate: cache.moveCoordinate,
                isReal: true,
                position: checkPos[i]
            })) {
                if (cache.applyPosition) {
                    // When some clones are visible, make tham look as current slide.
                    if (this.run('setExtraCurrent', {
                        isReal: true,
                        position: checkPos[i]
                    }).applied && checkPos[i] < stage.clonesCount + currentPosition) {
                        cache.inLoop = true;
                        cache.moveCoordinate -= stage.currentDisplacement;
                        stage.fixExtraDistance = stage.currentDisplacement;
                    }
                } else {
                    cache.applyPosition = checkPos[i];
                    if (this.settings.workers.single) {
                        break;
                    }
                }
            }
        }
        if (cache.applyPosition) {
            if (stage.resetPosition) {
                currentPosition = stage.fakePosition;
            }
            currentPosition += stage.clonesCount;
            // If current slide is not the one is visible - set visible as current
            if (cache.applyPosition !== currentPosition) {
                cache.currentData = {
                    isReal: true,
                    position: cache.applyPosition
                };
            }
        }
    }, 69);

    /**
     * If it is final - normalize move distance and current slide.
     */
    addRunnable(true, function (stage, cache) {
        var position, current;
        // If current slide is clone than set position to normalize or move distance exceeds boundaries.
        if (cache.currentData) {
            current = cache.currentData.position +
                (cache.currentData.isReal
                    ? 0
                    : stage.clonesCount);
            if (current < stage.clonesStart || current > stage.clonesEnd) {
                position = current;
            }
        }
        if (!position) {
            var startCoord = stage.coordinates[stage.clonesStart] + stage.viewportSize,
                endCoord = stage.coordinates[stage.clonesEnd + 1] - stage.viewportSize,
                coord = cache.moveCoordinate,
                len;
            if (startCoord < coord || endCoord > coord) {
                position = startCoord < coord
                    ? 1
                    : stage.clonesEnd;
                len = stage.$slides.length;
                for (; position <= len && stage.coordinates[position] > coord; position++) {
                    // All stuff is done at for condition
                }
            }
        }

        // If position needs normalization - do normalization and normalize move distance
        if (position) {
            cache.inLoop = true;
            var real = stage.clonesCount +
                    this.run('getPosition', {
                        isReal: true,
                        position: position});
            if (current) {
                cache.currentData.position = real;
                cache.currentData.isReal = true;
                if (cache.currentData.from) {
                    cache.currentData.from = real - current + cache.currentData.from +
                        (cache.currentData.fromReal
                            ? 0
                            : stage.clonesCount);
                    cache.currentData.fromReal = true;
                }
            }
            if (cache.list.apply) {
                cache.moveCoordinate -= stage.coordinates[position] - stage.coordinates[real];
            } else {
                var realCoordinate = {
                        isReal: true,
                        position: real
                    },
                    positionCoordinate = {
                        checkPosition: true,
                        isReal: true,
                        position: position
                    };
                realCoordinate = this.run('getSlideCoordinate', realCoordinate);
                positionCoordinate = this.run('getSlideCoordinate', positionCoordinate);
                cache.moveCoordinate -= positionCoordinate - realCoordinate;
                if (stage.fixExtraDistance) {
                    cache.moveCoordinate += stage.fixExtraDistance;
                    if (cache.currentData) {
                        cache.currentData.force = true;
                    }
                }
            }
        }
    }, 69);

    /**
     * Normalize current coordinate and distance.
     */
    addRunnable(true, function (stage, cache) {
        if (cache.inLoop) {
            var newDistance = cache.currentCoord - cache.moveCoordinate;
            if (Math.abs(cache.moveDistance) > Math.abs(newDistance)) {
                cache.moveDistance = newDistance;
            }
            cache.currentCoord = cache.moveCoordinate + cache.moveDistance;
        }
    }, 69);

    /**
     * Calculate speed runnables.
     * Adjust from and to positions.
     */
    setRunnableType('speed');
    addRunnable('from', function (stage, cache) {
        if (!cache.fromReal) {
            cache.from += stage.clonesCount;
        }
    }, 5);
    addRunnable('to', function (stage, cache) {
        if (!cache.toReal) {
            cache.to += stage.clonesCount;
        }
    }, 5);

    /**
     * Set slide position runnables.
     */
    setRunnableType('setPosition');

    /**
     * Adjust current positions.
     */
    addRunnable('current', function (stage, cache) {
        if (stage.resetPosition) {
            cache.currentPosition = stage.fakePosition;
        }
        cache.currentPosition += stage.clonesCount;
    });

    /**
     * Check if position will need reset.
     * Reset extra distance.
     */
    addRunnable('current', function (stage, cache) {
        stage.resetPosition = cache.real < stage.clonesStart || cache.real > stage.clonesEnd;
        if (stage.resetPosition) {
            stage.fakePosition = cache.real - stage.clonesCount;
        }
        stage.fixExtraDistance = 0;
    }, 10);

    /**
     * Check slide visibility runnables.
     * Adjust position.
     */
    setRunnableType('slideVisibility');
    addRunnable('position', function (stage, cache) {
        if (!cache.isReal) {
            cache.position += stage.clonesCount;
        }
        cache.current += stage.clonesCount;
    }, 5);

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceLoop = SliceLoop;
}));
