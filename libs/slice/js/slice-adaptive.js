/* global jQuery, Slice */

/**
 * Slice Slider: Adaptive Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }

    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Adaptive mode slider class
        adaptiveClass: 'slice-slider-adaptive',
        // Adaptive height animation easing function
        adaptiveEasing: 'swing',
        // Enable/disable adaptive height
        adaptiveHeight: false,
        // Resize only on current slide position change
        adaptiveStrict: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'adaptiveHeight';

    /**
     * Adaptive plugin constructor.
     *
     * @class SliceAdaptive
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceAdaptive (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceAdaptive.prototype.setup = function () {
        var settings = this.slice.settings,
            workers = settings.workers;
        this.slice.$element
            .toggleClass(settings.adaptiveClass, !!settings.adaptiveHeight);
        if (settings.adaptiveHeight) {
            workers.adaptiveHeight = true;
            workers.staticHeight = false;
            workers.checkheight = true;
            workers.adaptiveStrict = !!settings.adaptiveStrict;
            if (workers.adaptiveStrict) {
                settings.adaptiveRunnable = 'getActiveHeight';
                settings.adaptiveMoveRunnable = 'getActiveHeight';
            } else {
                settings.adaptiveRunnable = 'getVisibleHeight';
                settings.adaptiveMoveRunnable = 'getVisibleHeightLive';
            }
        }
    };

    /**
     * Add workers and runnables for adaptive mode only.
     */
    factory.addRequirement(requirement);

    /**
     * Store height, for autoadaptive.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        if (stage.getPosition) {
            cache.previousHeight = parseFloat(this.$viewport.css(fnSlice.cssProps.height));
        }
    }, -101, 'autoadaptive');

    /**
     * When in vertical mode enable autoadaptive
     * and turn off fixedsize, and request all workers rebuild.
     */
    addWorker(true, function (stage, cache) {
        cache.rebuild = true;
        var workers = this.settings.workers;
        workers.autosize = true;
        workers.fixedsize = false;
        workers.fixedslides = true;
        workers.autoadaptive = true;
        workers.fixedview = false;
    }, -10, '!autoadaptive, vertical');

    /**
     * Set runnables types for vertical non strict mode.
     */
    addWorker(true, function () {
        this.settings.adaptiveRunnable = 'getActiveHeight';
        this.settings.adaptiveMoveRunnable = 'getInViewHeightLive';
    }, -10, '!adaptiveStrict, !autoadaptive, vertical');

    /**
     * Set previous height, for autoadaptive.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        if (this.moveToPosition !== false && Slice.hasProperty(cache, 'previousHeight')) {
            this.$viewport.css(fnSlice.cssProps.height, cache.previousHeight);
        }
    }, 199, 'autoadaptive');

    /**
     * Adapt slider height to the height of active slides.
     */
    addWorker('reset, move', function (stage, cache) {
        var context = this,
            speed = cache.moveSpeed || 0,
            animate = speed > 0,
            animateProps = {},
            height = context.run(this.settings.adaptiveRunnable, {coordinate: cache.moveToCoordinate}),
            postponed = new Slice.Postponed();
        cache.animation.add(postponed);
        animateProps[fnSlice.cssProps.height] = height;
        stage.adaptiveHeight = Math.ceil(height);
        postponed.fail(function () {
            context.$viewport
                .stop(true)
                .css(animateProps);
        });
        postponed.add(function () {
            if (animate) {
                context.$viewport.stop(true)
                    .animate(
                        animateProps, speed, context.settings.adaptiveEasing,
                        function () {
                            postponed.resolve();
                        }
                    );
            } else {
                context.$viewport.css(animateProps);
                postponed.resolve();
            }
            return postponed;
        });
    }, 349);

    /**
     * Get size runnables.
     * When getting slider height, return height of in view slides.
     */
    factory.setRunnableType('size');
    factory.addRunnable('viewHeight', function (stage, cache) {
        cache.result = this.run(this.settings.adaptiveRunnable);
        return false;
    }, -1);

    /**
     * Set move runnables.
     */
    factory.setRunnableType('move');

    /**
     * Adapt slider height to the height of visible.
     */
    factory.addRunnable(true, function (stage, cache) {
        var context = this,
            height = context.run(this.settings.adaptiveMoveRunnable, {coordinate: cache.moveCoordinate}),
            intHeight = Math.ceil(height);
        if (stage.adaptiveHeight !== intHeight) {
            if (!Slice.hasProperty(cache, 'speed')) {
                var current = parseInt(context.$viewport.css(fnSlice.cssProps.height)),
                    diff = Math.abs(height - current);
                if (diff > 5) {
                    if (intHeight > stage.adaptiveHeight === intHeight > current) {
                        diff = Math.abs(height - stage.adaptiveHeight);
                    } else if (Math.abs(height - stage.adaptiveHeight) > 10) {
                        context.$viewport.stop(true);
                    } else {
                        context.$viewport.clearQueue();
                    }
                    if (diff < this.settings.speed) {
                        diff *= 10;
                    }
                    cache.speed = Math.min(diff, this.settings.speed);
                }
            }
            var styles = {};
            stage.adaptiveHeight = intHeight;
            styles[fnSlice.cssProps.height] = height;
            if (cache.speed && cache.speed > 0) {
                context.$viewport.animate(styles, cache.speed, context.settings.adaptiveEasing);
            } else {
                context.$viewport.stop(true).css(styles);
            }
        }
    }, 95);

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    factory.removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceAdaptive = SliceAdaptive;
}));
