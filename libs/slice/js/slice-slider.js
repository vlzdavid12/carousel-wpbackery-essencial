/* global jQuery, Slice */

/**
 * Slice Slider.
 * Slider core script.
 *
 * @author shininglab
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Slice slider default options.
     */
    var sliceDefaults = {
        // Active class.
        activeClass: 'slice-active',
        // Current class.
        currentClass: 'slice-current',
        // Disabled class.
        disabledClass: 'slice-disabled',
        // Slide change animation easing function.
        fallbackEasing: 'swing',
        // Enable/disable on click slide change.
        focusOnSelect: false,
        // Hidden class.
        hiddenClass: 'slice-hidden',
        // Maximal move animation speed.
        maxSpeed: false,
        // Minimal move animation speed.
        minSpeed: 200,
        // Not enought slides class.
        notEnoughtSlidesClass: 'slice-not-enought-slides',
        // Go backwards when the boundary has reached.
        rewind: false,
        // Slide class.
        slideClass: 'slice-slide',
        // Slide element.
        slideElement: 'div',
        // Slider root element class.
        sliderClass: false,
        // Slide animation move or fade.
        slidingAnimation: 'move',
        // Slide animation speed.
        slidingSpeed: false,
        // Slides spacing, allowed units: %, px.
        spacing: false,
        // Slide move animation speed.
        speed: 500,
        // Distance in pixels, that will be taken with speedType = 'uniform' or 'uniformfixed'.
        speedBase: 100,
        // Speed type, i.e. base distance to calculate speed, check available types in documentation.
        speedType: 'slide',
        // Stage class.
        stageClass: 'slice-stage',
        // Stage element.
        stageElement: 'div',
        // Enable/disable setting slider height based on maximum slide height.
        staticHeight: true,
        // Slider theme.
        theme: false,
        // Class to be added when user do nothing.
        userInactiveClass: 'slice-user-inactive',
        // Timeout to add userInactiveClass to slider.
        userInactiveDelay: 5000,
        // Viewport layer index.
        viewLayer: 1,
        // Viewport layer class.
        viewLayerClass: 'slice-view-layer',
        // Viewport class.
        viewportClass: 'slice-viewport',
        // Ignores requests to advance the slide position while animating.
        waitForAnimate: true
    };

    /**
     * List of CSS properties.
     */
    var cssProps = {
        height: 'height',
        width: 'width'
    };

    /**
     * Add 'destroyed' event, that will be triggered when element is removed.
     */
    if (!$.event.special.destroyed) {
        $.event.special.destroyed = {
            remove: function (o) {
                if (o.handler && o.type !== 'destroyed') {
                    o.handler();
                }
            }
        };
    }

    /**
     * Add 'windowresize' event, to quick handle window resize on elements.
     */
    if (!$.event.special.windowresize) {
        $(window)
            .data('slice__windowresize', jQuery([]))
            .on('resize load windowresize', function (e) {
                if (e.type === 'windowresize' && e.target !== this) {
                    return;
                }
                var $win = $(window),
                    $elements = $win.data('slice__windowresize');
                if ($elements.length) {
                    var data = {
                        height: $win.height(),
                        width: $win.width()
                    };
                    $elements.each(function (i, el) {
                        $(el).triggerHandler('windowresize', [data]);
                    });
                }
            });
        $.event.special.windowresize = {
            setup: function () {
                $(window).data('slice__windowresize', $(window).data('slice__windowresize')
                    .add(this));
            },
            teardown: function () {
                $(window).data('slice__windowresize', $(window).data('slice__windowresize')
                    .not(this));
            }
        };
    }

    /**
     * Add 'contentresize' event, to quick handle content resize on elements.
     */
    if (!$.event.special.contentresize) {
        $(window)
            .data('slice__contentresize', jQuery([]))
            .on('resize load contentresize', function (e) {
                if (e.type === 'contentresize' && e.target !== this) {
                    return;
                }
                var $win = $(window),
                    $elements = $win.data('slice__contentresize');
                if ($elements.length) {
                    var data = {
                        height: $win.height(),
                        width: $win.width()
                    };
                    $elements.each(function (i, el) {
                        $(el).triggerHandler('contentresize', [data]);
                    });
                }
            });
        $.event.special.contentresize = {
            setup: function () {
                $(window).data('slice__contentresize', $(window).data('slice__contentresize')
                    .add(this));
            },
            teardown: function () {
                $(window).data('slice__contentresize', $(window).data('slice__contentresize')
                    .not(this));
            }
        };
    }

    /**
     * Themes.
     */
    var themes = {};

    /**
     * Create theme options object.
     *
     * @param {string} name - Theme name.
     * @param {object} options - Theme options.
     * @returns {object} Theme options.
     */
    function createThemeOptions (name, options) {
        return $.extend(options, {
            class: options.hasOwnProperty('class')
                ? options.class
                : 'slice-theme-' + name,
            defaults: options.defaults || false,
            name: name,
            setup: typeof options.setup === 'function'
                ? options.setup
                : false
        });
    }

    /**
     * Add theme.
     *
     * @param {string} name - Theme name.
     * @param {object} options - Theme options.
     */
    function addTheme (name, options) {
        if (themes[name]) {
            if (window.console) {
                console.warn('Slice: \'' + name + '\' theme already exists.');
            }
            return;
        }
        themes[name] = createThemeOptions(name, options);
    }

    /**
     * Remove theme.
     *
     * @param {string} name - Theme name.
     */
    function removeTheme (name) {
        if (themes.hasOwnProperty(name)) {
            delete themes[name];
        }
    }

    /**
     * Get theme.
     *
     * @param {string} name - Theme name.
     * @returns {object} Theme options.
     */
    function getTheme (name) {
        return themes[name] || false;
    }

    /**
     * Parse options.
     *
     * @param {string|object} val - Options string or object.
     * @returns {object} Options object.
     */
    function parseOptions (val) {
        var valType = typeof val,
            ret = {};
        if (valType === 'string') {
            $.each(val.split(';'), function (i, str) {
                var keyVal = str.split(':'),
                    key = keyVal[0].trim();
                if (!key) {
                    return;
                }
                if (keyVal.length === 1) {
                    ret[key] = true;
                } else {
                    var rawVal = keyVal.slice(1).join(':');
                    try {
                        ret[key] = JSON.parse(rawVal);
                    } catch (e) {
                        ret[key] = rawVal.trim();
                    }
                }
            });
        } else if (valType === 'object') {
            return val;
        }
        return ret;
    }

    /**
     * Slice slider constructor.
     * Creates a slider.
     *
     * @class SliceSlider
     * @param {HTMLElement|jQuery} element - The element to create the slider for.
     * @param {object} [options] - The options.
     */
    function SliceSlider (element, options) {
        var context = this;
        Slice.WorkflowPlugin.prototype.constructor.call(context, SliceSlider.factory);
        // Slider element.
        context.$element = $(element);
        // Store element.
        context.$element.data('slice.slider', context);
        // Current settings for the slider.
        context.settings = null;
        // Current stage.
        context.stage = null;
        // Slider layers.
        context.layers = {};
        // Slider layers count.
        context.layersCount = {};
        // Default options.
        context.defaults = $.extend({}, sliceDefaults, fnSlice.Defaults || {});
        // Current options set by the caller including defaults.
        context.options = $.extend(
            {}, options || {},
            options && options.optionsProvider
                ? parseOptions(context.$element.data(options.optionsProvider))
                : {}
        );
        // Current slide index.
        context.currentPosition = 1;
        // Animation speed in milliseconds.
        context.speed = 0;
        // Move to slide index.
        context.moveToPosition = false;
        // Viewport element.
        context.$viewport = $([]);
        // Stage element.
        context.$stage = $([]);
        // Slider items.
        context.$slides = $([]);
        // References to the running plugins of this slider.
        context.plugins = {};
        // Current state information and their tags.
        context.state = new Slice.State({
            destroy: 'busy, hold',
            initializing: 'busy',
            setup: 'busy',
            stageTransition: 'animating, busy, hold',
            transition: 'animating, busy, hold'
        });
        // Enter setup state.
        context.state.enter('setup');
        // Add plugins.
        $.each(fnSlice.Plugins, function (key, Plugin) {
            context.plugins[key.charAt(0).toLowerCase() + key.slice(1)] = new Plugin(context);
        });
        // Pre setup.
        $.each(context.plugins, function (key, plugin) {
            if (typeof plugin.preSetup === 'function') {
                plugin.preSetup();
            }
        });
        // Do setup if not done.
        if (context.state.is('setup')) {
            var theme = getTheme(context.options.theme || context.defaults.theme);
            context.setup($.extend({}, $.extend(
                {}, context.defaults,
                theme && theme.defaults
                    ? theme.defaults
                    : {},
                context.options
            )));
            context.state.leave('setup');
        }
        // Initialize slider.
        context.run('initialize', null, 'initialize');
    }

    /**
     * SliceSlider extends SliceWorkflowPlugin.
     */
    SliceSlider.prototype = Object.create(Slice.WorkflowPlugin.prototype);
    SliceSlider.prototype.constructor = SliceSlider;

    /**
     * Slider version.
     */
    SliceSlider.prototype.version = '1.2';


    /**
     * Add slider workflow factory.
     */
    var factory = new Slice.WorkflowFactory(),
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable,
        setRunnableType = factory.setRunnableType,
        resetRunnableType = factory.resetRunnableType,
        setQuickRun = factory.setQuickRun;
    SliceSlider.factory = factory;

    /**
     * Trigger event.
     *
     * @param {string} name - Event name.
     * @param {string} [namespace] - Event namespace.
     * @param {Array} [data] - Event data.
     * @returns {event} Event data.
     */
    SliceSlider.prototype.trigger = function (name, namespace, data) {
        var context = this,
            event = $.Event(
                [name, 'slice', namespace || 'slider'].join('.').toLowerCase(),
                $.extend({relatedTarget: context}, data)
            );
        context.$element.trigger(event);
        return event;
    };

    /**
     * Setup settings.
     *
     * @param {object} settings - Settings object.
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.setup = function (settings) {
        var context = this,
            theme = getTheme(settings.theme);
        // Default workers requirements, may be overriten in plugins.
        settings.workers = settings.workers || {
            // Check slider slider reached sides.
            checkSides: !settings.rewind,
            // Check viewport height.
            checkheight: !!settings.staticHeight,
            // Fixed viewport height.
            fixedheight: false,
            // Fixed sizes.
            fixedsize: true,
            // Fixed slides size.
            fixedslides: true,
            // Fixed slider view.
            fixedview: true,
            // Move to slide on click.
            focusOnSelect: !!settings.focusOnSelect,
            // Horizontal sliding.
            horizontal: true,
            // Go backwards when the boundary has reached.
            rewind: !!settings.rewind,
            // Single slide.
            single: true,
            // Spaced slides.
            spaced: !!settings.spacing,
            // Speed calculation type.
            speedType: settings.speedType,
            // Static viewport height.
            staticHeight: !!settings.staticHeight,
            // Check if window is active.
            visibilityChange: typeof document.hidden !== 'undefined'
        };
        if (context.stage && context.stage.sliderClasses.length) {
            context.$element.removeClass(context.stage.sliderClasses.join(' '));
        }
        context.stage = {
            sliderClasses: []
        };
        context.settings = settings;
        // Setup plugin
        $.each(context.plugins, function (name, plugin) {
            if (plugin.setup) {
                plugin.setup();
            }
        });
        if (settings.sliderClass) {
            context.stage.sliderClasses.push(settings.sliderClass);
        }
        if (settings.focusOnSelect) {
            context.stage.sliderClasses.push('slice-focus-on-select');
        }
        // Setup theme
        if (theme) {
            context.stage.theme = theme;
            if (theme.sliderClass) {
                context.stage.sliderClasses.push(theme.sliderClass);
            }
            if (typeof theme.setup === 'function') {
                theme.setup.call(context);
            }
        }
        if (context.stage.sliderClasses.length) {
            context.$element.addClass(context.stage.sliderClasses.join(' '));
        }
        // Get workers manager for current setup.
        context.hireManager(context.settings.workers);
        // Mark that settings were changed.
        context.invalidate('settings');
        // Trigger change event.
        context.trigger('change');
        return context;
    };

    /**
     * Prepare slide before adding.
     *
     * @param {HTMLElement|jQuery|string} content - Slide content.
     * @returns {jQuery|HTMLElement} - Slide element.
     */
    SliceSlider.prototype.prepare = function (content) {
        var context = this,
            event = context.trigger('prepare', false, {content: content}),
            $slide = $('<' + context.settings.slideElement + '/>')
                .addClass(context.settings.slideClass)
                .append(event.content);

        if (context.resizeHandler) {
            $slide.find('[src], [data-src]').on('load.slice.slider', function () {
                $(this).closest('.' + context.settings.slideClass)
                    .addClass('slice-check-size');
                context.resizeHandler();
            });
        }

        event = context.trigger('prepared', false, {$slide: $slide});

        return event.$slide;
    };

    /**
     * Add slide.
     *
     * @param {HTMLElement|jQuery|string} content - Slide content.
     * @param {number} [position] - Position to insert the slide, otherwise add to the end.
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.add = function (content, position) {
        var context = this,
            $content = $(content),
            pos = typeof position === 'undefined'
                ? this.$slides.length
                : this.normalize(position, true);
        context.trigger('add', false, {
            content: $content,
            position: pos});

        var $slide = context.prepare(content),
            slides = context.$slides.get();
        slides.splice(pos, 0, $slide[0]);
        // Create new slides list with new slide added at specified position.
        context.$slides = $(slides);
        context.invalidate('slides');
        context.trigger('added', false, {
            position: pos,
            slide: $slide});
        return context;
    };

    /**
     * Refresh slider.
     *
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.refresh = function () {
        var context = this;
        context.trigger('refresh');
        context.update();
        context.trigger('refreshed');
        return context;
    };

    /**
     * Trigger slider resize.
     *
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.resize = function () {
        var context = this;
        if (context.resizeDelay) {
            clearTimeout(context.resizeDelay);
        }
        context.resizeDelay = window.setTimeout(function () {
            context.resizeDelay = false;
            context.run('resize', {
                silent: true
            });
        }, 50);
        return context;
    };

    /**
     * Check if slider is visible.
     *
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.isVisible = function () {
        return this.settings.checkVisibility
            ? this.$element.is(':visible')
            : true;
    };

    /**
     * Get arguments for SliceWorkflowManager workers.
     *
     * @param {object} cache - Worker cache.
     * @returns {Array} Array with workers arguments.
     */
    SliceSlider.prototype.getManagerData = function (cache) {
        return [this.stage, cache];
    };

    /**
     * Reset current slide position.
     *
     * @param {number} position - The absolute position of the new item.
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.reset = function (position) {
        var context = this;
        if (typeof position !== 'undefined') {
            context.moveToPosition = position;
        }
        context.invalidate('reset');
        context.update();
        return context;
    };

    /**
     * Move to slide position.
     *
     * @param {number} position - Slide position.
     * @param {number} [speed] - Time in milliseconds for the transition.
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.to = function (position, speed) {
        var context = this;
        if (context.settings.waitForAnimate && context.state.is('animating')) {
            return context;
        }
        context.moveToPosition = position;
        context.speed = speed;
        context.invalidate('move');
        context.update();
        return context;
    };

    /**
     * Move to the next slide position.
     *
     * @param {number} [speed] - The time in milliseconds for the transition.
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.next = function (speed) {
        var context = this;
        return context.to(context.run('getNextTo'), speed);
    };

    /**
     * Move to the previous slide position.
     *
     * @param {number} [speed] - The time in milliseconds for the transition.
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.prev = function (speed) {
        var context = this;
        return context.to(context.run('getPrevTo'), speed);
    };

    /**
     * Handle the end of an animation.
     *
     * @returns {SliceSlider} Current instance.
     */
    SliceSlider.prototype.onTransitionEnd = function () {
        var context = this;
        context.state.leave('transition');
        context.invalidate('translated');
        context.update();
        context.trigger('translated');
        return this;
    };

    /**
     * Add workers.
     *
     * Reset styles and sizes.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        stage.viewHeight = false;
        stage.displacement = 0;
        stage.displaceView = 0;
        stage.currentDisplacement = 0;
        stage.currentShift = 0;
        stage.slideSpacing = 0;
        if (stage.getPosition) {
            cache.previousPosition = stage.getPosition(this.$stage);
        }
        this.$viewport.attr('style', '');
        this.$stage.attr('style', '');
        if (stage.$slides) {
            stage.$slides.attr('style', '');
        }
    }, -100);

    /**
     * Check if slider has fixed height.
     */
    addWorker('size, settings', function (stage, cache) {
        // Add fake slide with enourmous height to check if viewport height is limited.
        var context = this,
            checkVal = 20000,
            $checkEl = $('<' + context.settings.slideElement + '/>')
                .addClass(context.settings.slideClass)
                .css('height', checkVal)
                .appendTo(context.$stage),
            height = context.$viewport.innerHeight(),
            fixedheight = height < checkVal;
        stage.viewportMaxHeight = fixedheight
            ? height
            : false;
        context.$element.toggleClass('slice-has-fixed-view', fixedheight);
        $checkEl.remove();
        if (context.settings.workers.fixedheight !== fixedheight) {
            context.settings.workers.fixedheight = fixedheight;
            cache.rebuild = true;
        }
    }, -20);

    /**
     * Re-build if requested.
     */
    addWorker(true, function (stage, cache) {
        if (cache.rebuild) {
            this.hireManager(this.settings.workers);
        }
        if (cache.rebuild || cache.reUpdate) {
            return 're-update';
        }
    });

    /**
     * Cleanup resize delay.
     */
    addWorker('size', function () {
        if (this.resizeDelay) {
            clearTimeout(this.resizeDelay);
            this.resizeDelay = false;
        }
    });

    /**
     * Create layers.
     */
    addWorker('settings', function (stage, cache) {
        var context = this,
            layers = {};
        if (cache.layers) {
            // Normalize layers.
            $.each(cache.layers, function (key, value) {
                var layer = {};
                if (typeof value === 'object') {
                    if (value.class) {
                        layer.class = [value.class];
                    }
                    layer.layer = value.layer;
                } else if (typeof key === 'string') {
                    layer.class = [key];
                    layer.layer = value;
                } else if (typeof key === 'number') {
                    layer.layer = key;
                } else {
                    layer = false;
                }
                if (layer) {
                    if (!layers[layer.layer]) {
                        if (!layer.class) {
                            layer.class = [];
                        }
                        layers[layer.layer] = layer;
                    } else if (layer.class) {
                        layers[layer.layer].class.push(layer.class);
                    }
                }
            });
        }
        // Add view layer.
        if (context.settings.viewLayer) {
            if (!layers[context.settings.viewLayer]) {
                layers[context.settings.viewLayer] = {
                    class: context.settings.viewLayerClass
                        ? [context.settings.viewLayerClass]
                        : [],
                    layer: context.settings.viewLayer
                };
            } else if (context.settings.viewLayerClass) {
                layers[context.settings.viewLayer].class.push(context.settings.viewLayerClass);
            }
        }
        // Create layers elements.
        var sorted = [],
            layersLeft = context.layersCount;
        $.each(layers, function (key, layer) {
            if (context.layers[key]) {
                --layersLeft;
                layer.$element = context.layers[key].$element.attr('class', '');
                delete context.layers[key];
            } else {
                layer.$element = $('<div/>');
            }
            layer.$element
                .addClass('slice-layer-' + layer.layer + ' slice-layer')
                .addClass(layer.class.join(' '));
            sorted.push(layer);
        });
        // Sort layers.
        sorted.sort(function (item1, item2) {
            return item1.layer - item2.layer;
        });
        sorted[0].$element.addClass('slice-first-layer');
        sorted[sorted.length - 1].$element.addClass('slice-last-layer');
        var $layer = context.$viewport;
        if (context.settings.viewLayer) {
            layers[context.settings.viewLayer].$element.prepend($layer);
            $layer = $([]);
        }
        // Check if layers should be created or deleted.
        if (layersLeft === 0 && context.layersCount === sorted.length) {
            context.layers = layers;
            context.layersCount = sorted.length;
            return;
        }
        // Nest layers elements.
        $.each(sorted, function (i, layer) {
            $layer = layer.$element.prepend($layer);
        });
        context.$element.prepend($layer);
        // Remove layers.
        if (layersLeft > 0) {
            cache.$trash = cache.$trash || $([]);
            $.each(context.layers, function (key, layer) {
                layer.$element.remove();
                delete context.layers[key];
            });
        }
        // Store layers.
        context.layers = layers;
        context.layersCount = sorted.length;
    });

    /**
     * Set dimensions.
     */
    addWorker('settings', function (stage) {
        stage.revSizeProp = cssProps.height;
        stage.sizeProp = cssProps.width;
        stage.getSize = function ($el) {
            return $el.innerWidth();
        };
        stage.setSize = function ($el, size) {
            $el.innerWidth(size);
        };
        stage.spacingProps = ['marginLeft', 'marginRight'];
        stage.positionProp = 'left';
        stage.getPosition = function ($el, position) {
            if (position) {
                return typeof position === 'object'
                    ? position.left || 0
                    : position;
            }
            return $el.position().left;
        };
    }, 0, 'horizontal');

    /**
     * Move animation function.
     *
     * @param {number} offset - Offset to move at.
     * @param {number} speed - Animation speed.
     * @param {object} animateProps - Othe animation styles.
     * @returns {object} Slice postponed.
     */
    function animateMove (offset, speed, animateProps) {
        var context = this,
            postponed = new Slice.Postponed(),
            props = animateProps || {};
        props[context.stage.positionProp] = offset + 'px';
        postponed.fail(function (reason) {
            if (reason === 'finish') {
                context.$stage
                    .finish()
                    .css(props);
            } else {
                context.$stage.stop(true);
            }
        });
        postponed.add(function () {
            if (speed && speed > 0) {
                context.$stage.animate(props, speed, context.settings.fallbackEasing, function () {
                    postponed.resolve();
                });
            } else {
                context.$stage.css(props);
                postponed.resolve();
            }
            return postponed;
        });
        return postponed;
    }

    /**
     * Fade animation function.
     *
     * @param {number} offset - Offset to move at.
     * @param {number} speed - Animation speed.
     * @param {object} animateProps - Othe animation styles.
     * @returns {object} Slice postponed.
     */
    function animateFade (offset, speed, animateProps) {
        var context = this,
            postponed = new Slice.Postponed(),
            props = animateProps || {},
            $hideSlides, $showSlides,
            queueName = 'fadeQueue',
            stagePosition = offset + 'px';
        postponed
            .fail(function (reason) {
                if ($hideSlides) {
                    $hideSlides.remove();
                }
                if (reason === 'finish') {
                    context.$stage
                        .finish()
                        .css(props);
                    if ($showSlides) {
                        $showSlides.finish(queueName);
                    }
                } else {
                    context.$stage.stop(true);
                    if ($showSlides) {
                        $showSlides.stop(queueName, true);
                    }
                }
            })
            .add(function () {
                // Remove all temp slides before animating.
                context.$element.find('.slice-temp-slide').remove();
                if (speed && speed > 0) {
                    var stage = context.stage;
                    $hideSlides = context.run('getVisibleSlides');
                    var startPos = stage.getPosition(context.$stage);
                    context.$stage.css(context.stage.positionProp, stagePosition);
                    var endPos = stage.getPosition(context.$stage);
                    $showSlides = context.run('getVisibleSlides');
                    // Clone slides
                    var $clonedSlides = $hideSlides.clone().addClass('slice-temp-slide');
                    $clonedSlides.each(function (i, el) {
                        var $el = $(el),
                            ind = $hideSlides.index($hideSlides[i]);
                        var prop = {
                            position: 'relative',
                            zIndex: 1
                        };
                        if (stage.currentSlideSize) {
                            $el.insertAfter($showSlides[ind]);
                        } else {
                            $el.insertAfter($hideSlides[ind]);
                            prop[stage.positionProp] = startPos - endPos;
                        }
                        prop[stage.spacingProps[0]] = -stage.getSize($el);
                        $el.css(prop);
                    });
                    $hideSlides = $clonedSlides;
                    $showSlides.css({
                        opacity: 0,
                        position: 'relative',
                        zIndex: 2
                    });
                    var count = $hideSlides.length + $showSlides.length,
                        resetProps = {
                            opacity: '',
                            position: '',
                            zIndex: ''
                        },
                        animationOpts = {
                            complete: function () {
                                --count;
                                $(this).css(resetProps);
                                if (!count) {
                                    postponed.resolve();
                                }
                            },
                            duration: speed,
                            easing: context.settings.fallbackEasing,
                            queue: queueName
                        };
                    resetProps[stage.positionProp] = '';
                    context.$stage.animate(props, speed, context.settings.fallbackEasing);
                    $showSlides.animate({
                        opacity: 1
                    }, animationOpts).dequeue(queueName);
                    $hideSlides.animate({
                        opacity: 0
                    }, animationOpts).dequeue(queueName);
                    if ($clonedSlides) {
                        $clonedSlides.queue(queueName, function (next) {
                            $(this).remove();
                            next();
                        });
                    }
                } else {
                    props[context.stage.positionProp] = stagePosition;
                    context.$stage.css(props);
                    postponed.resolve();
                }
                return postponed;
            });
        return postponed;
    }

    addWorker('settings', function (stage) {
        stage.animateMove = animateMove.bind(this);
        if (this.settings.slidingAnimation === 'fade') {
            stage.positioAnimation = true;
            stage.animateSlide = animateFade.bind(this);
        } else {
            stage.animateSlide = animateMove.bind(this);
        }
        stage.move = function (offset, speed, afterMove) {
            var context = this;
            context.state.exit('stageTransition');
            context.$stage.stop(true);
            if (speed) {
                context.state.enter('stageTransition');
                var props = {};
                props[context.stage.positionProp] = offset + 'px';
                context.$stage.animate(props, speed, context.settings.fallbackEasing, function () {
                    context.state.leave('stageTransition');
                    if (typeof afterMove === 'function') {
                        afterMove.call(context);
                    }
                });
            } else {
                context.$stage.css(context.stage.positionProp, offset + 'px');
                if (afterMove) {
                    afterMove.call(context);
                }
            }
        }.bind(this);
    });

    /**
     * Set view and work sizes.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        stage.viewportSize = stage.getSize(this.$viewport);
        cache.viewSize = stage.viewportSize;
    }, 10, ['horizontal', '!horizontal, fixedheight']);
    addWorker('size, slides, settings', function (stage, cache) {
        stage.viewSize = cache.viewSize;
        cache.workSpace = cache.viewSize;
    }, 20);
    addWorker('size, slides, settings', function (stage, cache) {
        stage.workSpace = cache.workSpace;
    }, 30);

    /**
     * Set minimum slides length.
     */
    addWorker('settings', function (stage) {
        stage.minSlides = 1;
    }, 0, 'single');

    /**
     * Cleanup slides elements.
     */
    addWorker('slides, settings', function (stage, cache) {
        var context = this;
        context.$slides.off('.slice');
        context.$slides.removeClass(context.settings.currentClass + ' ' + context.settings.activeClass);
        cache.$slides = $([]).add(context.$slides.detach());
    });

    /**
     * Set new slides elements.
     */
    addWorker('slides, settings', function (stage, cache) {
        stage.$slides = cache.$slides;
        stage.slidesCount = stage.$slides.length;
        stage.$slides.each(function (i, el) {
            $(el).attr('data-slice-index', i + 1);
        });
        this.$stage.append(stage.$slides);
    }, 9);

    /**
     * Set slide splacing.
     */
    addWorker('size, settings', function (stage) {
        stage.slideSpacing += Slice.Unit.toPx(this.settings.spacing, stage.viewSize);
    }, 20, 'spaced');

    /**
     * Set slides coordinates displacements.
     */
    addWorker('size, slides, settings', function (stage) {
        // Displacement per slide
        stage.displacePerSlide = stage.slideSpacing;
        // Displacement for first slide
        stage.displaceFirst = stage.slideSpacing;
    }, 30);

    /**
     * Set slide size.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.slideSize = stage.workSpace;
    }, 30, 'single');

    /**
     * Add move to slide on click event.
     */
    addWorker('slides, settings', function (stage) {
        var context = this;

        /**
         * Slide focus handler.
         *
         * @param {event} event - Click event.
         */
        function focusHandler (event) {
            var settings = this.settings;
            if (this.stage.skipFocus) {
                this.stage.skipFocus = false;
            } else {
                this.to($(event.target).closest('.' + settings.slideClass)
                    .data('sliceIndex'));
            }
        }
        stage.$slides.on('click.slice.focus', focusHandler.bind(context));
    }, 40, 'focusOnSelect');

    /**
     * Check enought slides.
     */
    addWorker('edges, slides, settings', function (stage) {
        stage.enoughSlides = stage.slidesCount > stage.minSlides;
    }, 40);

    /**
     * Store sizes.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        stage.slideSize = cache.slideSize;
        stage.size = stage.slideSize * stage.$slides.length;
    }, 60, 'fixedsize');

    /**
     * Add spacing to slides.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.$slides.css(stage.spacingProps[0], stage.slideSpacing);
        stage.size += stage.slideSpacing * stage.$slides.length;
    }, 60, 'spaced');

    /**
     * Set stage size.
     * Toggle notEnoughtSlidesClass class if not enought slides.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.setSize(this.$stage, stage.size);
        this.$element.toggleClass(this.settings.notEnoughtSlidesClass, !stage.enoughSlides);
    }, 70);

    /**
     * Set slides elements sizes.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.setSize(stage.$slides, stage.slideSize);
    }, 70, 'fixedsize');

    /**
     * Set viewport element height.
     */
    addWorker('size, slides, settings', function () {
        this.$viewport[cssProps.height](Math.ceil(this.run('getViewMaxHeight')));
    }, 70, '!fixedheight, horizontal, staticHeight');

    /**
     * Force browser to redraw slider.
     */
    addWorker('size, slides, settings', function () {
        Slice.redraw(this.$element[0]);
    }, 80);

    /**
     * Calculate slides coordinates.
     */
    addWorker('size, slides, settings', function (stage) {
        var size = stage.$slides.length,
            iterator = -1,
            base = {},
            coordinates = {};
        while (++iterator < size) {
            base[iterator + 1] = -1 * iterator * (stage.slideSize + stage.displacePerSlide);
            coordinates[iterator + 1] = base[iterator + 1] - stage.displaceFirst;
        }
        stage.baseCoordinates = base;
        stage.coordinates = coordinates;
    }, 85, 'fixedsize');

    /**
     * Set first and last slide positions.
     */
    addWorker('edges, slides, settings', function (stage) {
        stage.lastSlide = stage.slidesCount;
        stage.startSlide = 1;
    }, 100);

    /**
     * Check first and last slides positions.
     */
    addWorker('edges, slides, settings', function (stage) {
        if (stage.lastSlide > stage.slidesCount) {
            stage.lastSlide = stage.slidesCount || 1;
        }
        if (stage.startSlide > stage.slidesCount) {
            stage.startSlide = stage.slidesCount || 1;
        } else if (stage.startSlide < 1) {
            stage.startSlide = 1;
        }
        if (stage.startSlide > stage.lastSlide || !stage.enoughSlides) {
            stage.lastSlide = stage.startSlide;
        }
    }, 120);

    /**
     * Check viewport size, if changed force resize.
     */
    addWorker(true, function (stage, cache) {
        cache.resizeRequired = cache.resizeRequired ||
            Math.abs(stage.viewportSize - stage.getSize(this.$viewport)) > 1;
    }, 197, ['horizontal', '!horizontal, fixedview']);

    /**
     * Run resize if requested.
     * Maximum 2 in a row resizes.
     */
    addWorker(true, function (stage, cache) {
        var context = this;
        if (!stage.stopResize && cache.resizeRequired) {
            if (stage.forceResizeCount > 2) {
                // Whoops cant properly resize viewport
                if (window.console) {
                    console.log('Can\'t properly resize viewport for:');
                    console.log(context);
                }
                stage.stopResize = true;
            } else {
                stage.forceResizeCount = (stage.forceResizeCount || 0) + 1;
                context.invalidate('size');
                return 're-update';
            }
        }
    }, 198);

    /**
     * Reset resizes count.
     */
    addWorker(true, function (stage) {
        stage.forceResizeCount = 0;
    }, 1000);

    /**
     * Trigger content resize.
     */
    addWorker('size', function () {
        $(window).trigger('contentresize');
    }, 1000);

    /**
     * Reset current slide position.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        this.moved = true;
        if (this.moveToPosition === false) {
            this.reset();
        } else if (Slice.hasProperty(cache, 'previousPosition')) {
            stage.animateMove(cache.previousPosition).run();
        }
    }, 199);

    /**
     * Set move options for reseting slide position.
     */
    addWorker('reset', function (stage, cache) {
        cache.speed = 0;
        cache.moveSpeed = 0;
        cache.moveToPosition = this.moveToPosition || this.currentPosition;
        cache.fromPosition = this.currentPosition;
    }, 200);

    /**
     * Set move options for moving to slide position.
     */
    addWorker('move', function (stage, cache) {
        cache.speed = this.speed;
        cache.fromPosition = stage.position || this.currentPosition;
        cache.moveToPosition = this.moveToPosition;
    }, 200);

    /**
     * Normalize position.
     */
    addWorker('reset, move', function (stage, cache) {
        var positionData = this.run('current, force', {position: cache.moveToPosition}, 'setPosition');
        this.moveToPosition = false;
        cache.realPosition = positionData.real;
        cache.currentPosition = positionData.current;
    }, 210);

    /**
     * Check slider animation.
     */
    addWorker('reset, move', function (stage, cache) {
        if (this.state.is('animating') || this.moved) {
            this.run('finishAnimation');
        }
        cache.animation = new Slice.Postponed();
    }, 300);

    /**
     * Remove style 'transition' before animation.
     */
    addWorker('reset, move', function (stage) {
        this.state.enter('transition');
        this.$stage.css('transition', 'none');
        stage.$slides.css('transition', 'none');
    }, 300);

    /**
     * Calculate move speed.
     */
    addWorker('move', function (stage, cache) {
        var speedVal = parseInt(cache.speed);
        if (isNaN(speedVal)) {
            if (this.moved || !this.settings.slidingSpeed) {
                var speedOpts = this.run(['to', 'base', this.settings.speedType], {
                    to: cache.realPosition,
                    toReal: true}, 'speed');
                cache.baseSpeed = speedOpts.base;
                cache.moveSpeed = speedOpts.speed;
            } else {
                cache.baseSpeed = this.settings.slidingSpeed;
                cache.moveSpeed = this.settings.slidingSpeed;
            }
        } else {
            cache.moveSpeed = Math.max(speedVal, 0);
            cache.baseSpeed = Math.min(cache.moveSpeed, this.settings.speed);
        }
    }, 300);

    /**
     * Calculate move to coordinate.
     */
    addWorker('reset, move', function (stage, cache) {
        cache.moveToCoordinate = this.run('getMoveToCoordinate', {
            isReal: true,
            position: cache.realPosition
        });
        cache.animationType = this.moved
            ? 'animateMove'
            : 'animateSlide';
    }, 300);

    /**
     * Add move animation.
     */
    addWorker('reset, move', function (stage, cache) {
        var context = this,
            speed = cache.moveSpeed || 0,
            animate = speed > 0;
        if (animate) {
            context.trigger('translate');
        }
        var animation = stage[cache.animationType](cache.moveToCoordinate, speed, cache.animateProps)
            .done(function () {
                context.onTransitionEnd();
            });
        cache.animation.add(animation);
    }, 350);

    /**
     * Run animations.
     */
    addWorker('reset, move', function (stage, cache) {
        var animation = this.animation;
        this.animation = cache.animation;
        cache.animation.runAfter(animation, true);
        this.moved = false;
    }, 400);

    /**
     * Cleanup after animations.
     */
    addWorker('translated', function (stage) {
        if (!this.state.is('animating')) {
            this.$stage.css('transition', '');
            stage.$slides.css('transition', '');
        }
    }, 450);

    /**
     * Initialize slider runnables (private).
     */
    setRunnableType('initialize');

    /**
     * Enter initializing state.
     * Trigger initialize event.
     */
    addRunnable('initialize', function () {
        this.state.enter('initializing');
        this.trigger('initialize');
        if (!this.resizeHandler) {
            this.resizeHandler = this.resize.bind(this);
        }
    }, -1);

    /**
     * Create viewport and stage elements.
     */
    addRunnable('initialize', function () {
        var context = this;
        context.$stage = context.$element.find('.' + context.settings.stageClass);
        // If the stage is already in the DOM, grab it and skip stage initialization.
        if (context.$stage.length) {
            context.$viewport = context.$element.find('.' + context.settings.viewportClass);
            context.$stageWrap = context.$element.find('.' + context.settings.stageWrapClass);
            return;
        }
        // Create viewport.
        context.$viewport = $('<div/>', {
            class: context.settings.viewportClass
        });

        // Create and append stage.
        context.$stage = $('<' + context.settings.stageElement + '>', {
            class: context.settings.stageClass
        }).appendTo(context.$viewport);
        context.$element.append(context.$viewport);
    }, 10);

    /**
     * Create slides elements.
     */
    addRunnable('initialize', function () {
        var context = this,
            $slides = context.$element.find('.' + context.settings.slideClass);
        // If the items are already in the DOM, grab them and skip item initialization.
        if ($slides.length) {
            context.$slides = $slides.detach();
            context.invalidate('slides');
            return;
        }
        // Add slides.
        context.$element.children().not(context.$viewport)
            .each(function (i, content) {
                context.add(content);
            });
    }, 20);

    /**
     * Update slider if visible.
     */
    addRunnable('initialize', function () {
        var context = this;
        context.$element.addClass('slice-initialized');
        if (context.isVisible()) {
            context.update();
        } else {
            context.invalidate('size');
        }
    }, 50);

    /**
     * Bind events.
     */
    addRunnable('initialize', function () {
        var context = this;
        // Disable browser dragging.
        context.$stage
            .off('.slice.noDrag')
            .on('dragstart.slice.noDrag', function (e) {
                e.preventDefault();
            });
        context.$slides.find('[src], [data-src]')
            .off('.slice.slider')
            .on('load.slice.slider', context.resizeHandler);
        context.$element
            .off('.slice.slider')
            .on('windowresize.slice.slider', context.resizeHandler)
            .on('destroyed.slice.slider', function () {
                $(this).data('slice.slider')
                    .run('destroy');
            })
            .on('mouseeneter.slice mousemove.slice click.slice touchmove.slice', function () {
                context.run('userActive');
            });
    }, 90);

    /**
     * Check user activity.
     * Leave initializing state.
     * Trigger initialized event.
     */
    addRunnable('initialize', function () {
        this.run('userActive');
        this.state.leave('initializing');
        this.trigger('initialized');
    }, 100);

    /**
     * Resize runnables.
     */
    setRunnableType('resize');
    // Check slider size and run slider resize if needed.
    setQuickRun('resize', 'resize');
    // Check slider needs to be resized.
    setQuickRun('checkResize', 'runcheck');

    /**
     * Run viewport size check.
     */
    addRunnable('runcheck', function (stage, cache) {
        var context = this;
        stage.stopResize = false;
        context.$element.addClass('slice-resize-check');
        cache.result = context.run('check', {
            $changed: this.$element.find('.slice-check-size').removeClass('slice-check-size')
        }, 'resize').result;
        context.$element.removeClass('slice-resize-check');
    });

    /**
     * Check viewport size changed.
     */
    addRunnable('check', function (stage, cache) {
        if (Math.ceil(stage.viewportSize) !== Math.ceil(stage.getSize(this.$viewport))) {
            cache.result = true;
            return false;
        }
    });

    /**
     * Check stage size changed.
     */
    addRunnable('check', function (stage, cache) {
        var size = 0,
            context = this;
        if (context.settings.workers.fixedheight) {
            context.$element.removeClass('slice-has-fixed-view');
        }
        stage.$slides.each(function (i, el) {
            size += stage.getSize($(el));
        });
        if (context.settings.workers.fixedheight) {
            context.$element.addClass('slice-has-fixed-view');
        }
        if (Math.ceil(size) !== Math.ceil(stage.size)) {
            cache.result = true;
            return false;
        }
    }, 0, '!fixedslides');

    /**
     * Check viewport height changed.
     */
    addRunnable('check', function (stage, cache) {
        this.$viewport[cssProps.height]('');
        var height = this.run('getViewMaxHeight', {force: true});
        if (Math.ceil(stage.viewHeight) !== Math.ceil(height)) {
            cache.result = true;
            return false;
        }
        this.$viewport[cssProps.height](height);
    }, 0, 'checkheight, !fixedheight, horizontal');

    /**
     * Stop execution if size not changed.
     */
    addRunnable('resize', function (stage, cache) {
        if (cache.silent && !this.run('checkResize')) {
            return false;
        }
    }, 40);

    /**
     * Reset view height and reques resize.
     */
    addRunnable('resize', function () {
        this.stage.viewHeight = false;
        this.invalidate('size');
    }, 50);

    /**
     * Update slider.
     */
    addRunnable('resize', function () {
        this.update();
    }, 100);

    /**
     * Get size runnables.
     */
    setRunnableType('size');
    // Get maximal view height.
    setQuickRun('getViewMaxHeight', 'viewHeight, height, explicitHeight, viewExplicit');
    // Get max height of active slides.
    setQuickRun('getActiveHeight', 'activeHeight, height, explicitHeight');
    // Get max height of slides in view.
    setQuickRun('getInViewHeight', 'inViewHeight, height, explicitHeight, inViewExplicit');
    // Get max height of  visible slides.
    setQuickRun('getVisibleHeight', 'visibleHeight, height, explicitHeight, visibleExplicit');
    // Same as getInViewHeight, but for current slider state.
    setQuickRun('getInViewHeightLive', 'inViewHeight, height');
    // Same as getVisibleHeight, but for current slider state.
    setQuickRun('getVisibleHeightLive', 'visibleHeight, height');

    /**
     * If view height already calculated return it.
     */
    addRunnable('viewHeight', function (stage, cache) {
        if (stage.viewHeight && !cache.force) {
            cache.result = stage.viewHeight;
            return false;
        }
    }, -10);

    /**
     * Prepare for calculations.
     */
    addRunnable('height', function (stage, cache) {
        cache.slideSizes = [0];
        stage.$slides.css('transition', 'none');
    });

    /**
     * Set slides elements for view height calculation.
     */
    addRunnable('viewHeight', function (stage, cache) {
        cache.$slides = stage.$slides;
    });

    /**
     * Set slides elements for view height calculation.
     */
    addRunnable('visibleHeight', function (stage, cache) {
        cache.$slides = this.run('getVisibleSlides', {coordinate: cache.coordinate});
    });

    /**
     * Set slides elements for in view height calculation.
     */
    addRunnable('activeHeight', function (stage, cache) {
        cache.$slides = stage.$slides.filter('.' + this.settings.activeClass);
    });

    /**
     * Set slides elements for in view height calculation.
     */
    addRunnable('inViewHeight', function (stage, cache) {
        cache.$slides = this.run('getInViewSlides', {coordinate: cache.coordinate});
    });

    /**
     * Calculate maximal slide height.
     */
    addRunnable('height', function (stage, cache) {
        cache.$slides.each(function (i, el) {
            cache.slideSizes.push($(el)[cssProps.height]());
        });
        cache.slideSize = Math.max.apply(Math, cache.slideSizes);
    }, 10);

    /**
     * Cleanup and set results.
     */
    addRunnable('height', function (stage, cache) {
        cache.viewSize = cache.slideSize;
    }, 20, ['horizontal', 'single']);
    addRunnable('height', function (stage, cache) {
        cache.result = cache.viewSize;
        if (!this.state.is('animating')) {
            stage.$slides.css('transition', '');
        }
    }, 30);
    addRunnable('height', function (stage, cache) {
        // For horizontal view, best practice to make sure height is rounded to bigger integer.
        cache.result = Math.ceil(cache.result);
    }, 100, 'horizontal');
    addRunnable('viewHeight', function (stage, cache) {
        stage.viewHeight = cache.result;
    }, 100);

    /**
     * Get slide position runnables.
     */
    setRunnableType('position');
    // Get slide position, base position can be provided or current position will be taken.
    setQuickRun('getPosition', [], 'position');
    // Get slide position, base position can be provided or current position will be taken.
    setQuickRun('getPrecisePosition', 'normalize, precise', 'normalized');
    // Get slide real position in DOM tree, base position can be provided or current position will be taken.
    setQuickRun('getRealPosition', 'current', 'current');

    /**
     * Cache base position.
     */
    addRunnable(true, function (stage, cache) {
        if (!(Slice.hasProperty(cache, 'position') && typeof cache.position === 'number')) {
            cache.position = this.currentPosition;
        }
    }, 0, '!loop');

    /**
     * Check if position exceeds limits.
     */
    addRunnable(true, function (stage, cache) {
        if (cache.position < 1) {
            cache.position = stage.startSlide;
        } else if (cache.position > stage.slidesCount) {
            cache.position = stage.slidesCount;
        }
    }, 30, '!loop');

    /**
     * Cache normalized and current positions for further calculations.
     */
    addRunnable('real, normalize, current', function (stage, cache) {
        cache.normalized = cache.position;
        cache.current = cache.position;
    }, 50);

    /**
     * Check there is a real slide for position.
     */
    addRunnable(true, function (stage, cache) {
        if (cache.position < 1) {
            cache.position = 1;
        } else if (cache.position > stage.slidesCount) {
            cache.position = stage.slidesCount;
        }
    }, 60);

    /**
     * Cache real and stage positions for further calculations.
     */
    addRunnable('real', function (stage, cache) {
        cache.real = cache.normalized;
        cache.stage = cache.normalized;
    }, 70);

    /**
     * Check if real slide position exceeds limits.
     */
    addRunnable('real', function (stage, cache) {
        if (cache.real < stage.startSlide) {
            cache.real = stage.startSlide;
        } else if (cache.real > stage.$slides.length) {
            cache.real = stage.$slides.length;
        }
    }, 100);

    /**
     * Set slide position runnables.
     */
    setRunnableType('setPosition');
    // Set current slide, by provided position, i.e. adds current and active classes to current and active slides
    setQuickRun('setCurrent', 'current', 'real, current, applied');
    // Same as setCurrent, but in addition applies slides and slider styling.
    setQuickRun('setCurrentLive', 'current, live', 'real, current, applied');
    // Same as setCurrent, but sets extra current slides
    setQuickRun('setExtraCurrent', 'extraCurrent', 'real, current, applied');

    /**
     * Force position set.
     */
    addRunnable('force', function (stage, cache) {
        cache.force = true;
    });

    /**
     * Set positions.
     */
    addRunnable('current, extraCurrent', function (stage, cache) {
        var position = Slice.hasProperty(cache, 'position')
                ? cache.position
                : this.currentPosition,
            positionData = this.run('normalize, precise, real, current', {
                isReal: cache.isReal,
                position: position}, 'position');
        cache.position = positionData.position;
        cache.real = positionData.real;
        cache.stage = positionData.stage;
        cache.current = positionData.current;
        cache.normalized = positionData.normalized;
        cache.currentPosition = this.currentPosition;
        cache.applied = false;
    });

    /**
     * If there is from position calculate it too..
     */
    addRunnable('current', function (stage, cache) {
        if (Slice.hasProperty(cache, 'from')) {
            var positionData = this.run('current', {
                isReal: cache.fromReal,
                position: cache.from}, 'position');
            cache.from = positionData.position;
            cache.realFrom = positionData.current;
        }
    });

    /**
     * Stop running if not forced and already at current position.
     * Set stage position data.
     */
    addRunnable('current', function (stage, cache) {
        if (!cache.force && cache.real === cache.currentPosition) {
            return false;
        }
        this.currentPosition = cache.position;
        stage.position = cache.stage;
        stage.isFirstSlide = cache.stage <= stage.startSlide;
        stage.isLastSlide = cache.stage >= stage.lastSlide;
        stage.startCoordinate = this.run('getSlideCoordinate', {
            position: stage.startSlide
        });
        stage.endCoordinate = this.run('getSlideCoordinate', {
            position: stage.lastSlide
        });
    }, 10);

    /**
     * Stop running if it's already extra current.
     */
    addRunnable('extraCurrent', function (stage, cache) {
        if ($(stage.$slides[cache.real - 1]).hasClass(this.settings.currentClass)) {
            return false;
        }
    }, 10);

    /**
     * Cache start and end of active slides positions.
     */
    addRunnable('current, active, extraCurrent', function (stage, cache) {
        cache.start = Slice.hasProperty(cache, 'start')
            ? this.run('getRealPosition', {position: cache.start})
            : cache.real;
        cache.end = Slice.hasProperty(cache, 'end')
            ? Math.max(this.run('getRealPosition', {position: cache.end}), cache.start)
            : cache.real;
    }, 10);

    /**
     * Add current slide class.
     */
    addRunnable('current', function (stage) {
        stage.$slides.removeClass(this.settings.currentClass);
    }, 50);
    addRunnable('current, extraCurrent', function (stage, cache) {
        cache.applied = true;
        stage.$slides.eq(cache.current - 1).addClass(this.settings.currentClass);
    }, 50);

    /**
     * Add active slides class.
     */
    addRunnable('current, active', function (stage) {
        stage.$slides.removeClass(this.settings.activeClass);
    }, 50);
    addRunnable('current, active, extraCurrent', function (stage, cache) {
        stage.$slides.slice(cache.start - 1, cache.end).addClass(this.settings.activeClass);
    }, 50);

    /**
     * Get previous/next slide position runnables.
     */
    setRunnableType('movePosition');
    // Get next slide position, position can be provided or current position will be taken.
    setQuickRun('getNextPosition', 'next, normalize, generic', 'to');
    // Get next slide considering stage position.
    setQuickRun('getNextTo', 'next, normalize, checkNext, generic', 'to');
    // Get strict next slide position, i.e. don't allow lower position value.
    setQuickRun('getNextStrict', 'next, normalize, strict, generic', 'to');
    // Get strict next slide position, i.e. don't allow lower position value.
    setQuickRun('getNextRewind', 'next, normalize, rewind', 'to');
    // Get previous slide position, position can be provided or current position will be taken.
    setQuickRun('getPrevPosition', 'prev, normalize, generic', 'to');
    // Get previous slide considering stage position.
    setQuickRun('getPrevTo', 'prev, normalize, checkPrev, generic', 'to');
    // Get strict previous slide, i.e. don't allow lower position value.
    setQuickRun('getPrevStrict', 'prev, normalize, strict, generic', 'to');
    // Get strict previous slide, i.e. don't allow lower position value.
    setQuickRun('getPrevRewind', 'prev, normalize, rewind', 'to');

    /**
     * Set from and to slides positions.
     */
    addRunnable(true, function (stage, cache) {
        cache.positionData = this.run('current, normalize, real', cache, 'position');
        cache.from = cache.positionData.position;
        cache.to = cache.positionData.stage;
    });

    /**
     * Set if can rewind slides position.
     */
    addRunnable('generic', function (stage, cache) {
        cache.rewind = true;
    }, 0, 'rewind');
    addRunnable('rewind', function (stage, cache) {
        cache.rewind = true;
    }, 0);

    /**
     * Set stage coordinate.
     */
    addRunnable('checkNext, checkPrev', function (stage, cache) {
        cache.coordinate = (Slice.hasProperty(cache, 'coordinate')
            ? cache.coordinate
            : stage.getPosition(this.$stage)) + stage.displacement;
    });

    /**
     * Check stage coordinate, wheter should return current position instead.
     */
    addRunnable('checkNext', function (stage, cache) {
        if (Math.floor(cache.coordinate) > Math.ceil(stage.coordinates[cache.positionData.real])) {
            cache.to = cache.list.real
                ? cache.positionData.real
                : cache.positionData.normalized;
            return false;
        }
    }, 10);
    addRunnable('checkPrev', function (stage, cache) {
        if (Math.ceil(cache.coordinate) < Math.floor(stage.coordinates[cache.positionData.real])) {
            cache.to = cache.list.real
                ? cache.positionData.real
                : cache.positionData.normalized;
            return false;
        }
    }, 10);

    /**
     * Adjust position for next/previous slide.
     */
    addRunnable('next', function (stage, cache) {
        cache.to += 1;
    }, 20, 'single');
    addRunnable('prev', function (stage, cache) {
        cache.to -= 1;
    }, 20, 'single');

    /**
     * Check next/previous to rewind position, stop running if pass.
     */
    addRunnable('next', function (stage, cache) {
        if (cache.rewind && cache.to > stage.lastSlide) {
            cache.to = cache.list.strict
                ? stage.lastSlide
                : stage.startSlide;
            return false;
        }
    }, 30);
    addRunnable('prev', function (stage, cache) {
        if (cache.rewind && cache.to < stage.startSlide) {
            cache.to = cache.list.strict
                ? stage.startSlide
                : stage.lastSlide;
            return false;
        }
    }, 30);

    /**
     * Check slide position boundaries.
     */
    addRunnable('prev, next', function (stage, cache) {
        if (cache.to < stage.startSlide) {
            cache.to = stage.startSlide;
            return false;
        } else if (cache.to > stage.lastSlide) {
            cache.to = stage.lastSlide;
            return false;
        }
    }, 30, 'checkSides');

    /**
     * Move to slide.
     */
    setRunnableType('moveToSlide');
    // Move to next slide
    setQuickRun('next', 'next');
    // Move to next slide
    setQuickRun('prev', 'prev');

    /**
     * Get next slide position.
     */
    addRunnable('next', function (stage, cache) {
        cache.position = this.run('getNextTo', cache);
    });

    /**
     * Get prev slide position.
     */
    addRunnable('prev', function (stage, cache) {
        cache.position = this.run('getPrevTo', cache);
    });

    /**
     * Move to position.
     */
    addRunnable(true, function (stage, cache) {
        this.to(cache.position, cache.speed);
    }, 100);

    /**
     * Get slide position by coordinates runnables.
     */
    setRunnableType('closest');
    // Get closest slide position, depending on current/provided coordinates.
    setQuickRun('getClosest', 'closest, normalize', 'normalized');
    // Get closest slide real position in DOM tree, depending on current/provided coordinates.
    setQuickRun('getClosestReal', 'closest, real');
    // Get closest slide position with priority to next slide, depending on current/provided coordinates.
    setQuickRun('getClosestNext', 'next, normalize', 'normalized');
    // Get closest slide position with priority to previous slide, depending on current/provided coordinates.
    setQuickRun('getClosestPrev', 'prev, normalize', 'normalized');

    /**
     * Set stage coordinate.
     */
    addRunnable(true, function (stage, cache) {
        cache.coordinate = stage.displacement + (Slice.hasProperty(cache, 'coordinate')
            ? cache.coordinate
            : stage.getPosition(this.$stage));
    });

    /**
     * Set pecise calculation function.
     */
    addRunnable('preciseSlide', function (stage, cache) {
        var preciseValue = stage.slideSpacing + stage.slideSize * cache.precise || 0;
        cache.getPrecise = function () {
            return preciseValue;
        };
    }, 10, 'fixedsize');
    addRunnable('preciseViewport', function (stage, cache) {
        var preciseValue = stage.viewportSize * cache.precise || 0;
        cache.getPrecise = function () {
            return preciseValue;
        };
    }, 10);
    addRunnable('preciseStatic', function (stage, cache) {
        var preciseValue = cache.precise;
        cache.getPrecise = function () {
            return preciseValue;
        };
    }, 10);

    /**
     * Set position check function.
     */
    addRunnable('closest', function (stage, cache) {
        cache.checkClosest = function (coordinates, coordinate, position) {
            return typeof coordinates[position + 1] === 'undefined' || coordinate > coordinates[position + 1];
        };
    }, 10);
    addRunnable('prev', function (stage, cache) {
        var offset = cache.prevSlideOffset || 1;
        if (!cache.getPrecise) {
            cache.getPrecise = function () {
                return stage.slideSpacing;
            };
        }
        cache.checkClosest = function (coordinates, coordinate, position) {
            var preciseValue = cache.getPrecise(position),
                nextIndex = position + offset,
                distance = coordinate - coordinates[nextIndex];
            return typeof coordinates[nextIndex] === 'undefined' || distance > preciseValue;
        };
    }, 10);
    addRunnable('next', function (stage, cache) {
        cache.checkClosest = function (coordinates, coordinate, position) {
            var preciseValue = cache.getPrecise(position),
                nextIndex = position + 1,
                distance = coordinates[position] - coordinate;
            return typeof coordinates[nextIndex] === 'undefined' || distance < preciseValue;
        };
    }, 10);

    /**
     * Calculate slide position.
     */
    addRunnable(true, function (stage, cache) {
        var context = this,
            position = 0,
            coordinate = cache.coordinate,
            coordinates = stage.coordinates;
        $.each(coordinates, function (index, value) {
            position = parseInt(index, 10);
            if (coordinate >= value || cache.checkClosest.call(context, coordinates, coordinate, position)) {
                return false;
            }
        });
        cache.real = position;
    }, 20);

    /**
     * Check stuck to side boundaries.
     */
    addRunnable('prev', function (stage, cache) {
        if (cache.coordinate >= stage.startCoordinate) {
            cache.real = stage.startSlide;
        }
    }, 20, '!loop');
    addRunnable('next', function (stage, cache) {
        if (cache.coordinate <= stage.endCoordinate) {
            cache.real = stage.lastSlide;
        }
    }, 20, '!loop');

    /**
     * Set positions.
     */
    addRunnable(true, function (stage, cache) {
        var keys = [];
        if (cache.list.normalize) {
            keys.push('normalize');
        }
        if (cache.list.real) {
            keys.push('real');
        }
        cache.positionData = this.run(keys, {
            isReal: true,
            position: cache.real}, 'position');
        cache.position = cache.positionData.position;
        cache.normalized = cache.positionData.normalized;
    }, 20);

    /**
     * Move stage runnables.
     */
    setRunnableType('move');
    // Move stage by distance.
    setQuickRun('moveStage', 'stageMove');
    // Move stage to specific coordinate.
    setQuickRun('moveStageTo', 'stageMoveTo');

    /**
     * Check stage built coordinates.
     */
    addRunnable(true, function (stage) {
        if (!stage.coordinates) {
            return false;
        }
    }, -100);

    /**
     * Disable css transitions.
     * Set coordinates.
     */
    addRunnable(true, function (stage, cache) {
        this.$stage.css('transition', 'none');
        stage.$slides.css('transition', 'none');
        cache.currentCoord = stage.getPosition(this.$stage);
        if (!cache.hasOwnProperty('moveCoordinate')) {
            cache.moveCoordinate = cache.currentCoord;
        }
    });

    /**
     * Reset css transitions in the very end.
     */
    addRunnable(true, function (stage) {
        if (!this.state.is('animating')) {
            this.$stage.css('transition', '');
            stage.$slides.css('transition', '');
        }
    }, 1000);

    /**
     * Calculate move coordinate.
     */
    addRunnable('stageMove, stageMoveFrom', function (stage, cache) {
        cache.moveDistance = (cache.reverse
            ? -1
            : 1) * stage.getPosition(false, cache.distance || {});
    }, 10);
    addRunnable('position', function (stage, cache) {
        cache.moveCoordinate = this.run('getSlideCoordinate', cache);
    }, 10);
    addRunnable('stageMoveFrom', function (stage, cache) {
        cache.moveCoordinate = -((cache.fromCoordinate || 0) + cache.moveDistance);
    }, 10);
    addRunnable('stageMoveTo', function (stage, cache) {
        cache.moveCoordinate = -cache.moveTo || 0;
    }, 10);
    addRunnable('stageMove', function (stage, cache) {
        cache.moveCoordinate = cache.currentCoord - cache.moveDistance;
    }, 10);

    /**
     * Set move distance.
     */
    addRunnable(true, function (stage, cache) {
        cache.moveDistance = cache.currentCoord - cache.moveCoordinate;
    }, 20);

    /**
     * Check if need to move.
     */
    addRunnable('stageMove, stageMoveFrom', function (stage, cache) {
        if (cache.moveCoordinate === cache.currentCoord) {
            cache.result = false;
            return false;
        }
    }, 30);

    /**
     * Calculate at and to positions.
     */
    addRunnable('apply', function (stage, cache) {
        cache.at = this.run('getClosest', {coordinate: cache.moveCoordinate});
        cache.to = this.run('getNextPosition', {position: cache.at});
    }, 40);

    /**
     * Count precentage of slide at current slide position.
     *
     * @param {number} position - Slide position.
     * @param {number} moveTo - Stage coordinate.
     * @param {Array} coordinates - Slides coordinates.
     * @param {number} stageSize - Stage size.
     * @param {number} extraDistance - Normalize stage position.
     * @returns {number} Slide position precentage.
     */
    function countApplyPercent (position, moveTo, coordinates, stageSize, extraDistance) {
        var extra = extraDistance || 0,
            atCoords = coordinates[position] - extra,
            perc, toCoordinates;
        if (moveTo < atCoords) {
            if (coordinates.hasOwnProperty(position + 1)) {
                toCoordinates = coordinates[position + 1];
            } else {
                return 1;
            }
        } else if (coordinates.hasOwnProperty(position - 1)) {
            toCoordinates = coordinates[position - 1];
        } else {
            return 1;
        }
        toCoordinates -= extra;
        perc = Math.max((toCoordinates - moveTo) / (toCoordinates - atCoords), 0);
        return perc;
    }

    /**
     * Calculate at and to percentage.
     */
    addRunnable('apply', function (stage, cache) {
        if (cache.at === cache.to) {
            cache.atPerc = 1;
            cache.toPerc = 0;
        } else {
            cache.atPerc = countApplyPercent(
                cache.at, cache.moveCoordinate,
                stage.coordinates, stage.size, stage.displacement
            );
            cache.toPerc = countApplyPercent(
                cache.to, cache.moveCoordinate,
                stage.coordinates, stage.size, stage.displacement
            );
        }
    }, 50);

    /**
     * Set current slide data.
     */
    addRunnable('apply', function (stage, cache) {
        cache.currentData = cache.atPerc > cache.toPerc
            ? {
                from: cache.to,
                fromReal: true,
                isReal: true,
                percent: cache.atPerc,
                percentFrom: cache.toPerc,
                position: cache.at
            }
            : {
                from: cache.at,
                fromReal: true,
                isReal: true,
                percent: cache.toPerc,
                percentFrom: cache.atPerc,
                position: cache.to
            };
    }, 60);

    /**
     * Apply current slide data.
     */
    addRunnable(true, function (stage, cache) {
        if (Slice.hasProperty(cache, 'currentData')) {
            cache.currentData.coordinate = cache.moveCoordinate;
            this.run('setCurrentLive', cache.currentData);
        }
    }, 70);

    /**
     * Check if need to set speed.
     */
    addRunnable('smooth', function (stage, cache) {
        // If distance is less than 3 pixels - it's already smooth enough.
        if (Math.abs(cache.moveDistance) > 3) {
            var speed = Math.max(this.run([this.settings.speedType], {
                from: cache.currentCoord,
                minSpeed: 0,
                to: cache.moveCoordinate
            }, 'speed').speed, cache.speed || 0);
            // If it's more than 40fps (25ms = 40fps) - skip
            if (speed > 25) {
                cache.speed = speed;
            }
        }
    }, 90);

    /**
     * Move stage.
     */
    addRunnable(true, function (stage, cache) {
        var context = this;
        context.moved = true;
        stage.move(cache.moveCoordinate, cache.speed, cache.afterMove);
        cache.result = true;
    }, 100);

    /**
     * Move stage runnables.
     */
    setRunnableType('getDistance');

    /**
     * Set current coordinate.
     */
    addRunnable(true, function (stage, cache) {
        cache.currentCoord = stage.getPosition(this.$stage);
        cache.toCoord = -cache.currentCoord;
        cache.fromCoord = -cache.currentCoord;
    });

    /**
     * Set from/to coordinate.
     */
    addRunnable('from, to', function (stage, cache) {
        cache.currentData = this.run('position, current', {
            isReal: cache.realCurrent,
            position: cache.current
        }, 'position');
    }, 10);
    addRunnable('from', function (stage, cache) {
        cache.fromData = cache.hasOwnProperty('from')
            ? this.run('position, current', {
                isReal: cache.realFrom,
                position: cache.from
            }, 'position')
            : cache.currentData;
    }, 10);
    addRunnable('to', function (stage, cache) {
        cache.toData = cache.hasOwnProperty('to')
            ? this.run('position, current', {
                isReal: cache.realTo,
                position: cache.to
            }, 'position')
            : cache.currentData;
    }, 10);

    /**
     * Set from/to coordinate.
     */
    addRunnable('from', function (stage, cache) {
        var currentData = cache.currentData,
            positionData = cache.fromData;
        cache.fromCoord = -normalizeCoordinate(
            stage, positionData.current, cache.asCurrentFrom
                ? positionData.current
                : currentData.current,
            cache.checkFrom &&
            positionData.current !== currentData.current && positionData.position === currentData.position
        );
    }, 20);
    addRunnable('to', function (stage, cache) {
        var currentData = cache.currentData,
            positionData = cache.toData;
        cache.toCoord = -normalizeCoordinate(
            stage, positionData.current, cache.asCurrentTo
                ? positionData.current
                : currentData.current,
            cache.checkTo &&
            positionData.current !== currentData.current && positionData.position === currentData.position
        );
    }, 20);
    addRunnable('fromCoordinate', function (stage, cache) {
        if (cache.hasOwnProperty('fromCoordinate')) {
            cache.fromCoord = cache.fromCoordinate;
        }
    }, 30);
    addRunnable('toCoordinate', function (stage, cache) {
        if (cache.hasOwnProperty('toCoordinate')) {
            cache.toCoord = cache.toCoordinate;
        }
    }, 30);

    /**
     * Calculate distance from.
     */
    addRunnable(true, function (stage, cache) {
        cache.distance = (cache.reverse
            ? -1
            : 1) * (cache.toCoord - cache.fromCoord);
    }, 50);

    /**
     * Get slide coordinate runnables.
     */
    setRunnableType('getCoordinate');
    // Get coordinate of the slide.
    setQuickRun('getBaseCoordinate', 'base', 'coordinate');
    // Get coordinate of the slide.
    setQuickRun('getSlideCoordinate', 'slide', 'coordinate');
    // Get coordinate of the slide to move to.
    setQuickRun('getMoveToCoordinate', 'moveTo', 'coordinate');

    /**
     * Normalize coordinate.
     *
     * @param {object} stage - Stage object.
     * @param {number} position - Slide position to normalize.
     * @param {number} current - Current slide position.
     * @param {boolean} asCurrent - Pretend position is current.
     * @returns {number} Normalized coordinate.
     */
    function normalizeCoordinate (stage, position, current, asCurrent) {
        var displace = 0;
        if (current === position) {
            displace = -stage.displacement;
        } else if (asCurrent) {
            displace = current > position
                ? stage.displacement + stage.currentShift
                : -(stage.displacement + stage.currentDisplacement);
        } else if (current < position) {
            displace -= stage.currentDisplacement;
        }
        return stage.coordinates[position] + displace;
    }

    /**
     * Get slide coordinate.
     */
    addRunnable('slide', function (stage, cache) {
        var positionData = this.run('position, current', cache, 'position'),
            currentData = this.run('position, current', {
                isReal: cache.realCurrent,
                position: cache.current
            }, 'position');
        cache.coordinate = normalizeCoordinate(
            stage, positionData.current, currentData.current,
            cache.checkPosition &&
            positionData.current !== currentData.current && positionData.position === currentData.position
        );
    });
    addRunnable('base, moveTo', function (stage, cache) {
        var positionData = this.run('position, real', cache, 'position');
        cache.coordinate = stage.coordinates[positionData.real];
    });
    addRunnable('moveTo', function (stage, cache) {
        cache.coordinate -= stage.displacement;
    });

    /**
     * Check slide visibility.
     */
    setRunnableType('slideVisibility');
    // Check if current slide position is visible, base position can be provided or current position will be taken.
    setQuickRun('slideIsVisible', 'position, visible, slide', 'isVisible');
    // Check if current slide position is visible, base position can be provided or current position will be taken.
    setQuickRun('currentIsVisible', 'position, visible, current', 'isVisible');

    /**
     * Set variables for calculations.
     */
    addRunnable(true, function (stage, cache) {
        if (!Slice.hasProperty(cache, 'coordinate')) {
            cache.coordinate = stage.getPosition(this.$stage);
        }
        cache.isVisible = false;
        cache.inView = false;
        cache.current = this.currentPosition;
    });

    /**
     * Set slide position.
     */
    addRunnable('position', function (stage, cache) {
        cache.position = Slice.hasProperty(cache, 'position')
            ? cache.position
            : this.currentPosition;
    });

    /**
     * Check if slide with such position exists.
     * Set slide element.
     */
    addRunnable('position', function (stage, cache) {
        if (!cache.position || cache.position < 1 || cache.position > stage.$slides.length) {
            return false;
        }
        cache.$slide = stage.$slides.eq(cache.position - 1);
    }, 20);

    /**
     * Set slide coordinates and size.
     */
    addRunnable(true, function (stage, cache) {
        cache.slideCoord = stage.coordinates[cache.position];
        cache.slideSize = stage.slideSize;
        cache.start = cache.slideCoord + stage.displaceFirst - stage.slideSpacing;
        cache.end = cache.start + stage.viewportSize;
    }, 30);

    /**
     * Adjust start and end coordinates.
     */
    addRunnable(true, function (stage, cache) {
        cache.start -= cache.slideSize;
        if (cache.position > cache.current) {
            cache.end -= stage.currentDisplacement;
        } else if (cache.position < cache.current) {
            cache.start += stage.currentDisplacement;
        }
    }, 40);

    /**
     * Check is slide is visble.
     */
    addRunnable(true, function (stage, cache) {
        cache.isVisible = cache.coordinate > cache.start && cache.coordinate < cache.end;
    }, 50);

    /**
     * Get slides.
     */
    setRunnableType('slide');
    // Get visible slides.
    setQuickRun('getVisibleSlides', 'visible, coordinate', '$visible');
    // Get in view slides.
    setQuickRun('getInViewSlides', 'inView, coordinate', '$inView');

    /**
     * Set coordinate.
     */
    addRunnable('coordinate', function (stage, cache) {
        if (!Slice.hasProperty(cache, 'coordinate') ||
            typeof cache.coordinate === 'undefined') {
            cache.coordinate = cache.coordinate = stage.getPosition(this.$stage);
        }
    });

    /**
     * Check if has coordinates.
     * Set current slide position.
     * Set start and end position.
     */
    addRunnable('visible, inView', function (stage, cache) {
        if (!stage.coordinates) {
            cache.$visible = $([]);
            cache.$inView = $([]);
            return false;
        }
        cache.start = cache.coordinate - stage.slideSpacing - 1;
        cache.end = cache.coordinate;
        cache.currentSpacing = 0;
        cache.current = this.run('getRealPosition');
    });

    /**
     * Get first slide position at given coordinate.
     *
     * @param {object} stage - Stage object.
     * @param {number} coordinate - Stage coordinate.
     * @param {number} current - Current slide position.
     * @param {number} currentSpacing - Current slide spacing.
     * @param {boolean} isStart - Checking start position.
     * @param {number} startPosition - Position to start at.
     * @returns {number} Slide position.
     */
    function getFirstAt (stage, coordinate, current, currentSpacing, isStart, startPosition) {
        var coord = coordinate + stage.slideSpacing,
            position = startPosition || 1,
            currentPosition = current || 1,
            toPosition = currentPosition - 1,
            check = coord,
            coordinates = stage.baseCoordinates;
        while (position < toPosition) {
            if (coordinates[position + 1] <= check) {
                return position;
            }
            ++position;
        }
        toPosition = stage.$slides.length;
        if (position < currentPosition) {
            check = coord + currentSpacing;
            if (coordinates[position + 1] <= check) {
                return position;
            }
            ++position;
        }
        if (isStart && position === currentPosition && position < toPosition) {
            check = coord - currentSpacing + stage.currentDisplacement;
            if (coordinates[position + 1] <= check) {
                return position;
            }
            ++position;
        }
        check = coord + stage.currentDisplacement;
        while (position < toPosition) {
            if (coordinates[position + 1] <= check) {
                return position;
            }
            ++position;
        }
        return position;
    }

    /**
     * Get visible slides.
     */
    addRunnable('visible', function (stage, cache) {
        cache.end -= stage.viewportSize;
        var start = getFirstAt(stage, cache.start, cache.current, cache.currentSpacing, true),
            end = getFirstAt(stage, cache.end, cache.current, cache.currentSpacing, false, start);
        cache.$visible = stage.$slides.slice(start - 1, end);
    }, 10);

    /**
     * Get in view slides.
     */
    addRunnable('inView', function (stage, cache) {
        cache.start -= stage.displaceView;
        cache.end -= stage.displaceView + stage.viewSize;
        var start = getFirstAt(stage, cache.start, cache.current, cache.currentSpacing, true),
            end = getFirstAt(stage, cache.end, cache.current, cache.currentSpacing, false, start);
        cache.$inView = stage.$slides.slice(start - 1, end);
    }, 10);


    /**
     * Calculate speed runnables.
     */
    setRunnableType('speed');

    /**
     * Set base speed.
     * Set speed index.
     * Set slides spacing.
     */
    addRunnable(true, function (stage, cache) {
        cache.baseSpeed = Slice.hasProperty(cache, 'baseSpeed')
            ? cache.baseSpeed
            : this.settings.speed;
        cache.minSpeed = Slice.hasProperty(cache, 'minSpeed')
            ? cache.minSpeed
            : this.settings.minSpeed;
        cache.maxSpeed = Slice.hasProperty(cache, 'maxSpeed')
            ? cache.maxSpeed
            : this.settings.maxSpeed;
        cache.index = 0;
        cache.spacing = stage.slideSpacing;
    });

    /**
     * Set base and speed for calculations.
     */
    addRunnable('flat', function (stage, cache) {
        cache.base = cache.baseSpeed;
        cache.speed = cache.base;
        return false;
    });

    /**
     * Set start slide position and its coordinate.
     */
    addRunnable('from', function (stage, cache) {
        cache.start = cache.from;
        cache.from = stage.coordinates[cache.from] - stage.displacement;
    }, 10);

    /**
     * Set end slide position and its coordinate.
     */
    addRunnable('to', function (stage, cache) {
        cache.end = cache.to;
        cache.to = stage.coordinates[cache.to];
    }, 10);

    /**
     * Set default coordinates.
     */
    addRunnable(true, function (stage, cache) {
        cache.from = Slice.hasProperty(cache, 'from')
            ? cache.from
            : stage.getPosition(this.$stage);
        cache.to = (Slice.hasProperty(cache, 'to')
            ? cache.to
            : 0) - stage.displacement;
    }, 10);

    /**
     * Calculate speed index.
     */
    addRunnable('uniform, uniformfixed', function (stage, cache) {
        if (!Slice.hasProperty(cache, 'speedBase')) {
            cache.speedBase = this.settings.speedBase;
        }
        cache.index = Math.abs(cache.from - cache.to) / cache.speedBase;
    }, 20);
    addRunnable('viewport, viewportfixed', function (stage, cache) {
        cache.index = Math.abs(cache.from - cache.to) / stage.viewportSize;
    }, 20);
    addRunnable('slide, slidefixed', function (stage, cache) {
        cache.index = Math.abs(cache.from - cache.to) / (stage.slideSize + cache.spacing);
    }, 20, 'fixedsize');
    addRunnable('fixed, slidefixed, viewportfixed, uniformfixed', function (stage, cache) {
        cache.index = Math.ceil(cache.index);
    }, 30);
    addRunnable('static', function (stage, cache) {
        cache.index = 1;
    }, 30);

    /**
     * Calculate speed and base speed.
     */
    addRunnable(true, function (stage, cache) {
        cache.speed = Math.max(
            Math.min(cache.baseSpeed * cache.index, cache.maxSpeed || Infinity),
            cache.minSpeed || 0
        );
    }, 40);
    addRunnable('base', function (stage, cache) {
        cache.base = Math.max(Math.min(cache.speed, cache.baseSpeed, cache.maxSpeed || Infinity), cache.minSpeed || 0);
    }, 50);

    /**
     * Manage animation runnables.
     */
    setRunnableType('animation');
    // Finish current animations.
    setQuickRun('finishAnimation', 'finish');
    // Stop current animations.
    setQuickRun('stopAnimation', 'stop');

    /**
     * Finish current animations.
     */
    addRunnable('finish', function () {
        var context = this;
        if (context.animation) {
            context.animation.reject('finish');
            context.animation = false;
        }
        if (context.state.is('transition')) {
            context.onTransitionEnd();
        }
    }, 10);

    /**
     * Stop current animations.
     */
    addRunnable('stop', function () {
        var context = this;
        if (context.animation) {
            context.animation.reject('stop');
            context.animation = false;
        }
        context.state.leave('transition');
    }, 10);

    /**
     * User actions runnables.
     */
    setRunnableType('useraction');
    // User active, i.e. user did somethig to slider.
    setQuickRun('userActive', 'active');
    // User inactive, i.e. user does nothing to slider.
    setQuickRun('userInactive', 'inactive');

    /**
     * Rewind user inactive status.
     */
    addRunnable('active', function () {
        var context = this;
        if (context.inactiveTimeout) {
            clearTimeout(context.inactiveTimeout);
        }
        context.inactiveTimeout = setTimeout(function () {
            context.run('userInactive');
            context.inactiveTimeout = false;
        }, context.settings.userInactiveDelay);
    });

    /**
     * Toggle inactive class to the slider element.
     */
    addRunnable('inactive, active', function (stage, cache) {
        var userInactiveClass = this.settings.userInactiveClass;
        if (userInactiveClass) {
            this.$element.toggleClass(userInactiveClass, !cache.list.active);
        }
    });

    /**
     * Destroy slider.
     */
    setRunnableType('destroy');
    // Destroy slider.
    setQuickRun('destroy', 'destroy');

    /**
     * Enter destroy state.
     */
    addRunnable('destroy', function () {
        var context = this;
        context.state.enter('destroy');
    }, 100);

    /**
     * Cleanup slider.
     */
    addRunnable('destroy', function () {
        var context = this;
        if (context.animation) {
            context.animation.reject('destroy');
            context.animation = false;
        }
        context.$element.off('.slice');
        context.$element.data('slice.slider', null);
    }, 100);

    // Cleanup runnable type.
    resetRunnableType();

    /**
     * Extend jQuery with Slice Slider.
     *
     * @param {object} [options] - Options.
     * @returns {jQuery} Provided jQuery elements.
     */
    function fnSlice (options) {
        var args = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
            var $el = $(this),
                slice = $el.data('slice.slider');
            if (!slice) {
                slice = new SliceSlider($el, typeof options === 'object' && options);
            }
            if (typeof options === 'string' && options.charAt(0) !== '_') {
                slice[options].apply(slice, args);
            }
        });
    }
    Slice.Slider = SliceSlider;
    $.fn.sliceSlider = fnSlice;

    /**
     * Set global Slice functionality.
     */
    // Default options.
    fnSlice.Defaults = {};
    // Plugins list.
    fnSlice.Plugins = {};
    // Quick slider functions list.
    fnSlice.fn = {};
    // Parse options string/object.
    fnSlice.parseOptions = parseOptions;
    // Slider theme.
    fnSlice.addTheme = addTheme;
    fnSlice.removeTheme = removeTheme;
    fnSlice.getTheme = getTheme;
    // CSS properties list.
    fnSlice.cssProps = cssProps;
    // Enable/Disable slider autoinit.
    fnSlice.autoInit = true;

    /**
     * Auto-init Slice Slider.
     */
    $(function () {
        if (fnSlice.autoInit) {
            $('[data-slice-slider]').sliceSlider({
                optionsProvider: 'sliceSlider'
            });
        }
    });
}));
