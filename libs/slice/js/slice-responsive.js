/* global jQuery, Slice */

/**
 * Slice Slider: Responsive Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Responsive settings
        responsive: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addRunnable = factory.addRunnable;

    /**
     * Get responsive options.
     *
     * @param {jQuery} $element - Element to get options from.
     * @param {string} optionsProvider - Options provider.
     * @returns {Array|boolean} Responsive otions or false.
     */
    function getResponsiveOptions ($element, optionsProvider) {
        var strLen = optionsProvider.length;
        if (!optionsProvider) {
            return false;
        }
        var data = $element.data(),
            list = [];
        $.each(data, function (key, val) {
            var keyStr = key + '';
            if (keyStr.lastIndexOf(optionsProvider, 0) === 0) {
                var opts = fnSlice.parseOptions(val),
                    breakpoint = opts.hasOwnProperty('breakpoint')
                        ? opts.breakpoint
                        : parseInt(keyStr.slice(strLen));
                if (!isNaN(breakpoint)) {
                    opts.breakpoint = Math.abs(breakpoint);
                    list.push(opts);
                }
            }
        });
        return list.length
            ? list
            : false;
    }

    /**
     * Responsive plugin constructor.
     *
     * @class SliceResponsive
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceResponsive (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
        var options = getResponsiveOptions(
            slice.$element,
            slice.options.optionsProvider || slice.defaults.optionsProvider
        );
        if (options) {
            slice.options.responsive = $.extend(slice.options.responsive || {}, options);
        }
    }

    /**
     * Creat breakpoint.
     *
     * @param {number} breakpoint - The breakpoint value.
     * @param {object} options - Raw breakpoint options.
     * @returns {object} - Breakpoint settings.
     */
    function createBreakpoint (breakpoint, options) {
        var opt = $.extend({}, options || {});
        if (opt.responsive) {
            delete opt.responsive;
        }
        opt.breakpoint = breakpoint;
        return {
            breakpoint: breakpoint,
            options: opt
        };
    }

    /**
     * Create breakpoints.
     *
     * @param {object} options - Base options.
     * @param {object|Array} responsive - Responsive options.
     * @returns {Array} - List of settings for different breakpoints.
     */
    function createBreakpoints (options, responsive) {
        var breakpoints = {};
        // Add base breakpoint
        breakpoints[0] = createBreakpoint(0, options);
        breakpoints[0].defaults = {};
        if (responsive) {
            $.each(responsive, function (i, breakpointSettings) {
                if (breakpointSettings.hasOwnProperty('breakpoint') &&
                    breakpointSettings.breakpoint === parseInt(breakpointSettings.breakpoint, 10)) {
                    breakpoints[breakpointSettings.breakpoint] = createBreakpoint(
                        breakpointSettings.breakpoint,
                        breakpointSettings
                    );
                }
            });
        }
        // Create array of breakpoints sorted by 'breakpoint'
        breakpoints = Object.keys(breakpoints)
            .map(function (key) {
                return breakpoints[key];
            })
            .sort(function (item1, item2) {
                return item1.breakpoint - item2.breakpoint;
            });
        return breakpoints;
    }

    /**
     * Build breakpoint.
     *
     * @param {Array} breakpoints - List of breakpoints.
     * @param {number} index - Index of breakpoint to build.
     * @returns {object} Breakpoint with built options.
     */
    function buildBreakPoint (breakpoints, index) {
        var breakpoint = breakpoints[index];
        if (!breakpoint.builtOptions) {
            if (index && !breakpoint.defaults) {
                for (var i = 1; i <= index; i++) {
                    if (!breakpoints[i].defaults) {
                        breakpoints[i].defaults = $.extend({}, breakpoints[i - 1].defaults, breakpoints[i - 1].options);
                    }
                }
            }
            breakpoint.builtOptions = $.extend({}, breakpoint.defaults, breakpoint.options);
        }
        return breakpoint;
    }

    /**
     * Build theme breakpoint.
     *
     * @param {Array} breakpoints - List of breakpoints.
     * @param {number} index - Index of breakpoint to build.
     * @param {number} resolution - Resolution for breakpoint.
     * @param {object} currentBreakpoint - Current active breakpoint to check with.
     * @returns {object|boolean} Current active breakpoint or false if equal to currentBreakpoint.
     */
    function buildThemeBreakPoint (breakpoints, index, resolution, currentBreakpoint) {
        var ind = index || 0,
            breakpoint = breakpoints[ind];
        if (!breakpoint.builtOptions) {
            if (ind && !breakpoint.defaults) {
                for (var i = 1; i <= ind; i++) {
                    if (!breakpoints[i].defaults) {
                        breakpoints[i].defaults = $.extend({}, breakpoints[i - 1].defaults, breakpoints[i - 1].options);
                    }
                }
            }
            breakpoint.builtOptions = $.extend({}, breakpoint.defaults, breakpoint.options);
        }
        var builtOptions = breakpoint.builtOptions,
            breakpointValue = breakpoint.builtOptions.breakpoint,
            theme = builtOptions.theme,
            themeBreakpoint;
        theme = fnSlice.getTheme(theme);
        if (theme) {
            if (!theme.breakpoints) {
                theme.breakpoints = createBreakpoints(theme.defaults, theme.defaults.responsive);
            }
            themeBreakpoint = getBreakpoint(theme.breakpoints, resolution || 0, currentBreakpoint);
            breakpointValue = themeBreakpoint && breakpointValue < themeBreakpoint.breakpoint
                ? themeBreakpoint.breakpoint
                : breakpointValue;
        }
        return currentBreakpoint === breakpointValue
            ? false
            : $.extend({}, themeBreakpoint || {}, breakpoint.builtOptions, {
                breakpoint: breakpointValue
            });
    }

    /**
     * Get breakpoint.
     *
     * @param {Array} breakpoints - List of breakpoints.
     * @param {number} resolution - Resolution for breakpoint.
     * @param {object} currentBreakpoint - Current active breakpoint to check with.
     * @returns {object} Breakpoint.
     */
    function getBreakpoint (breakpoints, resolution, currentBreakpoint) {
        var res = resolution || 0;
        for (var i = breakpoints.length - 1; i >= 0; i--) {
            if (res >= breakpoints[i].breakpoint) {
                return buildThemeBreakPoint(breakpoints, i, res, currentBreakpoint);
            }
        }
        return buildThemeBreakPoint(breakpoints, 0, res, currentBreakpoint);
    }

    /**
     * Setup responsive settings.
     */
    SliceResponsive.prototype.preSetup = function () {
        var context = this,
            slice = context.slice,
            responsive = slice.options.hasOwnProperty('responsive')
                ? slice.options.responsive
                : slice.defaults.responsive || false;
        context.breakpoints = createBreakpoints(slice.options, responsive);
        context.breakpoints[0].defaults.theme = slice.defaults.theme || false;

        context.checkBreak();
        context.breakpoint.setup = false;
        context.slice.setup(context.breakpoint);
        context.slice.state.leave('setup');
    };

    /**
     * Check slider for current resolution.
     *
     * @returns {boolean} Wheater new breakpoint was set.
     */
    SliceResponsive.prototype.checkBreak = function () {
        var context = this;
        context.resolution = $(window).width();
        var breakpoint = getBreakpoint(context.breakpoints, context.resolution, context.breakpoint);
        if (!context.breakpoint || context.breakpoint.breakpoint !== breakpoint.breakpoint) {
            context.breakpoint = $.extend({
                setup: true
            }, context.slice.defaults, breakpoint);
            return true;
        }
        return context.breakpoint.setup;
    };

    /**
     * Resize runnables.
     */
    factory.setRunnableType('resize');

    /**
     * Check current resolution.
     */
    addRunnable('check', function (stage, cache) {
        var responsive = this.plugins.sliceResponsive;
        if ($(window).width() !== responsive.resolution) {
            cache.result = true;
            return false;
        }
    });

    /**
     * Resoulution changed and there is new breakpoint for it that run setup again.
     */
    addRunnable('resize', function () {
        var responsive = this.plugins.sliceResponsive;
        if ($(window).width() !== responsive.resolution && responsive.checkBreak()) {
            responsive.breakpoint.setup = false;
            this.setup(responsive.breakpoint);
        }
    }, 60);

    /**
     * Cleanup.
     */
    factory.resetRunnableType();

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceResponsive = SliceResponsive;

    /**
     * Add functions to work with responsive options and breakpoints.
     */
    fnSlice.getResponsiveOptions = getResponsiveOptions;
    fnSlice.buildBreakPoint = buildBreakPoint;
    fnSlice.createBreakpoint = createBreakpoint;
    fnSlice.createBreakpoints = createBreakpoints;
}));
