/* global jQuery, Slice */

/**
 * Slice Slider: Keys Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Custom key controls
        customKeys: false,
        // Disable prev/next slide keys on fields (input, textarea)
        keyScrollNoField: true,
        // Prevent prev/next slide keys browser default behaviour
        keyScrollPreventDefault: true,
        // Enable/disable slider keys controls
        keysControl: true,
        // Keys element
        keysElement: 'self',
        // Number of slides to scroll, when set to false will scroll default ammount of slides
        keysSlidesToScroll: false,
        // Next slide key
        nextKey: 'auto',
        // Prev slide key
        prevKey: 'auto'
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable;

    /**
     * Keys.
     */
    var keyBase = {
        action: null,
        actionData: null,
        caseSensitive: false,
        defaultKey: null,
        ignoreElements: false,
        preventDefault: false,
        required: false,
        single: true,
        unique: false
    };
    fnSlice.keyDefaults = typeof fnSlice.keyDefaults === 'object'
        ? fnSlice.keyDefaults
        : $.extend(true, {}, keyBase);
    fnSlice.codesList = {
        13: 'Enter',
        27: 'Escape',
        32: ' ',
        33: 'PageUp',
        34: 'PageDown',
        35: 'End',
        36: 'Home',
        37: 'ArrowLeft',
        38: 'ArrowUp',
        39: 'ArrowRight',
        40: 'ArrowDown',
        45: 'Insert',
        46: 'Delete',
        8: 'Backspace',
        9: 'Tab'
    };
    fnSlice.charList = {
        Enter: '\n'
    };

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'keysControl';

    /**
     * Stage plugin constructor.
     *
     * @class SliceStage
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceKeysControls (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceKeysControls.prototype.setup = function () {
        var settings = this.slice.settings;
        settings.workers.keysControl = !!settings.keysControl;
    };

    /**
     * Create key data from event.
     *
     * @param {KeyboardEvent} e - KeyboardEvent object.
     * @returns {object} Key data.
     */
    function createKeyEventData (e) {
        var key,
            char = null;
        if (typeof e.key !== 'undefined') {
            // Some browsers, like IE11, has different key name, so check for keyCode first.
            key = typeof e.keyCode !== 'undefined' && fnSlice.codesList.hasOwnProperty(e.keyCode)
                ? fnSlice.codesList[e.keyCode]
                : e.key;
        } else if (typeof e.keyCode !== 'undefined') {
            key = fnSlice.codesList.hasOwnProperty(e.keyCode)
                ? fnSlice.codesList[e.keyCode]
                : String.fromCharCode(e.keyCode);
        }
        if (fnSlice.charList.hasOwnProperty(key)) {
            char = fnSlice.charList[key];
        } else if (key && key.length === 1) {
            char = key;
        }
        return {
            char: char,
            key: key
        };
    }


    /**
     * Add key by its value to keys list.
     *
     * @param {string} keyVal - Key value.
     * @param {object} key - Key data/object.
     * @param {object} keys - Existing keys.
     * @param {boolean} single - Anly add once.
     * @returns {boolean} Keys added or not.
     */
    function addKey (keyVal, key, keys, single) {
        if (!(keyVal && typeof keyVal === 'string')) {
            return false;
        }
        var unique = !!key.unique,
            uniqueFilter = Array.isArray(key.unique)
                ? function (keyStr) {
                    return Slice.Tags.contains(this, keyStr);
                }.bind(Slice.toObject(key.unique))
                : false,
            existingKeys = keys[keyVal],
            hasExisting = existingKeys && existingKeys.length;
        // Check existing keys for unique and required.
        if (hasExisting) {
            for (var i = existingKeys.length - 1; i >= 0; i--) {
                if (existingKeys[i].unique &&
                    (existingKeys[i].unique === true ||
                    uniqueFilter && Slice.some(existingKeys[i].unique, uniqueFilter))) {
                    // If key is required and existing isn't than remove existing
                    if (key.addedCount === 0 && key.required && !existingKeys[i].required) {
                        --existingKeys[i].addedCount;
                        existingKeys[i].splice(i, 1);
                    } else {
                        return false;
                    }
                }
            }
        }
        if (!keys[keyVal] || hasExisting && unique && !uniqueFilter) {
            keys[keyVal] = [];
        }
        keys[keyVal].push(key);
        if (!single && !key.caseSensitive && keyVal.length === 1) {
            addKey(keyVal === keyVal.toUpperCase()
                ? keyVal.toLowerCase()
                : keyVal.toUpperCase(), key, keys, true);
        }
        ++key.addedCount;
        return true;
    }

    /**
     * Create key settings.
     *
     * @param {object} slider - Slice slider object.
     * @param {object} options - Key options.
     * @param {object} keys - Existing keys.
     * @returns {object} Keys settings.
     */
    function createKey (slider, options, keys) {
        var key = $.extend(true, {}, keyBase, fnSlice.keyDefaults, options),
            keysArr;
        key.addedCount = typeof key.addedCount === 'number' && key.addedCount > 0
            ? key.addedCount
            : 0;
        key.unique = key.unique
            ? Slice.Tags.fromValue(key.unique)
            : false;
        if (key.key === 'auto' || key.key === 'default') {
            keysArr = Slice.Tags.fromValue(typeof key.defaultKey === 'function'
                ? key.defaultKey.call(slider, key)
                : key.defaultKey, true);
        } else {
            keysArr = Slice.Tags.fromValue(key.key, true);
        }
        for (var i = 0; i < keysArr.length; i++) {
            if (addKey(keysArr[i], key, keys) && key.single) {
                break;
            }
        }
        return key;
    }


    /**
     * Create keys settings.
     *
     * @param {object} slider - Slice slider object.
     * @param {object} list - List with keys options.
     * @returns {object} Keys settings.
     */
    function createKeys (slider, list) {
        var keys = {};
        $.each(list, function (i, value) {
            createKey(slider, value, keys);
        });
        return keys;
    }

    /**
     * Remove keys controls.
     */
    addWorker('settings', function () {
        var controlKeys = this.controlKeys;
        if (controlKeys) {
            document.removeEventListener('keydown', controlKeys.keydown, true);
            document.removeEventListener('keyup', controlKeys.keyup, true);
        }
        delete this.controlKeys;
    }, 10);

    factory.addRequirement(requirement);

    /**
     * Add keys list.
     */
    addWorker('settings', function (stage, cache) {
        var settings = this.settings,
            ignoreElements = settings.keyScrollNoField
                ? 'textarea, input, button'
                : false,
            nextKey = {
                action: 'next',
                defaultKey: function () {
                    return this.settings.workers.vertical
                        ? 'ArrowDown'
                        : 'ArrowRight';
                },
                ignoreElements: ignoreElements,
                key: settings.nextKey,
                preventDefault: settings.keyScrollPreventDefault
            },
            prevKey = {
                action: 'prev',
                defaultKey: function () {
                    return this.settings.workers.vertical
                        ? 'ArrowUp'
                        : 'ArrowLeft';
                },
                ignoreElements: ignoreElements,
                key: settings.prevKey,
                preventDefault: settings.keyScrollPreventDefault
            };
        if (settings.keysSlidesToScroll) {
            nextKey.action = function () {
                this.to(this.currentPosition + this.settings.keysSlidesToScroll);
            };
            prevKey.action = function () {
                this.to(this.currentPosition - this.settings.keysSlidesToScroll);
            };
        }
        cache.keysList = $.extend(true, {
            nextKey: nextKey,
            prevKey: prevKey
        }, settings.customKeys || {});
    });

    /**
     * Add keys controls.
     */
    addWorker('settings', function (stage, cache) {
        var context = this,
            controlKeys = {
                keydown: function (e) {
                    if ($(e.target).is(this.controlKeys.element) ||
                        $(e.target).closest(this.controlKeys.element).length ||
                        $(':hover').filter(this.controlKeys.element).length) {
                        this.run('keydown', $.extend({
                            event: e
                        }, createKeyEventData(e)));
                    }
                }.bind(context),
                keyup: function (e) {
                    if ($(e.target).is(this.controlKeys.element) ||
                        $(e.target).closest(this.controlKeys.element).length ||
                        $(':hover').filter(this.controlKeys.element).length) {
                        this.run('keyup', $.extend({
                            event: e
                        }, createKeyEventData(e)));
                    }
                }.bind(context)
            };
        var $element,
            element = false,
            keysElement = context.settings.keysElement;
        switch (keysElement) {
        case 'self':
            element = context.$element[0];
            break;
        case 'global':
        case 'document':
            element = document;
            break;
        default:
            $element = context.$element.find(keysElement);
            if (!$element.length) {
                $element = $(keysElement);
            }
            if (!$element.length) {
                $element = context.$element;
            }
            element = $element[0];
            break;
        }
        document.addEventListener('keydown', controlKeys.keydown, true);
        document.addEventListener('keyup', controlKeys.keyup, true);
        controlKeys.element = element;
        controlKeys.keys = createKeys(context, cache.keysList);
        context.controlKeys = controlKeys;
    }, 10);

    /**
     * User actions runnables.
     */
    factory.setRunnableType('useraction');
    // Run keydow flow.
    factory.setQuickRun('keydown', 'keydown, keys');
    // Run keyup flow.
    factory.setQuickRun('keyup', 'keyup, keys');

    /**
     * Store keys list.
     */
    addRunnable('keys', function (stage, cache) {
        if (cache.key && this.controlKeys.keys[cache.key]) {
            cache.keyList = this.controlKeys.keys[cache.key];
        }
    });

    /**
     * Handle keyup keys actions.
     */
    addRunnable('keyup', function (stage, cache) {
        if (!cache.keyList) {
            return;
        }
        var context = this,
            event = cache.event,
            ignoreElements,
            keyData,
            actionData;
        for (var i = 0; i < cache.keyList.length; i++) {
            keyData = cache.keyList[i];
            ignoreElements = keyData.ignoreElements;
            if (!event || !keyData.ignoreElements ||
                !($(event.target).is(ignoreElements) ||
                $(':hover').filter(ignoreElements).length)) {
                switch (typeof keyData.actionData) {
                case 'function':
                    actionData = keyData.actionData.call(context, stage, cache, keyData);
                    break;
                default:
                    actionData = keyData.actionData;
                    break;
                }
                switch (typeof keyData.action) {
                case 'string':
                    context.run(keyData.action, actionData);
                    break;
                case 'function':
                    keyData.action.call(context, actionData, keyData);
                    break;
                default:
                    break;
                }
                if (event && keyData.preventDefault) {
                    event.preventDefault();
                }
            }
        }
    }, 10);

    /**
     * Handle keydown keys actions.
     */
    addRunnable('keydown', function (stage, cache) {
        if (!cache.keyList) {
            return;
        }
        var event = cache.event,
            ignoreElements,
            keyData;
        for (var i = 0; i < cache.keyList.length; i++) {
            keyData = cache.keyList[i];
            ignoreElements = keyData.ignoreElements;
            if (!event || !keyData.ignoreElements ||
                !($(event.target).is(ignoreElements) ||
                $(':hover').filter(ignoreElements).length)) {
                if (event && keyData.preventDefault) {
                    event.preventDefault();
                }
            }
        }
    }, 10);

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    factory.removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceKeysControls = SliceKeysControls;
}));
