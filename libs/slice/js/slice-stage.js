/* global jQuery, Slice */

/**
 * Slice Slider: Stage Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Stage maximum size to distribute between shown slides, allowed units: %, px.
        stageMaxSize: false,
        // Size padding, allowed units: %, px.
        stagePadding: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'stagePadding';

    /**
     * Stage plugin constructor.
     *
     * @class SliceStage
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceStage (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceStage.prototype.setup = function () {
        var settings = this.slice.settings;
        settings.workers.stagePadding = !!(settings.stagePadding || settings.stageMaxSize);
        settings.workers.stageMaxSize = !!settings.stageMaxSize;
    };

    /**
     * Add workers and runnables for stage mode only.
     */
    factory.addRequirement(requirement);

    /**
     * Calculate stage padding.
     */
    addWorker('size, settings', function (stage) {
        stage.padding = Slice.Unit.toPx(this.settings.stagePadding || 0, stage.viewportSize);
    }, 10);

    /**
     * Adjust stage padding to maximum stage size.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        var size = Slice.Unit.toPx(this.settings.stageMaxSize, stage.viewportSize);
        if (cache.viewSize > size) {
            size = Math.ceil((cache.viewSize - size) / 2);
            if (size > stage.padding) {
                stage.padding = size;
            }
        }
    }, 11, 'stageMaxSize');

    /**
     * Adjust view size.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.viewSize -= stage.padding * 2;
    }, 15);

    /**
     * Adjust stage displacement, used to calculate slides coordinates.
     */
    addWorker('size, slides, settings', function (stage) {
        if (stage.padding > 0) {
            stage.displaceFirst -= stage.padding;
            stage.displaceView += stage.padding;
        }
    }, 81);

    /**
     * Get size runnables.
     */
    factory.setRunnableType('size');

    /**
     * Calculate stage padding.
     */
    addRunnable('height', function (stage, cache) {
        cache.padding = (cache.padding || 0) +
            Slice.Unit.revPx(this.settings.stagePadding || 0, cache.viewSize);
    }, 29, 'vertical');

    /**
     * Adjust stage padding to maximum stage size.
     */
    addRunnable('stagePadding', function (stage, cache) {
        var size = Slice.Unit.revPx(this.settings.stageMaxSize, cache.viewSize);
        if (cache.viewSize > size) {
            size = Math.ceil((cache.viewSize - size) / 2);
            if (size > stage.padding) {
                stage.padding = size;
            }
        }
    }, 29, 'stageMaxSize, vertical');

    /**
     * Adjust view size.
     */
    addRunnable('height', function (stage, cache) {
        cache.viewSize += cache.padding * 2;
    }, 29, 'vertical');

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    factory.removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceStage = SliceStage;
}));
