/* global jQuery, Slice */

/**
 * Slice Slider: Lazy Load Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 * @typedef {object} SlicePostponed Slice postponed class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Defines how to load active slides.
        lazyActive: false,
        // Number of active loads when lazyQueue is set.
        lazyActiveLoads: 3,
        // Defines how to load current slide.
        lazyCurrent: 'lazy',
        // Enable/disable lazy loading queueing.
        lazyQueue: 'slider',
        // Number of slides to load forward and backward.
        lazyRadius: false,
        // Enable/disable lazy loading, when set to 'auto' will ignore other options
        lazyload: 'auto'
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addRunnable = factory.addRunnable,
        addRequirement = factory.addRequirement,
        removeRequirement = factory.removeRequirement;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'lazyload';

    /**
     * Requirements for some cases.
     */
    var tempRequirement;

    /**
     * Create postponed for global lazyloading.
     */
    fnSlice.lazyQueue = new Slice.Postponed(3);

    /**
     * Create SlicePostponed to load image.
     *
     * @param {HTMLElement|jQuery} element - Image element.
     * @param {string} src - Real image src.
     * @returns {SlicePostponed} Postponed object.
     */
    function loadImage (element, src) {
        var $element = $(element),
            postponed = $element.data('slice_load_postponed'),
            imageSrc = src;
        // Check if already loading
        if (!postponed) {
            postponed = new Slice.Postponed();
            $element.data('slice_load_postponed', postponed);
            postponed
                .add(function () {
                    var image = new Image();
                    jQuery(image)
                        .on({
                            error: function () {
                                postponed.reject();
                                jQuery(this).remove();
                            },
                            load: function () {
                                $element.attr('src', imageSrc);
                                postponed.resolve(imageSrc);
                                jQuery(this).remove();
                            }
                        })
                        .attr('src', imageSrc);
                    return postponed;
                });
        }
        return postponed;
    }

    /**
     * Handle slide loading.
     *
     * @param {HTMLElement|jQuery} slide - Slide element.
     * @param {object} list - List to add loadings.
     * @param {string} queue - Default queue name.
     * @param {string} order - Default loading order.
     * @returns {object} List of loadings per queue type.
     */
    function handleSlideLoad (slide, list, queue, order) {
        var $slide = $(slide),
            result = list || {};
        // Find lazy images and create postponed loadings for tham
        $slide.find('img[data-slice-lazy-src]')
            .each(function (n, element) {
                var $element = $(element),
                    imageLoad = loadImage($element, $element.data('sliceLazySrc')),
                    queueName = $element.data('sliceLazyChain') || queue;
                if (queueName) {
                    var name, action;
                    // Set loading queue
                    switch (queueName) {
                    case 'slide':
                        // Falls through
                    case 'lazyGlobal':
                        // Falls through
                    case 'slider':
                        name = queueName;
                        break;
                    default:
                        name = 'slide';
                        break;
                    }
                    // Set order
                    switch (order) {
                    case 'lazy':
                        // Falls through
                    case 'immidiate':
                        // Falls through
                    case 'forward':
                        action = order;
                        break;
                    default:
                        action = 'lazy';
                        break;
                    }
                    if (!result[name]) {
                        result[name] = {};
                    }
                    if (!result[name][action]) {
                        result[name][action] = [];
                    }
                    result[name][action].push(imageLoad);
                } else {
                    imageLoad.run();
                }
            });
        return result;
    }

    /**
     * Simple slide loading.
     *
     * @param {HTMLElement|jQuery} slide - Slide element.
     * @param {Array} list - List to add loadings.
     * @returns {Array} List of loadings per queue type.
     */
    function simpleSlideLoad (slide, list) {
        var loads = list || [];
        $(slide).find('img[data-slice-lazy-src]')
            .each(function (n, element) {
                var $element = $(element),
                    imageLoad = loadImage($element, $element.data('sliceLazySrc'));
                loads.push(imageLoad);
            });
        return loads;
    }

    /**
     * Lazyload plugin constructor.
     *
     * @class SliceLazyload
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceLazyload (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
        this.queue = new Slice.Postponed();
    }

    /**
     * Setup settings.
     */
    SliceLazyload.prototype.setup = function () {
        var context = this,
            settings = context.slice.settings;
        if (settings.lazyload) {
            settings.workers.lazyload = true;
            switch (settings.lazyload) {
            case 'auto':
                // Falls through
            case 'smart':
                settings.workers.lazyauto = true;
                context.queue.setMaxActive(3);
                break;
            default:
                settings.workers.lazymanual = true;
                context.queue.setMaxActive(settings.lazyActiveLoads);
                settings.workers.lazyrange = !!settings.lazyRadius;
                break;
            }
        }
    };

    /**
     * Add runnables for lazyloading mode only.
     */
    addRequirement(requirement);

    /**
     * Set slide position runnables.
     */
    factory.setRunnableType('setPosition');

    /**
     * Add runnables for manual lazy loading only.
     */
    tempRequirement = 'lazymanual';
    addRequirement(tempRequirement);

    /**
     * Add current slide loads to queue.
     */
    addRunnable('current', function (stage, cache) {
        var settings = this.settings,
            $current = stage.$slides.eq(cache.real - 1);
        cache.lazyload = $current.data('slice_lazyload');
        if (!cache.lazyload) {
            cache.lazyload = new Slice.Postponed(settings.lazyActiveLoads);
            $current.data('slice_lazyload', cache.lazyload);
        }
        cache.lazyloads = {};
        handleSlideLoad($current, cache.lazyloads, settings.lazyQueue, settings.lazyCurrent);
        if (cache.start !== cache.end) {
            for (var i = cache.start; i <= cache.end; i++) {
                if (i !== cache.real) {
                    handleSlideLoad(
                        stage.$slides.eq(i - 1),
                        cache.lazyloads, settings.lazyQueue, settings.lazyActive || settings.lazyCurrent
                    );
                }
            }
        }
    }, 100);

    /**
     * Add in range slides loads to queue.
     */
    addRunnable('current', function (stage, cache) {
        var settings = this.settings,
            range = settings.lazyRadius,
            max = stage.$slides.length,
            i, loadIndex;
        for (i = 0; i < range; i++) {
            loadIndex = cache.end + i;
            if (max > loadIndex) {
                handleSlideLoad(stage.$slides.eq(loadIndex), cache.lazyloads, settings.lazyQueue);
            }
            loadIndex = cache.start - i - 2;
            if (loadIndex >= 0) {
                handleSlideLoad(stage.$slides.eq(loadIndex), cache.lazyloads, settings.lazyQueue);
            }
        }
    }, 110, 'lazyrange');

    /**
     * Distribute loads to coresponding queues.
     */
    addRunnable('current', function (stage, cache) {
        var context = this;
        $.each(cache.lazyloads, function (name, actions) {
            var queue;
            switch (name) {
            case 'lazyGlobal':
                queue = fnSlice.lazyQueue;
                break;
            case 'slide':
                queue = cache.lazyload;
                break;
            case 'slider':
                // Falls through
            default:
                queue = context.plugins.sliceLazyload.queue;
                break;
            }
            $.each(actions, function (action, loads) {
                switch (action) {
                case 'immidiate':
                    queue.addRange(loads, true, true);
                    break;
                case 'forward':
                    queue.addRange(loads, true);
                    break;
                case 'lazy':
                    // Falls through
                default:
                    queue.addRange(loads);
                    break;
                }
            });
            queue.run();
        });
    }, 120);

    removeRequirement(tempRequirement);

    /**
     * Add runnables for auto lazy loading only.
     */
    tempRequirement = 'lazyauto';
    addRequirement(tempRequirement);

    /**
     * Auto manage lazy load.
     */
    addRunnable('current', function (stage, cache) {
        var queue = this.plugins.sliceLazyload.queue,
            max = stage.$slides.length,
            loads = [];
        queue.setMaxActive(1);
        queue.run();
        // Load current slide
        simpleSlideLoad(stage.$slides.eq(cache.real - 1), loads);
        if (loads.length) {
            queue.addRange(loads, true, true);
        }
        // Load closest slides
        var next = [],
            prev = [],
            position,
            i;
        loads = [];
        if (cache.start !== cache.end) {
            for (i = cache.start; i <= cache.end; i++) {
                if (i !== cache.real) {
                    simpleSlideLoad(stage.$slides.eq(i - 1), loads);
                }
            }
        }
        position = cache.end;
        if (max > position) {
            simpleSlideLoad(stage.$slides.eq(position), next);
        }
        position = cache.start - 2;
        if (position >= 0) {
            simpleSlideLoad(stage.$slides.eq(position), prev);
        }
        if (cache.fromPosition > cache.real) {
            loads = loads.concat(prev.concat(next));
            position = cache.start - 3;
            if (max > position) {
                simpleSlideLoad(stage.$slides.eq(position), loads);
            }
        } else {
            loads = loads.concat(next.concat(prev));
            position = cache.end + 1;
            if (max > position) {
                simpleSlideLoad(stage.$slides.eq(position), loads);
            }
        }
        queue.addRange(loads, true);
    }, 100);

    /**
     * Cleanup.
     */
    removeRequirement(tempRequirement);
    factory.resetRunnableType();
    removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceLazyload = SliceLazyload;
}));
