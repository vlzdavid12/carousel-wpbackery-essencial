/* global jQuery, Slice */

/**
 * Slice Slider: Animate Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 * @typedef {object} SlicePostponed Slice postponed class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Enable/disable content animation
        animate: true,
        // Animated element class
        animateClass: 'slice-animated',
        // Hide animation order relative to slide animation
        animateHide: 'before',
        // Hide animation order on position reset or after moving stage relative to slide animation
        animateResetHide: 'startBefore',
        // Show animation order on position reset or after moving stage relative to slide animation
        animateResetShow: false,
        // Show animation order relative to slide animation
        animateShow: 'after',
        // Force slider to finish previous animation
        finishPreviousAnimation: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        SlicePostponed = Slice.Postponed,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'animate';

    /**
     * Animate plugin constructor.
     *
     * @class SliceAnimate
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceAnimate (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceAnimate.prototype.setup = function () {
        var context = this,
            settings = context.slice.settings;
        settings.workers.animate = !!settings.animate;
        settings.animateOptions = {
            animateClass: settings.animateClass
        };
        if (!settings.animateResetShow && settings.animateShow === 'before') {
            settings.animateResetShow = 'startBefore';
        }
    };

    /**
     * Get animation options for current breakpoint.
     *
     * @param {(HTMLElement|jQuery)} element - The element to apply animation.
     * @param {string} name - Animation name.
     * @param {number} resolution - Current resolution size.
     * @returns {object} Breakpoint settings object.
     */
    function getBreakpoint (element, name, resolution) {
        var $element = $(element),
            breakpoints = $element.data('slice__' + name);
        if (!breakpoints) {
            var attrName = 'slice' + (name.charAt(0).toUpperCase() + name.slice(1)),
                options = fnSlice.parseOptions($element.data(attrName)),
                responsive = fnSlice.getResponsiveOptions($element, attrName);
            breakpoints = fnSlice.createBreakpoints(options, responsive);
            $element.data('slice__' + name, breakpoints);
        }
        for (var i = breakpoints.length - 1; i >= 0; i--) {
            if (resolution >= breakpoints[i].breakpoint) {
                return fnSlice.buildBreakPoint(breakpoints, i);
            }
        }
        return fnSlice.buildBreakPoint(breakpoints, 0);
    }

    /**
     * Reset element styles difference.
     *
     * @param {HTMLElement|jQuery} element - The element to reset animation.
     * @param {object} options1 - Animation to find difference.
     * @param {object} options2 - Animation to find difference.
     */
    function resetDiff (element, options1, options2) {
        if (options2 && options2.css) {
            var $element = $(element),
                css = {};
            if (options1 && options1.css) {
                $.each(options2.css, function (name) {
                    if (!options1.css.hasOwnProperty(name)) {
                        css[name] = '';
                    }
                });
            } else {
                $.each(options2.css, function (name) {
                    css[name] = '';
                });
            }
            $element.css(css);
        }
    }

    /**
     * Get static styles.
     *
     * @param {HTMLElement|jQuery} element - The element to apply styles.
     * @param {boolean} onlyNew - Return styles only for a new breakpoint.
     * @returns {object} Static styles object.
     */
    function getStaticStyles (element, onlyNew) {
        var $element = $(element),
            resolution = $(window).width(),
            breakpoint = getBreakpoint($element, 'static', resolution),
            currentBreakpoint = $element.data('slice__current__static');
        if (currentBreakpoint && currentBreakpoint.breakpoint === breakpoint.breakpoint) {
            return onlyNew
                ? null
                : currentBreakpoint.builtOptions;
        }
        $element.data('slice__current__static', breakpoint);
        return breakpoint.builtOptions;
    }

    /**
     * Apply static styles.
     *
     * @param {HTMLElement|jQuery} element - The element to apply styles.
     * @param {boolean} onlyNew - Apply only when new styles.
     */
    function applyStaticStyles (element, onlyNew) {
        var current = $(element).data('slice__current__static'),
            styles = getStaticStyles(element, onlyNew);
        if (styles) {
            if (current) {
                resetDiff(element, {
                    css: styles
                }, {
                    css: current.builtOptions
                });
            }
            $(element).css(styles);
        }
    }

    /**
     * Get animation options for current breakpoint.
     *
     * @param {HTMLElement|jQuery} element - The element to apply animation.
     * @param {string} name - Animation name.
     * @param {object} defaults - Default animation options.
     * @param {boolean} onlyNew - Return options only for a new breakpoint.
     * @returns {object} Animation options.
     */
    function getAnimateOptions (element, name, defaults, onlyNew) {
        var $element = $(element),
            resolution = $(window).width(),
            breakpoint = getBreakpoint($element, name, resolution),
            animationBreakpoint = getBreakpoint($element, 'animate', resolution),
            currentBreakpoint = $element.data('slice__current__' + name),
            breakValue = Math.max(breakpoint.breakpoint, animationBreakpoint.breakpoint);
        if (currentBreakpoint && currentBreakpoint.breakpoint === breakValue) {
            return onlyNew
                ? null
                : $.extend({}, currentBreakpoint.options);
        }
        var buildBreakpoint = {
                breakpoint: breakValue
            },
            raw = $.extend({}, animationBreakpoint.builtOptions, breakpoint.builtOptions),
            css = $.extend({}, raw),
            classValue = false,
            duration = 0,
            delay = 0;
        if (raw.name) {
            css.animationName = raw.name;
            delete css.name;
        }
        if (raw.delay) {
            delay = raw.delay;
            delete css.delay;
        }
        if (raw.duration) {
            duration = raw.duration;
            css.animationDuration = raw.duration / 1000 + 's';
            delete css.duration;
        }
        if (raw.class) {
            classValue = raw.class;
            delete css.class;
        }
        var reset = $.extend({}, css);
        $.each(reset, function (prop) {
            reset[prop] = '';
        });
        buildBreakpoint.options = $.extend(true, {}, defaults || {}, {
            class: classValue,
            css: css,
            delay: delay,
            duration: duration,
            reset: reset
        });
        buildBreakpoint.options.raw = raw;
        $element.data('slice__current__' + name, buildBreakpoint);
        return $.extend({}, buildBreakpoint.options);
    }

    /**
     * Reset animation.
     *
     * @param {HTMLElement|jQuery} element - The element to apply animation.
     * @param {object} options - Animation options.
     */
    function resetAnimation (element, options) {
        $(element)
            .css({
                animationDelay: '',
                transition: ''
            })
            .css(options.reset)
            .removeClass(options.animateClass)
            .removeClass(options.class);
    }

    /**
     * Show animation.
     *
     * @param {HTMLElement|jQuery} element - The element to apply animation.
     * @param {object} defaults - Default animation options.
     * @param {number} maxTime - Max time of the animation.
     * @returns {SlicePostponed} Postponed animation.
     */
    function animateShow (element, defaults, maxTime) {
        var $element = $(element),
            postponed = new SlicePostponed();
        postponed.add(function () {
            var current = $.extend(true, {}, $element.data('slice__current__show')),
                options = getAnimateOptions($element, 'show', defaults, true),
                delay = 0,
                state = $element.data('slice_state');
            $element.data('slice_state', 'show');
            switch (state) {
            case 'show':
                if (!options) {
                    if (current.options) {
                        $element
                            .css(current.options.css)
                            .addClass(current.options.class)
                            .addClass(current.options.animateClass);
                    }
                    return postponed.resolve();
                }
                resetAnimation($element, options);
                delay = -options.duration;
                break;
            case 'hide':
                resetAnimation($element, $element.data('slice__current__hide').options);
                applyStaticStyles($element);
                // Falls through
            default:
                if (!options && current.options) {
                    options = current.options;
                }
                if (!options) {
                    return postponed.resolve();
                }
                delay = options.delay;
                break;
            }
            if (current.options) {
                resetDiff($element, options, current.options);
            }
            if (maxTime === 'instant') {
                delay = 0;
                options.delay = 0;
                options.duration = 0;
                options.css.animationDuration = '0s';
            }
            $element
                .css({
                    animationDelay: delay / 1000 + 's',
                    transition: 'none'
                })
                .css(options.css)
                .addClass(options.class)
                .addClass(options.animateClass);
            var timeout = setTimeout(function () {
                $element.css({
                    animationDelay: -options.duration / 1000 + 's',
                    transition: ''
                });
                timeout = false;
                postponed.resolve();
            }, delay + options.duration);
            postponed.fail(function () {
                if (timeout) {
                    clearTimeout(timeout);
                }
                timeout = null;
            });
            return postponed;
        });
        return postponed;
    }

    /**
     * Hide animation.
     *
     * @param {HTMLElement|jQuery} element - The element to apply animation.
     * @param {object} defaults - Default animation options.
     * @param {number} maxTime - Max time of the animation.
     * @returns {SlicePostponed} Postponed animation.
     */
    function animateHide (element, defaults, maxTime) {
        var $element = $(element),
            postponed = new SlicePostponed();
        postponed.add(function () {
            var current = $.extend(true, {}, $element.data('slice__current__hide')),
                options = getAnimateOptions($element, 'hide', defaults),
                state = $element.data('slice_state');
            $element.data('slice_state', 'hide');
            switch (state) {
            case 'show':
                resetAnimation($element, $element.data('slice__current__show').options);
                break;
            case 'hide':
                return postponed.resolve();
            }
            if (current.options) {
                resetDiff($element, options, current.options);
            }
            if (maxTime === 'instant') {
                options.delay = 0;
                options.duration = 0;
                options.css.animationDuration = '0s';
            }
            $element
                .css({
                    animationDelay: options.delay / 1000 + 's',
                    transition: 'none'
                })
                .css(options.css)
                .addClass(options.class)
                .addClass(options.animateClass);
            var timeout = setTimeout(function () {
                timeout = null;
                postponed.resolve();
            }, options.delay + options.duration);
            postponed.always(function () {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                if ($element.data('slice_state') === 'hide') {
                    resetAnimation($element, options);
                    applyStaticStyles($element);
                    $element.data('slice_state', null);
                }
            });
            return postponed;
        });
        return postponed;
    }

    /**
     * Add workers and runnables for animate mode only.
     */
    factory.addRequirement(requirement);

    /**
     * Apply static styles.
     */
    addWorker('size, slides', function (stage) {
        stage.$slides.each(function (i, slide) {
            $(slide).find('[data-slice-static]')
                .each(function (n, element) {
                    applyStaticStyles(element, true);
                });
        });
    }, 10);

    /**
     * Declare SlicePostponed object for animations.
     */
    addWorker('reset, move', function (stage, cache) {
        cache.contentAnimationInstant = new SlicePostponed();
        cache.contentAnimationStartBefore = new SlicePostponed();
        cache.contentAnimationBefore = new SlicePostponed();
        cache.contentAnimationAfter = new SlicePostponed();
    }, 300);

    /**
     * Declare animation order for reset position.
     */
    addWorker('reset', function (stage, cache) {
        var settings = this.settings;
        cache.animateHide = settings.animateResetHide;
        cache.animateShow = settings.animateResetShow;
    }, 300);

    /**
     * Declare animation order for move to position
     * and finish previous animation if required.
     */
    addWorker('move', function (stage, cache) {
        var settings = this.settings;
        if (settings.finishPreviousAnimation) {
            this.run('finishAnimation');
        }
        if (this.moved) {
            cache.animateHide = settings.animateResetHide || settings.animateHide;
            cache.animateShow = settings.animateResetShow || settings.animateShow;
        } else {
            cache.animateHide = settings.animateHide;
            cache.animateShow = settings.animateShow;
        }
    }, 300);

    /**
     * Add slides content animation to animation postponed objects.
     */
    addWorker('reset, move', function (stage, cache) {
        var context = this,
            animation;
        if (cache.fromPosition !== cache.realPosition) {
            switch (cache.animateHide) {
            case 'instant':
                animation = cache.contentAnimationInstant;
                break;
            case 'startBefore':
                animation = cache.contentAnimationStartBefore;
                break;
            case 'transition':
                animation = cache.animation;
                break;
            case 'after':
                animation = cache.contentAnimationAfter;
                break;
            case 'before':
                // Falls through
            default:
                animation = cache.contentAnimationBefore;
                break;
            }
            stage.$slides.eq(cache.fromPosition - 1).find('[data-slice-animate]')
                .each(function (n, element) {
                    animation.add(animateHide(
                        element, context.settings.animateOptions,
                        cache.animateHide === 'instant'
                            ? 'instant'
                            : null
                    ));
                });
        }
        switch (cache.animateShow) {
        case 'instant':
            animation = cache.contentAnimationInstant;
            break;
        case 'startBefore':
            animation = cache.contentAnimationStartBefore;
            break;
        case 'before':
            animation = cache.contentAnimationBefore;
            break;
        case 'transition':
            animation = cache.animation;
            break;
        case 'after':
            // Falls through
        default:
            animation = cache.contentAnimationAfter;
            break;
        }
        stage.$slides.eq(cache.realPosition - 1).find('[data-slice-animate]')
            .each(function (n, element) {
                animation.add(animateShow(
                    element, context.settings.animateOptions,
                    cache.animateShow === 'instant'
                        ? 'instant'
                        : null
                ));
            });
        cache.animateShowAnimation = animation;
    }, 310);

    /**
     * When there will be a position reset, add show animation of the actual slide.
     */
    addWorker('reset, move', function (stage, cache) {
        if (stage.resetPosition) {
            var context = this;
            stage.$slides.eq(context.currentPosition + stage.clonesCount - 1).find('[data-slice-animate]')
                .each(function (n, element) {
                    cache.animateShowAnimation.add(animateShow(
                        element, context.settings.animateOptions,
                        cache.animateShow === 'instant'
                            ? 'instant'
                            : null
                    ));
                });
        }
    }, 310, 'loop');

    /**
     * Add animations that should be runned before moving to slide position.
     */
    addWorker('reset, move', function (stage, cache) {
        if (cache.contentAnimationInstant.state.not('plain')) {
            cache.contentAnimationBefore.add(cache.contentAnimationInstant);
        }
        if (cache.contentAnimationBefore.state.not('plain') || cache.contentAnimationStartBefore.state.not('plain')) {
            var animation = this.animation;
            this.animation = cache.contentAnimationBefore;
            cache.contentAnimationBefore.runAfter(animation, true);
            if (cache.contentAnimationStartBefore.state.not('plain')) {
                cache.contentAnimationStartBefore.runAfter(animation, true);
                cache.contentAnimationAfter.add(cache.contentAnimationStartBefore);
            }
        }
    }, 349);

    /**
     * Add animations that should be runned after moving to slide position.
     */
    addWorker('reset, move', function (stage, cache) {
        if (cache.contentAnimationAfter.state.not('plain')) {
            var animation = this.animation;
            this.animation = cache.contentAnimationAfter;
            cache.contentAnimationAfter.runAfter(animation, true);
        }
    }, 401);

    /**
     * Cleanup.
     */
    factory.removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceAnimate = SliceAnimate;
}));
