/*!
  * Slice lib.1.2.4
  * Copyright 2019 shininglab (https://shininglab-code.com/slice)
  */

(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define([], factory);
  } else if (typeof exports !== "undefined") {
    factory();
  } else {
    var mod = {
      exports: {}
    };
    factory();
    global.sliceCore = mod.exports;
  }
})(typeof globalThis !== "undefined" ? globalThis : typeof self !== "undefined" ? self : this, function () {
  "use strict";

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _inheritsLoose(subClass, superClass) { subClass.prototype = Object.create(superClass.prototype); subClass.prototype.constructor = subClass; subClass.__proto__ = superClass; }

  var s4 = function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  };

  var Slice = /*#__PURE__*/function () {
    function Slice() {}

    Slice.uniqueId = function uniqueId(ids, pref) {
      var id = "" + s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();

      if (pref) {
        id = pref + id;
      }

      if (ids && ids[id]) {
        id = self.uniqueId(ids);
      }

      return id;
    };

    Slice.ucfirst = function ucfirst(str) {
      return str.charAt(0).toUpperCase() + str.slice(1);
    };

    Slice.redraw = function redraw(element) {
      if (element) {
        var disp = element.style.display;
        element.style.display = 'none';
        element.offsetHeight;
        element.style.display = disp;
      }
    };

    Slice.some = function some(arr, predicate) {
      var len = arr.length;
      var i = 0;

      for (; i < len; i++) {
        if (predicate(arr[i], i)) {
          return true;
        }
      }

      return false;
    };

    Slice.toObject = function toObject(arr) {
      var obj = {};
      var i = 0;
      var len = arr.length;

      for (; i < len; i++) {
        obj[arr[i]] = true;
      }

      return obj;
    };

    Slice.arrayEach = function arrayEach(arr, func) {
      var i = 0;
      var len = arr.length;

      for (; i < len; i++) {
        func(arr[i], i);
      }

      return arr;
    };

    Slice.objectEach = function objectEach(obj, func) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          func(obj[prop], prop);
        }
      }

      return obj;
    };

    Slice.objectSome = function objectSome(obj, predicate) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop) && predicate(obj[prop], prop)) {
          return true;
        }
      }

      return false;
    };

    Slice.accessibleProperty = function accessibleProperty(obj, prop) {
      return obj && typeof obj === 'object' && (obj.hasOwnProperty(prop) || typeof obj[prop] === 'function');
    };

    Slice.hasProperty = function hasProperty(obj, prop) {
      return obj && typeof obj === 'object' && obj.hasOwnProperty(prop) && typeof obj[prop] !== 'undefined';
    };

    Slice.getObjectValue = function getObjectValue(obj, prop) {
      if (obj && typeof obj === 'object' && obj.hasOwnProperty(prop)) {
        return typeof obj[prop] === 'function' ? obj[prop]() : obj[prop];
      }

      return null;
    };

    Slice.getObjectValues = function getObjectValues(obj, arr) {
      if (!Array.isArray(arr)) {
        return obj;
      }

      var res = {};
      Slice.arrayEach(arr, function (value) {
        if (obj.hasOwnProperty(value)) {
          res[value] = obj[value];
        }
      });
      return res;
    };

    Slice.quickApply = function quickApply(obj, method, args) {
      if (Slice.accessibleProperty(obj, method)) {
        return obj[method].apply(obj, args);
      }

      return false;
    };

    Slice.noop = function noop() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return args;
    };

    return Slice;
  }();

  var unitRegExp = new RegExp('^((?:[+]|[-])?[0-9]+(?:[.][0-9]+)?)(.*)$');

  var SliceUnit = /*#__PURE__*/function () {
    function SliceUnit() {}

    SliceUnit.toUnitType = function toUnitType(value) {
      var unitType = {
        type: 'px',
        value: 0
      };

      if (typeof value === 'string') {
        var result = value.match(unitRegExp);
        var number = Math.floor(result[1] ? Number(result[1]) : 0);
        var unit = result[2];

        switch (unit) {
          case '%':
            unitType.value = number;
            unitType.type = unit;
            unitType.scale = number / 100;
            break;

          case 'px':
          case '':
            unitType.value = number;
            break;
        }
      } else {
        unitType.value = value;
      }

      return unitType;
    };

    SliceUnit.toPx = function toPx(value, baseValue) {
      var pxVal = 0;

      if (typeof value === 'string') {
        var result = value.match(unitRegExp);
        var number = Math.floor(result[1] ? Number(result[1]) : 0);
        var unit = result[2];

        switch (unit) {
          case '%':
            pxVal = number * baseValue / 100;
            break;

          case 'px':
          case '':
            pxVal = number;
            break;
        }
      } else {
        pxVal = value;
      }

      return pxVal;
    };

    SliceUnit.revPx = function revPx(value, resValue) {
      var pxVal = 0;

      if (typeof value === 'string') {
        var result = value.match(unitRegExp);
        var number = Math.floor(result[1] ? Number(result[1]) : 0);
        var unit = result[2];

        switch (unit) {
          case '%':
            pxVal = number * resValue / (100 - number);
            break;

          case 'px':
          case '':
            pxVal = number;
            break;
        }
      } else {
        pxVal = value;
      }

      return pxVal;
    };

    return SliceUnit;
  }();

  Slice.Unit = SliceUnit;

  var SliceState = /*#__PURE__*/function () {
    function SliceState(states) {
      var _this = this;

      this.states = {};
      this.current = {};

      if (states) {
        Slice.objectEach(states, function (subStates, state) {
          _this.add(state, subStates);
        });
      }
    }

    var _proto = SliceState.prototype;

    _proto.add = function add(name, subStates) {
      var subs = Slice.Tags.fromValue(subStates);

      if (this.states[name]) {
        this.states[name] = this.states[name].concat(subs);
      } else {
        this.states[name] = subs;
      }

      return this;
    };

    _proto.is = function is(name) {
      return this.current[name] && this.current[name] > 0;
    };

    _proto.not = function not(name) {
      return !this.is(name);
    };

    _proto.enter = function enter(name, times) {
      var _this2 = this;

      var number = times && times > 0 ? Math.ceil(times) : 1;
      Slice.arrayEach([name].concat(this.states[name] || []), function (state) {
        if (typeof _this2.current[state] === 'undefined') {
          _this2.current[state] = 0;
        }

        _this2.current[state] += number;
      });
      return this;
    };

    _proto.leave = function leave(name) {
      var _this3 = this;

      if (this.is(name)) {
        Slice.arrayEach([name].concat(this.states[name] || []), function (state) {
          _this3.current[state]--;
        });
      }

      return this;
    };

    _proto.exit = function exit(name) {
      var _this4 = this;

      if (this.is(name)) {
        var size = this.current[name];
        Slice.arrayEach([name].concat(this.states[name] || []), function (state) {
          _this4.current[state] -= size;

          if (_this4.current[state] < 0) {
            _this4.current[state] = 0;
          }
        });
      }

      return this;
    };

    _proto.reset = function reset() {
      this.current = {};
      return this;
    };

    return SliceState;
  }();

  Slice.State = SliceState;

  var SliceTags = /*#__PURE__*/function () {
    SliceTags.fromString = function fromString(str) {
      var tags = [];
      var list = str.split(',');
      var i = 0;
      var tag;

      for (; i < list.length; i++) {
        tag = list[i].trim();

        if (tag) {
          tags.push(tag);
        }
      }

      return tags;
    };

    SliceTags.fromValue = function fromValue(value, strict) {
      return value === true && !strict ? true : Array.isArray(value) ? value : typeof value === 'string' ? SliceTags.fromString(value) : [];
    };

    SliceTags.contains = function contains(tags, tag) {
      switch (typeof tag) {
        case 'string':
          return !Slice.some(tag.split('+'), function (value) {
            return !tags[value.trim()];
          });

        default:
          return tags[tag];
      }
    };

    function SliceTags(tags) {
      this.tags = SliceTags.fromValue(tags, true);
      this.list = false;
    }

    var _proto2 = SliceTags.prototype;

    _proto2.add = function add(tags) {
      var builtTags = tags instanceof SliceTags ? tags.getAll() : SliceTags.fromValue(tags, true);
      this.list = false;
      this.tags = this.tags.concat(builtTags);
      return this;
    };

    _proto2.has = function has(tags) {
      var checkTags = tags instanceof SliceTags ? tags.getAll() : SliceTags.fromValue(tags, true);
      var list = this.getList();
      return Slice.some(checkTags, function (value) {
        return SliceTags.contains(list, value);
      });
    };

    _proto2.hasAll = function hasAll(tags) {
      var checkTags = tags instanceof SliceTags ? tags.getAll() : SliceTags.fromValue(tags, true);
      var list = this.getList();
      return !Slice.some(checkTags, function (value) {
        return !SliceTags.contains(list, value);
      });
    };

    _proto2.allIn = function allIn(tags) {
      var list = tags instanceof SliceTags ? tags.getList() : Slice.toObject(SliceTags.fromValue(tags, true));
      var checkTags = this.tags;
      return !Slice.some(checkTags, function (value) {
        return !SliceTags.contains(list, value);
      });
    };

    _proto2.getList = function getList() {
      if (!this.list) {
        this.list = Slice.toObject(this.tags);
      }

      return this.list;
    };

    _proto2.getAll = function getAll() {
      return this.tags;
    };

    return SliceTags;
  }();

  Slice.Tags = SliceTags;

  var flatPromises = function flatPromises(depth) {
    var promises = [];

    for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    Slice.arrayEach(args, function (promise) {
      if (typeof promise === 'object' && typeof promise.then === 'function') {
        promises.push(promise);
      } else if (depth && Array.isArray(promise)) {
        promises.push.apply(promises, flatPromises.apply(void 0, [depth - 1].concat(promise)));
      } else {
        promises.push(SlicePromise.resolve(promise));
      }
    });
    return promises;
  };

  var callFns = function callFns(fns, types, result) {
    var runTypes;

    if (Array.isArray(types)) {
      runTypes = Slice.toObject(types);
    } else if (typeof types === 'string') {
      runTypes = {};
      runTypes[types] = true;
    }

    Slice.arrayEach(fns, function (fn) {
      if (runTypes[fn.type]) {
        fn.fn(result);
      }
    });
  };

  var createFns = function createFns(type, toAdd, fns) {
    var addedList = Array.isArray(fns) ? fns : [];
    Slice.arrayEach(toAdd, function (fn) {
      if (typeof fn === 'function') {
        var fnData = {
          fn: fn,
          type: type
        };
        addedList.push(fnData);
      } else if (Array.isArray(fn)) {
        createFns(type, fn, addedList);
      }
    });
    return addedList;
  };

  var addFns = function addFns(fns, type, toAdd, noAdd, run, runResult) {
    var addedList = createFns(type, toAdd);

    if (!noAdd) {
      fns.push.apply(fns, addedList);
    }

    if (run) {
      callFns(addedList, type, runResult);
    }
  };

  var reject = function reject(promise, value) {
    if (promise.state.not('completed')) {
      promise.state.reset().enter('rejected');
      var fns = promise.fns;
      promise.fns = [];
      promise.result = value;
      callFns(fns, ['fail', 'always'], value);
    }

    return promise;
  };

  var resolve = function resolve(promise, value) {
    if (promise.state.not('completed')) {
      promise.state.reset().enter('resolved');
      var fns = promise.fns;
      promise.fns = [];
      promise.result = value;
      callFns(fns, ['done', 'always'], value);
    }

    return promise;
  };

  var SlicePromise = /*#__PURE__*/function () {
    function SlicePromise(func) {
      var _this5 = this;

      this.state = new SliceState({
        plain: 'pending',
        rejected: 'completed, run, failed, triggered',
        resolved: 'completed, run, fulfilled, triggered'
      });
      this.state.enter('plain');
      this.fns = [];

      var resolvePromise = function resolvePromise(value) {
        resolve(_this5, value);
      };

      var rejectPromise = function rejectPromise(value) {
        reject(_this5, value);
      };

      func.call(this, resolvePromise, rejectPromise);
    }

    var _proto3 = SlicePromise.prototype;

    _proto3.always = function always() {
      var completed = this.state.is('completed');

      for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        args[_key3] = arguments[_key3];
      }

      addFns(this.fns, 'always', args, completed, completed, this.result);
      return this;
    };

    _proto3.finaly = function finaly() {
      var resPromise;
      var promise = new SlicePromise(function (res) {
        resPromise = res;
      });

      for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
        args[_key4] = arguments[_key4];
      }

      args.push(function (result) {
        resPromise(result);
      });
      this.always.apply(this, args);
      return promise;
    };

    _proto3.done = function done() {
      for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
        args[_key5] = arguments[_key5];
      }

      addFns(this.fns, 'done', args, this.state.is('completed'), this.state.is('resolved'), this.result);
      return this;
    };

    _proto3.fail = function fail() {
      for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
        args[_key6] = arguments[_key6];
      }

      addFns(this.fns, 'fail', args, this.state.is('completed'), this.state.is('rejected'), this.result);
      return this;
    };

    _proto3.catch = function _catch() {
      for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {
        args[_key7] = arguments[_key7];
      }

      return this.then(null, args);
    };

    _proto3.then = function then(doneFn, failFn) {
      var resPromise, rejPromise;
      var promise = new SlicePromise(function (res, rej) {
        resPromise = res;
        rejPromise = rej;
      });
      var doneFns = Array.isArray(doneFn) ? doneFn : [doneFn];
      var failFns = Array.isArray(failFn) ? failFn : [failFn];
      doneFns.push(function (result) {
        resPromise(result);
      });
      failFns.push(function (result) {
        rejPromise(result);
      });
      var completed = this.state.is('completed');
      addFns(this.fns, 'done', doneFns, completed, this.state.is('resolved'), this.result);
      addFns(this.fns, 'fail', failFns, completed, this.state.is('rejected'), this.result);
      return promise;
    };

    SlicePromise.resolve = function resolve(result) {
      return new SlicePromise(function (res) {
        res(result);
      });
    };

    SlicePromise.reject = function reject(result) {
      return new SlicePromise(function (res) {
        res(result);
      });
    };

    SlicePromise.all = function all() {
      for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {
        args[_key8] = arguments[_key8];
      }

      var promises = flatPromises(1, args);
      var pendingLen = promises.length;
      var resArgs = [];
      var resPromise, rejPromise;
      var retPromise = new SlicePromise(function (res, rej) {
        resPromise = res;
        rejPromise = rej;
      });

      if (promises.length) {
        Slice.arrayEach(promises, function (promise) {
          promise.then(function (result) {
            if (pendingLen) {
              --pendingLen;
              resArgs.push(result);

              if (pendingLen === 0) {
                resPromise(resArgs);
              }
            }
          }, function (result) {
            if (pendingLen) {
              pendingLen = null;
              resArgs = null;
              rejPromise(result);
            }
          });
        });
      } else {
        resPromise();
      }

      return retPromise;
    };

    SlicePromise.any = function any() {
      for (var _len9 = arguments.length, args = new Array(_len9), _key9 = 0; _key9 < _len9; _key9++) {
        args[_key9] = arguments[_key9];
      }

      var promises = flatPromises(1, args);
      var pendingLen = promises.length;
      var resArgs = [];
      var resPromise, rejPromise;
      var retPromise = new SlicePromise(function (res, rej) {
        resPromise = res;
        rejPromise = rej;
      });

      if (promises.length) {
        Slice.arrayEach(promises, function (promise) {
          promise.then(function (result) {
            if (pendingLen) {
              pendingLen = null;
              resArgs = null;
              resPromise(result);
            }
          }, function (result) {
            if (pendingLen) {
              --pendingLen;
              resArgs.push(result);

              if (pendingLen === 0) {
                rejPromise(resArgs);
              }
            }
          });
        });
      } else {
        resPromise();
      }

      return retPromise;
    };

    SlicePromise.allSettled = function allSettled() {
      for (var _len10 = arguments.length, args = new Array(_len10), _key10 = 0; _key10 < _len10; _key10++) {
        args[_key10] = arguments[_key10];
      }

      var promises = flatPromises(1, args);
      var pendingLen = promises.length;
      var resArgs = [];
      var resPromise;
      var retPromise = new SlicePromise(function (res) {
        resPromise = res;
      });

      if (promises.length) {
        Slice.arrayEach(promises, function (promise) {
          promise.then(function (result) {
            if (pendingLen) {
              --pendingLen;
              resArgs.push({
                status: 'fulfilled',
                value: result
              });

              if (pendingLen === 0) {
                resPromise(resArgs);
              }
            }
          }, function (result) {
            if (pendingLen) {
              --pendingLen;
              resArgs.push({
                reason: result,
                status: 'rejected'
              });

              if (pendingLen === 0) {
                resPromise(resArgs);
              }
            }
          });
        });
      } else {
        resPromise();
      }

      return retPromise;
    };

    SlicePromise.race = function race() {
      for (var _len11 = arguments.length, args = new Array(_len11), _key11 = 0; _key11 < _len11; _key11++) {
        args[_key11] = arguments[_key11];
      }

      var promises = flatPromises(1, args);
      var pending = true;
      var resPromise, rejPromise;
      var retPromise = new SlicePromise(function (res, rej) {
        resPromise = res;
        rejPromise = rej;
      });
      Slice.arrayEach(promises, function (promise) {
        promise.then(function (result) {
          if (pending) {
            pending = false;
            resPromise(result);
          }
        }, function (result) {
          if (pending) {
            pending = false;
            rejPromise(result);
          }
        });
      });
      return retPromise;
    };

    return SlicePromise;
  }();

  Slice.Promise = SlicePromise;

  var SliceDeferred = /*#__PURE__*/function (_SlicePromise) {
    _inheritsLoose(SliceDeferred, _SlicePromise);

    function SliceDeferred() {
      return _SlicePromise.call(this, function (resolve, reject) {
        this.resolve = resolve;
        this.reject = reject;
      }) || this;
    }

    var _proto4 = SliceDeferred.prototype;

    _proto4.promise = function promise() {
      if (!this.promise) {
        this.promise = SlicePromise.race(this);
      }

      return this.promise;
    };

    return SliceDeferred;
  }(SlicePromise);

  Slice.Deferred = SliceDeferred;

  function runPostponed(postponed, item) {
    var promise;

    switch (typeof item) {
      case 'object':
        if (item instanceof SlicePostponed) {
          item.run();
        }

        if (item.always) {
          promise = item;
        } else {
          promise = SlicePromise.resolve(item);
        }

        break;

      case 'function':
        promise = runPostponed(postponed, item());
        break;

      default:
        promise = SlicePromise.resolve(item);
        break;
    }

    return promise;
  }

  function runPostponedAndRemove(postponed, item) {
    var promise = runPostponed(postponed, item);
    postponed.active.push(promise);
    return promise.always(function () {
      postponed.remove(promise);
    });
  }

  function _runNext(postponed, item) {
    var promise = runPostponed(postponed, item);
    postponed.active.push(promise);
    promise.always(function () {
      postponed.remove(promise);
      postponed.runNext();
    });
  }

  function runImmidiate(postponed, item) {
    if (postponed.maxActive) {
      _runNext(postponed, item);
    } else {
      runPostponedAndRemove(postponed, item);
    }
  }

  function refreshData(postponed) {
    postponed.promise = null;
    postponed.fns = [];
    postponed.result = null;
    postponed.done(function (result) {
      var resArgs = [result];
      Slice.quickApply(postponed.after, 'resolve', resArgs);
      var items = postponed.active.concat(postponed.items);
      postponed.items = [];
      postponed.state.enter('plain');
      Slice.arrayEach(items, function (item) {
        Slice.quickApply(item, 'resolve', resArgs);
      });
    }).fail(function (result) {
      var resArgs = [result];
      Slice.quickApply(postponed.after, 'reject', resArgs);
      var items = postponed.active.concat(postponed.items);
      postponed.items = [];
      postponed.state.enter('plain');
      Slice.arrayEach(items, function (item) {
        switch (typeof item) {
          case 'object':
            Slice.quickApply(item, 'reject', resArgs);
            break;

          case 'function':
            item();
            break;
        }
      });
    });
  }

  function refresh(postponed) {
    if (postponed.state.is('triggered')) {
      if (postponed.state.is('completed')) {
        postponed.state.exit('rejected').exit('resolved');
        refreshData(postponed);
        postponed.run();
      } else if (postponed.state.is('queued')) {
        postponed.runNext();
      } else if (postponed.state.is('running')) {
        var active = postponed.items;
        postponed.items = [];
        Slice.arrayEach(active, function (item) {
          runPostponedAndRemove(postponed, item);
        });
      }
    }
  }

  var SlicePostponed = /*#__PURE__*/function (_SliceDeferred) {
    _inheritsLoose(SlicePostponed, _SliceDeferred);

    function SlicePostponed(maxActive) {
      var _this6;

      _this6 = _SliceDeferred.call(this) || this;
      refreshData(_assertThisInitialized(_this6));
      _this6.active = [];
      _this6.maxActive = maxActive || 0;

      _this6.state.add('queue', 'pending').add('queued', 'run, triggered').add('runAfter', 'run, pending').add('running', 'run, triggered');

      _this6.items = [];
      _this6.length = 0;
      return _this6;
    }

    var _proto5 = SlicePostponed.prototype;

    _proto5.runAfter = function runAfter(variable, linked) {
      var _this7 = this;

      var promise = SlicePromise.race(variable);
      this.state.enter('runAfter');

      if (linked) {
        this.after = variable;
        promise.always(function () {
          _this7.after = null;
        });
      }

      if (this.state.not('triggered')) {
        promise.always(function () {
          _this7.state.exit('runAfter');
        }).done(function () {
          _this7.run();
        }).fail(function () {
          var _this7$reject;

          (_this7$reject = _this7.reject).apply.apply(_this7$reject, arguments);
        });
      }

      return this;
    };

    _proto5.setMaxActive = function setMaxActive(value) {
      if (this.maxActive !== value) {
        this.maxActive = value;

        if (this.state.is('queued')) {
          this.runNext();
        }
      }

      return this;
    };

    _proto5.checkFinish = function checkFinish() {
      if (this.state.is('running') && this.state.not('queue')) {
        this.resolve('completed');
      }

      return this;
    };

    _proto5.add = function add(item, forward, immidiate) {
      if (this.state.is('plain')) {
        this.state.exit('plain');
      }

      this.state.enter('queue');

      if (immidiate) {
        runImmidiate(this, item);
      } else {
        var action = forward ? 'unshift' : 'push';
        this.items[action](item);
        refresh(this);
      }

      return this;
    };

    _proto5.addRange = function addRange(list, forward, immidiate) {
      var _this8 = this;

      if (this.state.is('plain')) {
        this.state.exit('plain');
      }

      this.state.enter('queue', list.length);

      if (immidiate) {
        Slice.arrayEach(list, function (item) {
          runImmidiate(_this8, item);
        });
      } else {
        this.items = forward ? list.concat(this.items) : this.items.concat(list);
        refresh(this);
      }

      return this;
    };

    _proto5.run = function run() {
      var _this9 = this;

      if (this.state.not('triggered')) {
        this.state.enter('running');

        if (this.maxActive) {
          this.state.enter('queued');
          this.runNext();
        } else {
          var active = this.items;
          this.items = [];
          Slice.arrayEach(active, function (item) {
            runPostponedAndRemove(_this9, item);
          });
          this.checkFinish();
        }
      } else if (this.state.is('queued')) {
        this.runNext();
      }

      return this;
    };

    _proto5.runNext = function runNext() {
      var _this10 = this;

      this.checkFinish();

      if (this.state.not('completed')) {
        if (!this.maxActive || this.state.not('running')) {
          return this.run();
        }

        Slice.arrayEach(this.items.splice(0, Math.max(0, Math.min(this.maxActive - this.active.length, this.items.length))), function (item) {
          _runNext(_this10, item);
        });
      }

      return this;
    };

    _proto5.remove = function remove(item) {
      var removed = false;
      var position = this.items.indexOf(item);

      if (position >= 0) {
        removed = true;
        this.items.splice(position, 1);
      }

      position = this.active.indexOf(item);

      if (position >= 0) {
        removed = true;
        this.active.splice(position, 1);
      }

      if (removed) {
        this.state.leave('queue');
        this.checkFinish();
      }

      return this;
    };

    return SlicePostponed;
  }(SliceDeferred);

  Slice.Postponed = SlicePostponed;

  var SliceWorkflow = /*#__PURE__*/function () {
    function SliceWorkflow(workers, caching, quick) {
      this.workers = workers;
      this.quick = quick || {};

      switch (caching) {
        case 'all':
          this.cache = {};
          this.quickCache = {};
          break;

        case 'quick':
          this.quickCache = {};
          break;

        case 'no-cache':
          this.cache = false;
          this.quickCache = false;
          break;

        default:
          this.cache = caching ? {} : false;
          this.quickCache = caching ? {} : false;
          break;
      }
    }

    SliceWorkflow.createWorker = function createWorker(keys, fn, priority) {
      if (typeof fn !== 'function') {
        return null;
      }

      return {
        keys: keys,
        priority: priority || 0,
        run: fn
      };
    };

    SliceWorkflow.runWorker = function runWorker(worker, cache) {
      return worker.run(cache);
    };

    SliceWorkflow.buildWorker = function buildWorker(worker) {
      if (!worker.built) {
        worker.keys = Slice.Tags.fromValue(worker.keys);
        worker.built = true;
      }

      return worker;
    };

    SliceWorkflow.handleResult = function handleResult(flow, result) {
      if (flow.asObject) {
        return Slice.getObjectValues(result, flow.expected);
      }

      return result[flow.expected];
    };

    SliceWorkflow.runFlow = function runFlow(flow, data, fn) {
      var _this11 = this;

      var execute = typeof fn === 'function' ? fn : SliceWorkflow.runWorker;

      if (!flow.cache) {
        flow.cache = {};
      }

      flow.cache = _extends({}, data || {});
      flow.cache.list = flow.list;
      Slice.some(flow.workers, function (worker) {
        return execute.call(_this11, worker, flow.cache) === false;
      });
      return SliceWorkflow.handleResult(flow, flow.cache);
    };

    var _proto6 = SliceWorkflow.prototype;

    _proto6.getFlowSettings = function getFlowSettings(keys) {
      var asObject = true;
      var arrKeys, expected;

      if (typeof keys === 'string' && this.quick[keys]) {
        var _this$quick$keys = this.quick[keys];
        asObject = _this$quick$keys.asObject;
        expected = _this$quick$keys.expected;
        arrKeys = _this$quick$keys.keys;
      } else {
        arrKeys = Slice.Tags.fromValue(keys, true);
      }

      var list = Slice.toObject(arrKeys);
      return {
        asObject: asObject,
        expected: expected,
        filter: function (key) {
          return Slice.Tags.contains(this, key);
        }.bind(list),
        keys: arrKeys,
        list: list
      };
    };

    _proto6.getFlow = function getFlow(keys) {
      var flow = this.getFlowSettings(keys);
      var keysLen = flow.keys.length;
      flow.workers = [];
      Slice.arrayEach(this.workers, function (worker) {
        SliceWorkflow.buildWorker(worker);

        if (worker.keys === true || keysLen && Slice.some(worker.keys, flow.filter)) {
          flow.workers.push(worker);
        }
      });
      return flow;
    };

    _proto6.getCachedFlow = function getCachedFlow(keys) {
      if (!this.cache) {
        return null;
      }

      var cache, cacheKey;

      if (typeof keys === 'string' && this.quick[keys]) {
        cache = 'quickCache';
        cacheKey = keys;
      } else {
        cacheKey = Array.isArray(keys) ? keys.join(', ') : "" + keys;
        cache = 'cache';
      }

      if (!this[cache]) {
        return null;
      }

      if (!this[cache].hasOwnProperty(cacheKey)) {
        this[cache][cacheKey] = this.getFlow(keys);
      }

      return this[cache][cacheKey];
    };

    _proto6.runAll = function runAll(data, fn) {
      return SliceWorkflow.runFlow({
        asObject: true,
        list: {
          all: true
        },
        workers: this.workers
      }, data, fn);
    };

    _proto6.run = function run(keys, data, fn) {
      var flow = this.getCachedFlow(keys, true);

      if (flow === null) {
        this.runDirect(keys, fn);
      }

      return SliceWorkflow.runFlow(flow, data, fn);
    };

    _proto6.runDirect = function runDirect(keys, data, fn) {
      var _this12 = this;

      var flow = this.getFlowSettings(keys);
      var execute = typeof fn === 'function' ? fn : SliceWorkflow.runWorker;
      var keysLen = flow.keys.length;

      var cache = _extends({}, data || {});

      cache.list = flow.list;
      Slice.some(this.workers, function (worker) {
        return (worker.keys === true || keysLen && Slice.some(worker.keys, flow.filter)) && execute.call(_this12, worker, cache) === false;
      });
      return SliceWorkflow.handleResult(flow, cache);
    };

    return SliceWorkflow;
  }();

  Slice.Workflow = SliceWorkflow;

  var SliceWorkflowFactory = /*#__PURE__*/function () {
    function SliceWorkflowFactory() {
      this.resetRequirements();
      this.workers = [];
      this.runWorkers = [];
      this.runnableType = false;
      this.quickRunList = {};
      this.quickRunToType = {};
      this.addWorker = this.addWorker.bind(this);
      this.addRunnable = this.addRunnable.bind(this);
      this.setRunnableType = this.setRunnableType.bind(this);
      this.resetRunnableType = this.resetRunnableType.bind(this);
      this.setQuickRun = this.setQuickRun.bind(this);
      this.removeQuickRun = this.removeQuickRun.bind(this);
      this.addRequirement = this.addRequirement.bind(this);
      this.removeRequirement = this.removeRequirement.bind(this);
      this.resetRequirements = this.resetRequirements.bind(this);
    }

    SliceWorkflowFactory.parseRequirements = function parseRequirements(str) {
      var requirements = {};
      Slice.arrayEach(str.split(','), function (rawReq) {
        var requirement = rawReq.trim();
        var value = true;

        if (requirement.slice(0, 1) === '!') {
          value = false;
          requirement = requirement.slice(1).trim();
        }

        if (requirement) {
          requirements[requirement] = value;
        }
      });
      return requirements;
    };

    var _proto7 = SliceWorkflowFactory.prototype;

    _proto7.resetRequirements = function resetRequirements() {
      this.presetRequirements = {};
      this.presetRequirementsCount = 0;
    };

    _proto7.addRequirement = function addRequirement(requirement) {
      var _this13 = this;

      var requirements;

      if (typeof requirement === 'string') {
        requirements = SliceWorkflowFactory.parseRequirements(requirement);
      } else if (typeof requirement === 'object') {
        requirements = requirement;
      }

      if (requirements) {
        Slice.objectEach(requirements, function (value, req) {
          if (!_this13.presetRequirements.hasOwnProperty(req)) {
            ++_this13.presetRequirementsCount;
          }

          _this13.presetRequirements[req] = value;
        });
      }
    };

    _proto7.removeRequirement = function removeRequirement(requirement) {
      var _this14 = this;

      var requirements;

      if (typeof requirement === 'string') {
        requirements = SliceWorkflowFactory.parseRequirements(requirement);
      } else if (typeof requirement === 'object') {
        requirements = requirement;
      }

      if (requirements) {
        Slice.objectEach(requirements, function (value, req) {
          if (_this14.presetRequirements.hasOwnProperty(req) && _this14.presetRequirements[req] === requirements[req]) {
            delete _this14.presetRequirements[req];
            --_this14.presetRequirementsCount;
          }
        });
      }
    };

    _proto7.createRequirements = function createRequirements(value) {
      var _this15 = this;

      if (this.presetRequirementsCount || value) {
        var requirements;

        if (Array.isArray(value)) {
          requirements = [];
          Slice.arrayEach(value, function (requirement) {
            requirements.push(_this15.createRequirements(requirement));
          });
        } else {
          requirements = _extends({}, this.presetRequirements || {});

          switch (typeof value) {
            case 'string':
              requirements = _extends(requirements, SliceWorkflowFactory.parseRequirements(value));
              break;

            case 'object':
              requirements = _extends(requirements, value);
              break;

            default:
              requirements = this.presetRequirementsCount ? requirements : !!value;
              break;
          }
        }

        return requirements;
      }

      return false;
    };

    _proto7.createWorker = function createWorker(keys, fn, priority, requirements) {
      var worker = SliceWorkflow.createWorker(keys, fn, priority);

      if (worker) {
        worker.requirements = this.createRequirements(requirements);
      }

      return worker;
    };

    _proto7.addWorker = function addWorker(keys, fn, priority, enabled) {
      var worker = this.createWorker(keys, fn, priority, enabled);

      if (worker) {
        this.workers.push(worker);
      }

      return this;
    };

    _proto7.setRunnableType = function setRunnableType(name) {
      this.runnableType = "" + name;
      return this;
    };

    _proto7.resetRunnableType = function resetRunnableType() {
      this.runnableType = false;
      return this;
    };

    _proto7.addRunnable = function addRunnable(keys, fn, priority, enabled, workerType) {
      var worker = this.createWorker(keys, fn, priority, enabled);
      var type = !(workerType || this.runnableType) || workerType === true ? '_' : workerType || this.runnableType;

      if (!worker) {
        return;
      }

      if (!this.runWorkers[type]) {
        this.runWorkers[type] = [];
      }

      this.runWorkers[type].push(worker);
    };

    _proto7.setQuickRun = function setQuickRun(name, keys, expected, workerType) {
      this.removeQuickRun(name);
      var type = !(workerType || this.runnableType) || workerType === true ? '_' : workerType || this.runnableType;
      var exp;

      if (expected) {
        exp = expected;

        if (typeof exp === 'string') {
          exp = SliceTags.fromString(exp);

          if (exp.length === 1) {
            var _exp = exp;
            exp = _exp[0];
          }
        }
      } else {
        exp = 'result';
      }

      if (!this.quickRunList.hasOwnProperty(type)) {
        this.quickRunList[type] = {};
      }

      this.quickRunToType[name] = type;
      this.quickRunList[type][name] = {
        asObject: Array.isArray(exp),
        expected: exp,
        keys: SliceTags.fromValue(keys)
      };
    };

    _proto7.removeQuickRun = function removeQuickRun(name) {
      if (this.quickRunToType[name]) {
        delete this.quickRunList[this.quickRunToType[name]][name];
        delete this.quickRunToType[name];
      }
    };

    SliceWorkflowFactory.checkRequirements = function checkRequirements(requirements, check) {
      var enabled = !!check;

      if (Array.isArray(requirements)) {
        if (Slice.some(requirements, function (value) {
          return SliceWorkflowFactory.checkRequirements(value, check);
        })) {
          return enabled;
        }

        return false;
      }

      if (typeof requirements === 'object') {
        for (var requirement in requirements) {
          if (requirements[requirement] !== (typeof requirements[requirement] === 'boolean' ? !!check[requirement] : check[requirement])) {
            return false;
          }
        }
      }

      return enabled;
    };

    SliceWorkflowFactory.filterWorkers = function filterWorkers(list, requirements) {
      var workers;

      if (requirements) {
        workers = list.filter(function (worker) {
          return SliceWorkflowFactory.checkRequirements(worker.requirements, requirements);
        });
      } else {
        workers = list.slice(0);
      }

      workers.sort(function (item1, item2) {
        return item1.priority - item2.priority;
      });
      return workers;
    };

    _proto7.createManager = function createManager(requirements) {
      var _this16 = this;

      var manager = new SliceWorkflowManager(new SliceWorkflow(SliceWorkflowFactory.filterWorkers(this.workers, requirements), true));
      Slice.objectEach(this.runWorkers, function (list, key) {
        manager.addRunflow(key, new SliceWorkflow(SliceWorkflowFactory.filterWorkers(list, requirements), true, _this16.quickRunList[key]));
      });
      manager.quickRunList = this.quickRunList;
      manager.quickRunToType = this.quickRunToType;
      return manager;
    };

    return SliceWorkflowFactory;
  }();

  Slice.WorkflowFactory = SliceWorkflowFactory;

  var SliceWorkflowManager = /*#__PURE__*/function () {
    function SliceWorkflowManager(workflow, runflows) {
      var _this17 = this;

      this.workflow = null;
      this.runflows = {};
      this.quickRunList = {};
      this.quickRunToType = {};

      if (workflow) {
        this.setWorkflow(workflow);
      }

      if (typeof runflows === 'object') {
        Slice.objectEach(runflows, function (runflow, name) {
          _this17.addRunflow(name, runflow);
        });
      }
    }

    var _proto8 = SliceWorkflowManager.prototype;

    _proto8.setWorkflow = function setWorkflow(workflow) {
      if (workflow instanceof SliceWorkflow) {
        this.workflow = workflow;
      }

      return this;
    };

    _proto8.addRunflow = function addRunflow(name, runflow) {
      if (runflow instanceof SliceWorkflow) {
        this.runflows[name] = runflow;
      }

      return this;
    };

    _proto8.removeRunflow = function removeRunflow(name) {
      if (this.runflows.hasOwnProperty(name)) {
        delete this.runflows[name];
      }

      return this;
    };

    _proto8.setQuickRun = function setQuickRun(name, keys, expected, workerType) {
      this.removeQuickRun(name);
      var type = !workerType || workerType === true ? '_' : workerType || false;
      var exp;

      if (expected) {
        exp = expected;

        if (typeof exp === 'string') {
          exp = SliceTags.fromString(exp);

          if (exp.length === 1) {
            var _exp2 = exp;
            exp = _exp2[0];
          }
        }
      } else {
        exp = 'result';
      }

      if (!this.quickRunList.hasOwnProperty(type)) {
        this.quickRunList[type] = {};
      }

      this.quickRunToType[name] = type;
      this.quickRunList[type][name] = {
        asObject: Array.isArray(exp),
        expected: exp,
        keys: SliceTags.fromValue(keys)
      };
    };

    _proto8.removeQuickRun = function removeQuickRun(name) {
      if (this.quickRunToType[name]) {
        delete this.quickRunList[this.quickRunToType[name]][name];
        delete this.quickRunToType[name];
      }
    };

    _proto8.run = function run(keys, data, workerType, work) {
      var workflow = !workerType && typeof keys === 'string' && this.quickRunToType[keys] ? this.runflows[this.quickRunToType[keys]] : this.runflows[workerType && workerType !== true ? workerType : '_'];
      return workflow.run(keys, data, work);
    };

    _proto8.do = function _do(keys, data, work) {
      return this.workflow.run(keys, data, work);
    };

    _proto8.doAll = function doAll(data, work) {
      return this.workflow.runAll(data, work);
    };

    return SliceWorkflowManager;
  }();

  Slice.WorkflowManager = SliceWorkflowManager;

  var SliceWorkflowPlugin = /*#__PURE__*/function () {
    function SliceWorkflowPlugin(factory) {
      this.factory = null;
      this.invalidated = {};
      this.requirements = null;

      if (factory) {
        this.setFactory(factory);
      }
    }

    var _proto9 = SliceWorkflowPlugin.prototype;

    _proto9.setFactory = function setFactory(factory) {
      if (factory instanceof SliceWorkflowFactory) {
        this.factory = factory;
      }

      return this;
    };

    _proto9.hireManager = function hireManager(requirements) {
      this.requirements = requirements;
      this.manager = this.factory.createManager(requirements);
      return this;
    };

    _proto9.invalidate = function invalidate(part) {
      if (typeof part === 'string') {
        this.invalidated[part] = true;
      }

      return Object.keys(this.invalidated);
    };

    _proto9.run = function run(keys, data, workerType) {
      var _this18 = this;

      return this.manager.run(keys, data, workerType, function (worker, cache) {
        return worker.run.apply(_this18, _this18.getManagerData(cache));
      });
    };

    _proto9.update = function update() {
      var _this19 = this;

      var invalidated = this.invalidated;

      var work = function work(worker, cache) {
        var result = worker.run.apply(_this19, _this19.getManagerData(cache));

        if (result === 're-update') {
          _extends(_this19.invalidated, invalidated);

          _this19.update();

          result = false;
        }

        return result;
      };

      this.invalidated = {};

      if (invalidated.all) {
        this.manager.doAll(null, work);
      } else {
        this.manager.do(Object.keys(invalidated), null, work);
      }

      return this;
    };

    _proto9.getManagerData = function getManagerData(cache) {
      return [cache];
    };

    return SliceWorkflowPlugin;
  }();

  Slice.WorkflowPlugin = SliceWorkflowPlugin;
  var sliceEaseDefaults = {
    action: false,
    actionArgs: false,
    ease: 'linear',
    easeArgs: false,
    range: [0, 100]
  };

  var easeOut = function easeOut(fn) {
    return function (t, x) {
      return 1 - fn(1 - t, x);
    };
  };

  var easeInOut = function easeInOut(fn) {
    return function (t, x) {
      return t < 0.5 ? fn(2 * t, x) / 2 : (2 - fn(2 * (1 - t), x)) / 2;
    };
  };

  var easeIn = function easeIn(t, x) {
    var newX = x && x > 0 ? x : 2;
    return Math.pow(t, newX);
  };

  var easeList = {
    easeIn: easeIn,
    easeInOut: easeInOut(easeIn),
    easeOut: easeOut(easeIn),
    linear: function linear(progress) {
      return progress;
    }
  };

  var SliceEase = /*#__PURE__*/function () {
    SliceEase.toNumber = function toNumber(progress) {
      if (typeof progress === 'number' && !isNaN(progress)) {
        return Math.max(Math.min(progress, 1), 0);
      }

      return progress ? 1 : 0;
    };

    SliceEase.ease = function ease(name, func, inFN, outFn, inOutFn) {
      if (arguments.length < 2) {
        if (typeof name === 'string' && easeList.hasOwnProperty(name)) {
          return easeList[name];
        }

        return null;
      }

      var upCamel = Slice.ucfirst(name);

      if (inFN === true || typeof inFN === 'undefined') {
        easeList["easeIn" + upCamel] = func;
      } else if (typeof inFN === 'function') {
        easeList["easeIn" + upCamel] = inFN;
      } else {
        easeList[name] = func;
      }

      if (outFn === true || typeof outFn === 'undefined') {
        easeList["easeOut" + upCamel] = easeOut(func);
      } else if (typeof outFn === 'function') {
        easeList["easeOut" + upCamel] = outFn;
      }

      if (inOutFn === true || typeof inOutFn === 'undefined') {
        easeList["easeInOut" + upCamel] = easeInOut(func);
      } else if (typeof inOutFn === 'function') {
        easeList["easeInOut" + upCamel] = inOutFn;
      }

      return SliceEase;
    };

    SliceEase.has = function has(name) {
      return easeList.hasOwnProperty(name);
    };

    SliceEase.create = function create(range, ease, args, manual) {
      return new SliceEase({
        args: args,
        ease: ease,
        manual: manual,
        range: range
      });
    };

    function SliceEase(options) {
      var built = _extends(this.getDefaults(), options || {});

      this.setEase(built.ease, built.args).setRange(built.range).setAction(built.action, built.actionArgs);

      if (!this.manual) {
        this.progress(built.progress);
      }
    }

    var _proto10 = SliceEase.prototype;

    _proto10.getDefaults = function getDefaults() {
      return _extends({}, sliceEaseDefaults, SliceEase.defaults || {});
    };

    _proto10.setEase = function setEase(ease, args) {
      var easing;

      if (ease) {
        var type = typeof ease;

        if (type === 'function') {
          easing = ease;
        } else if (type === 'string' && SliceEase.has(ease)) {
          easing = easeList[ease];
        } else {
          easing = easeList.linear;
        }
      } else {
        easing = easeList.linear;
      }

      this.ease = easing;
      this.args = Array.isArray(args) ? args : [args];
      return this;
    };

    _proto10.setAction = function setAction(action, args) {
      var built;

      if (action) {
        var type = typeof action;

        if (type === 'function') {
          built = action;
        } else {
          built = type === 'string' && typeof Math[action] === 'function' ? action : 'round';
          built = Math[built].bind(Math);
        }

        this.action = built;
        this.actionArgs = Array.isArray(args) ? args : [args];
      } else {
        this.action = null;
        this.actionArgs = null;
      }

      return this;
    };

    _proto10.setRange = function setRange(range) {
      this.start = 0;
      this.end = 100;

      if (Array.isArray(range)) {
        if (range.length > 0 && typeof range[0] === 'number') {
          this.start = range[0];
        }

        if (range.length > 1 && typeof range[1] === 'number') {
          this.end = range[1];
        }
      }

      var diff = this.end - this.start;
      this.diff = Math.abs(diff);
      this.mult = this.diff ? diff / this.diff : 1;
      this.value = this.start;
      return this;
    };

    _proto10.progress = function progress(_progress, force) {
      var built = false;

      if (typeof _progress !== 'undefined') {
        built = SliceEase.toNumber(_progress);
      } else if (force) {
        built = this.delta;
      }

      if (built !== false) {
        var value = this.start + this.mult * this.diff * this.ease(built, this.args);

        if (this.action) {
          value = this.action.apply(this, [value].concat(this.actionArgs));
        }

        this.delta = built;
        this.value = value;
      }

      return this.value;
    };

    return SliceEase;
  }();

  _defineProperty(SliceEase, "defaults", {});

  SliceEase.ease('circ', function (t) {
    return 1 - Math.sin(Math.acos(t));
  });
  SliceEase.ease('back', function (t, x) {
    var newX = x || 1.5;
    return Math.pow(t, 2) * ((newX + 1) * t - newX);
  });
  SliceEase.ease('bounce', function (t) {
    for (var a = 0, b = 1;; a += b, b /= 2) {
      if (t >= (7 - 4 * a) / 11) {
        return -Math.pow((11 - 6 * a - 11 * t) / 4, 2) + Math.pow(b, 2);
      }
    }
  });
  SliceEase.ease('elastic', function (t, x) {
    var newX = x && x > 0 ? x : 10;
    return Math.pow(2, newX * (t - 1)) * Math.sin(10.5 * Math.PI * t);
  });
  SliceEase.ease('wave', function (t, x) {
    var newX = (x && x > 0 ? x : 2) - 1;
    return (newX ? Math.pow(2 - 1 / newX, 2 * (t - 1)) : 1) * Math.sin((2 * newX + 0.5) * Math.PI * t);
  });
  Slice.Ease = SliceEase;

  var SliceCheckpointData = /*#__PURE__*/function () {
    SliceCheckpointData.create = function create(value, checkpointType, baseData) {
      if (value instanceof SliceCheckpointData) {
        return value;
      }

      var data = _extends({}, baseData ? SliceCheckpointData.create(baseData, checkpointType).data : {}, value instanceof SliceCheckpoint ? value.getMatchData() : (SliceCheckpoint.getRegistered(checkpointType) || SliceCheckpoint).createMatchData(value));

      return new SliceCheckpointData(data);
    };

    function SliceCheckpointData(data) {
      this.data = data;
    }

    return SliceCheckpointData;
  }();

  Slice.CheckpointData = SliceCheckpointData;
  var checkpointDefaults = {
    tags: false
  };
  var checkpointTypes = {};

  var SliceCheckpoint = /*#__PURE__*/function () {
    SliceCheckpoint.register = function register(name, checkpoint) {
      if (checkpoint.prototype instanceof SliceCheckpoint) {
        var names = SliceTags.fromValue(name, true);
        Slice.arrayEach(names, function (regName) {
          checkpointTypes[regName] = checkpoint;
        });
      }
    };

    SliceCheckpoint.isRegistered = function isRegistered(value) {
      var tags = SliceTags.fromValue(value, true);

      if (tags.length) {
        return Slice.some(tags, function (tag) {
          return checkpointTypes.hasOwnProperty(tag);
        });
      }

      try {
        if (value.prototype instanceof SliceCheckpoint) {
          return Slice.objectSome(checkpointTypes, function (CheckpointType) {
            return value.prototype instanceof CheckpointType;
          });
        }
      } catch (_unused) {
        /* Empty */
      }

      return false;
    };

    SliceCheckpoint.getRegistered = function getRegistered(value) {
      var tags = SliceTags.fromValue(value, true);
      var Checkpoint = false;

      if (tags.length) {
        Slice.some(tags, function (tag) {
          if (checkpointTypes.hasOwnProperty(tag)) {
            Checkpoint = checkpointTypes[tag];
            return true;
          }
        });
      } else {
        try {
          if (value.prototype instanceof SliceCheckpoint) {
            return Slice.objectSome(checkpointTypes, function (CheckpointType) {
              if (value.prototype instanceof CheckpointType) {
                Checkpoint = value;
                return true;
              }
            });
          }
        } catch (_unused2) {
          /* Empty */
        }
      }

      return Checkpoint;
    };

    SliceCheckpoint.create = function create(options, checkpointType) {
      return new (SliceCheckpoint.getRegistered(checkpointType) || SliceCheckpoint)(options);
    };

    function SliceCheckpoint(options) {
      this.options(options instanceof SliceCheckpointData ? SliceCheckpointData.data : options);
    }

    var _proto11 = SliceCheckpoint.prototype;

    _proto11.getDefaults = function getDefaults() {
      return _extends({}, checkpointDefaults, SliceCheckpoint.defaults);
    };

    _proto11.setup = function setup(options) {
      this.tags = new SliceTags(options.tags);
    };

    _proto11.options = function options(_options) {
      this.options = _extends(this.getDefaults(), _options || {});
      this.setup(_options);
      return this;
    };

    _proto11.hasTags = function hasTags(tags) {
      return this.tags.has(tags);
    };

    _proto11.isCrossedBy = function isCrossedBy(value) {
      var match = SliceCheckpointData.create(value, 'tags');
      return this.tags.allIn(match.data.tags);
    };

    _proto11.isBefore = function isBefore(value) {
      var match = SliceCheckpointData.create(value, 'tags');
      return this.tags.allIn(match.data.tags);
    };

    _proto11.getMatchData = function getMatchData() {
      return {
        tags: this.tags
      };
    };

    SliceCheckpoint.createMatchData = function createMatchData(value) {
      if (value instanceof SliceCheckpoint) {
        return value.getMatchData();
      }

      var data = {};
      var tags = value;

      if (typeof value === 'object') {
        data = _extends({}, value);
        tags = value.tags;
      }

      data.tags = SliceTags.fromValue(tags);
      return data;
    };

    return SliceCheckpoint;
  }();

  _defineProperty(SliceCheckpoint, "defaults", {});

  SliceCheckpoint.register('base, core, tags', SliceCheckpoint);
  Slice.Checkpoint = SliceCheckpoint;
  var progressCheckpointDefaults = {
    progress: false,
    strict: false
  };

  var SliceProgressCheckpoint = /*#__PURE__*/function (_SliceCheckpoint) {
    _inheritsLoose(SliceProgressCheckpoint, _SliceCheckpoint);

    function SliceProgressCheckpoint() {
      return _SliceCheckpoint.apply(this, arguments) || this;
    }

    var _proto12 = SliceProgressCheckpoint.prototype;

    _proto12.getDefaults = function getDefaults() {
      return _extends({}, _SliceCheckpoint.prototype.getDefaults.call(this), progressCheckpointDefaults, SliceProgressCheckpoint.defaults);
    };

    _proto12.setup = function setup(options) {
      this.progress = SliceEase.toNumber(options.progress);

      _SliceCheckpoint.prototype.setup.call(this, options);
    };

    _proto12.isCrossedBy = function isCrossedBy(value) {
      var match = SliceCheckpointData.create(value, 'progress');
      return this.progress <= match.progress && (!(this.options.strict || match.strict) || _SliceCheckpoint.prototype.isCrossedBy.call(this, match));
    };

    _proto12.isBefore = function isBefore(value) {
      var match = SliceCheckpointData.create(value, 'progress');
      return this.progress <= match.progress;
    };

    _proto12.getMatchData = function getMatchData() {
      return _extends({}, _SliceCheckpoint.prototype.getMatchData.call(this), {
        progress: this.progress,
        strict: this.options.strict
      });
    };

    SliceProgressCheckpoint.createMatchData = function createMatchData(value) {
      if (value instanceof SliceProgressCheckpoint) {
        return value.getMatchData();
      }

      var data = {};
      var progress = value;
      var strict = false;

      if (typeof value === 'object') {
        data = value;
        progress = value.progress;
        strict = !value.strict;
      }

      data.progress = SliceEase.toNumber(progress);
      data.strict = strict;
      return SliceProgressCheckpoint.createMatchData(data);
    };

    return SliceProgressCheckpoint;
  }(SliceCheckpoint);

  _defineProperty(SliceProgressCheckpoint, "defaults", {});

  SliceCheckpoint.register('progress, percent', SliceProgressCheckpoint);
  var sliceDistanceCheckpointDefaults = {
    distance: false,
    distanceLimit: false
  };

  var SliceDistanceCheckpoint = /*#__PURE__*/function (_SliceProgressCheckpo) {
    _inheritsLoose(SliceDistanceCheckpoint, _SliceProgressCheckpo);

    function SliceDistanceCheckpoint() {
      return _SliceProgressCheckpo.apply(this, arguments) || this;
    }

    var _proto13 = SliceDistanceCheckpoint.prototype;

    _proto13.getDefaults = function getDefaults() {
      return _extends({}, _SliceProgressCheckpo.prototype.getDefaults.call(this), sliceDistanceCheckpointDefaults, SliceDistanceCheckpoint.defaults);
    };

    _proto13.setup = function setup(options) {
      this.distance = options.distance || 0;
      var limit = options.distanceLimit === false ? 0 : Math.max(options.distanceLimit, 0);

      _SliceProgressCheckpo.prototype.setup.call(this, _extends({}, options, {
        progress: limit ? this.distance / limit : 0
      }));
    };

    _proto13.isCrossedBy = function isCrossedBy(value) {
      var match = SliceCheckpointData.create(value, 'distance');
      return this.distance <= match.distance && (!(this.options.strict || match.strict) || _SliceProgressCheckpo.prototype.isCrossedBy.call(this, match));
    };

    _proto13.isBefore = function isBefore(value) {
      var match = SliceCheckpointData.create(value, 'distance');
      return this.distance <= match.distance;
    };

    _proto13.getMatchData = function getMatchData() {
      return _extends({}, _SliceProgressCheckpo.prototype.getMatchData.call(this), {
        distance: this.distance
      });
    };

    SliceDistanceCheckpoint.createMatchData = function createMatchData(value) {
      if (value instanceof SliceDistanceCheckpoint) {
        return value.getMatchData();
      }

      var data = {};
      var distance = value;
      var limit = 0;

      if (typeof value === 'object') {
        data = value;
        distance = value.distance;
        limit = Math.max(value.distanceLimit || 0, 0);
      }

      data.progress = limit ? distance / limit : 0;
      data.distance = distance;
      return SliceProgressCheckpoint.createMatchData(data);
    };

    return SliceDistanceCheckpoint;
  }(SliceProgressCheckpoint);

  _defineProperty(SliceDistanceCheckpoint, "defaults", {});

  SliceCheckpoint.register('distance', SliceDistanceCheckpoint);
  var sliceTimePassCheckpointDefaults = {
    timeLimit: false,
    timePass: false
  };

  var SliceTimePassCheckpoint = /*#__PURE__*/function (_SliceProgressCheckpo2) {
    _inheritsLoose(SliceTimePassCheckpoint, _SliceProgressCheckpo2);

    function SliceTimePassCheckpoint() {
      return _SliceProgressCheckpo2.apply(this, arguments) || this;
    }

    var _proto14 = SliceTimePassCheckpoint.prototype;

    _proto14.getDefaults = function getDefaults() {
      return _extends({}, _SliceProgressCheckpo2.prototype.getDefaults.call(this), sliceTimePassCheckpointDefaults, SliceTimePassCheckpoint.defaults);
    };

    _proto14.setup = function setup(options) {
      this.timePass = SliceDelay.toDuration(options.timePass);
      var limit = options.timeLimit === false ? 0 : SliceDelay.toDuration(options.timeLimit);

      _SliceProgressCheckpo2.prototype.setup.call(this, _extends({}, options, {
        progress: limit ? this.timePass / limit : 0
      }));
    };

    _proto14.isCrossedBy = function isCrossedBy(value) {
      var match = SliceCheckpointData.create(value, 'timePass');
      return this.timePass <= match.timePass && (!(this.options.strict || match.strict) || _SliceProgressCheckpo2.prototype.isCrossedBy.call(this, match));
    };

    _proto14.isBefore = function isBefore(value) {
      var match = SliceCheckpointData.create(value, 'timePass');
      return this.timePass <= match.timePass;
    };

    _proto14.getMatchData = function getMatchData() {
      return _extends({}, _SliceProgressCheckpo2.prototype.getMatchData.call(this), {
        timePass: this.timePass
      });
    };

    SliceTimePassCheckpoint.createMatchData = function createMatchData(value) {
      if (value instanceof SliceTimePassCheckpoint) {
        return value.getMatchData();
      }

      var data = {};
      var timePass = value;
      var limit = 0;

      if (typeof value === 'object') {
        data = value;
        timePass = value.timePass;
        limit = SliceDelay.toDuration(value.timeLimit);
      }

      data.progress = limit ? timePass / limit : 0;
      data.timePass = SliceDelay.toDuration(timePass);
      return SliceProgressCheckpoint.createMatchData(data);
    };

    return SliceTimePassCheckpoint;
  }(SliceProgressCheckpoint);

  _defineProperty(SliceTimePassCheckpoint, "defaults", {});

  SliceCheckpoint.register('duration, delay, timepass, timePass, timer', SliceTimePassCheckpoint);
  Slice.TimePassCheckpoint = SliceTimePassCheckpoint;
  var actionDefaults = {
    action: false,
    count: 0,
    executed: false,
    repeatable: true
  };

  function isThisAction(action) {
    switch (typeof action) {
      case 'object':
        return action.isAction && action.do === this.do;

      case 'function':
        return action === this.do;

      default:
        return false;
    }
  }

  var SliceAction = /*#__PURE__*/function () {
    SliceAction.create = function create(value) {
      if (value instanceof SliceAction) {
        return value;
      }

      return new SliceAction(typeof value === 'object' ? value : {
        action: value
      });
    };

    function SliceAction(options) {
      this.iteration = 0;
      this.setOptions(_extends(this.getDefaults(), options || {}));
    }

    var _proto15 = SliceAction.prototype;

    _proto15.getDefaults = function getDefaults() {
      return _extends({}, actionDefaults, SliceAction.defaults || {});
    };

    _proto15.setOptions = function setOptions(options) {
      var builtOptions = _extends(this.getDefaults(), options || {});

      this.options = builtOptions;
      this.id = builtOptions.hasOwnProperty('id') ? builtOptions.id : null;
      this.repeat = builtOptions.repeat;

      if (builtOptions.hasOwnProperty('executed')) {
        this.executed = !!builtOptions.executed;

        if (typeof builtOptions.executed === 'number') {
          this.iteration = Math.max(builtOptions.executed, 0);
        } else {
          this.iteration = this.executed ? 1 : 0;
        }
      }

      this.action = typeof builtOptions.action === 'function' ? builtOptions.action : Slice.noop;
      return this;
    };

    _proto15.is = function is(action, strict) {
      if (action instanceof SliceAction) {
        if (action === this) {
          return true;
        } else if (strict) {
          return false;
        }

        return action.action === this.action || this.id !== null && this.id === action.id;
      }

      switch (typeof action) {
        case 'function':
          return action === this.action;

        default:
          return this.id !== null && this.id === action;
      }
    };

    _proto15.do = function _do(data) {
      if (this.executed && !this.repeat) {
        return null;
      }

      return this.action(data);
    };

    return SliceAction;
  }();

  _defineProperty(SliceAction, "defaults", {});

  Slice.Action = SliceAction;
  var triggerDefaults = {
    actions: false,
    ready: true,
    repeatManualy: false,
    repeatReady: true,
    repeatable: false,
    startManualy: false
  };
  var sliceTriggerStates = {
    canceled: 'completed, skipped',
    clean: true,
    done: 'completed, executed',
    failed: 'completed, skipped',
    init: true,
    preparation: 'pending',
    ready: 'execute',
    removed: true,
    started: 'pending',
    triggered: 'execute',
    updateTags: 'updateCheckpoints'
  };
  var SliceTriggerTypes = {};

  var SliceTrigger = /*#__PURE__*/function () {
    SliceTrigger.run = function run(actions, trigger, data) {
      if (typeof actions === 'object') {
        Slice.objectEach(actions, function (action) {
          action.do(trigger, data);
        });
      }
    };

    SliceTrigger.register = function register(name, checkpoint) {
      if (checkpoint.prototype instanceof SliceTrigger) {
        var names = SliceTags.fromValue(name, true);
        Slice.arrayEach(names, function (regName) {
          SliceTriggerTypes[regName] = checkpoint;
        });
      }
    };

    SliceTrigger.isRegistered = function isRegistered(value) {
      var tags = SliceTags.fromValue(value, true);

      if (tags.length) {
        return Slice.some(tags, function (tag) {
          return SliceTriggerTypes.hasOwnProperty(tag);
        });
      }

      try {
        if (value.prototype instanceof SliceTrigger) {
          return Slice.objectSome(SliceTriggerTypes, function (TriggerType) {
            return value.prototype instanceof TriggerType;
          });
        }
      } catch (_unused3) {
        /* Empty */
      }

      return false;
    };

    SliceTrigger.getRegistered = function getRegistered(value) {
      var tags = SliceTags.fromValue(value, true);
      var Checkpoint = false;

      if (tags.length) {
        Slice.some(tags, function (tag) {
          if (SliceTriggerTypes.hasOwnProperty(tag)) {
            Checkpoint = SliceTriggerTypes[tag];
            return true;
          }
        });
      } else {
        try {
          if (value.prototype instanceof SliceTrigger) {
            return Slice.objectSome(SliceTriggerTypes, function (TriggerType) {
              if (value.prototype instanceof TriggerType) {
                Checkpoint = value;
                return true;
              }
            });
          }
        } catch (_unused4) {
          /* Empty */
        }
      }

      return Checkpoint;
    };

    SliceTrigger.create = function create(options, checkpointType) {
      return new (SliceTrigger.getRegistered(checkpointType) || SliceTrigger)(options);
    };

    function SliceTrigger(options) {
      _defineProperty(this, "checkpointType", 'tags');

      this.options = {};
      this.actions = {};
      this.quickValues = {};
      this.preset = {};
      this.flow = {};
      this.maxIterations = Infinity;
      this.state = new SliceState(sliceTriggerStates);
      this.state.enter('clean');
      this.setOptions(options || {}, true);
    }

    var _proto16 = SliceTrigger.prototype;

    _proto16.getDefaults = function getDefaults() {
      return _extends({}, triggerDefaults, SliceTrigger.defaults || {});
    };

    _proto16.getDefaultQuickValues = function getDefaultQuickValues() {
      return {};
    };

    _proto16.setOptions = function setOptions(options, fromScratch) {
      if (fromScratch) {
        this.cleanup();
        this.options = _extends(this.getDefaults(), options || {});
        this.init();
      } else if (options) {
        this.updateOptions(options);
        this.options = _extends(this.options, options || {});

        if (this.iteration === 1) {
          this.prepareReset(this.preset);
        } else {
          this.prepareRepeat(this.preset);
        }

        if (this.state.is('started')) {
          this.adjustFlow(this.flow, this.preset);
          this.update();
        }
      }

      return this;
    };

    _proto16.updateOptions = function updateOptions(options) {
      var _this20 = this;

      if (options.actions && Array.isArray(options.actions)) {
        Slice.arrayEach(options.actions, function (action) {
          _this20.addAction(action);
        });
      }

      var actProp = 'Actions';
      var actPropLen = actProp.length;
      Slice.objectEach(options, function (value, name) {
        if (Array.isArray(value) && name.indexOf(actProp, name.length - actPropLen) !== -1) {
          var actionType = name.slice(0, name.length - actPropLen);
          Slice.arrayEach(value, function (action) {
            _this20.addAction(action, actionType);
          });
        }
      });
      var quickValues = this.getDefaultQuickValues();

      if (typeof options.quickValues === 'object') {
        _extends(quickValues, options.quickValues);
      }

      Slice.objectEach(quickValues, function (value, name) {
        _this20.setQuickValue(name, value);
      });

      if (typeof options.repeat === 'number') {
        this.maxIterations = Math.max(Math.ceil(options.repeat), 0) + 1;
      }
    };

    _proto16.cleanup = function cleanup() {
      if (this.state.is('clean')) {
        return false;
      }

      this.options = {};
      this.actions = {};
      this.preset = {};
      this.flow = {};
      this.maxIterations = Infinity;
      this.state.reset().is('clean');
      return true;
    };

    _proto16.init = function init() {
      this.state.enter('init');
      this.updateOptions(this.options);
      this.state.exit('init');
      this.reset(null, true);
    };

    _proto16.prepareReset = function prepareReset(preset) {
      this.iteration = 1;
      preset.ready = this.options.ready;
    };

    _proto16.prepareRepeat = function prepareRepeat(preset) {
      preset.ready = this.options.repeatReady;
    };

    _proto16.adjustFlow = function adjustFlow(flow, value) {
      this.adjustSettings(flow, value);
    };

    _proto16.adjustSettings = function adjustSettings(settings, value) {
      if (typeof value === 'object') {
        if (value.hasOwnProperty('ready')) {
          settings.ready = !!value.ready;
        }
      } else {
        settings.ready = !!value;
      }
    };

    _proto16.prepareStart = function prepareStart(flow, preset) {
      _extends(flow, preset);

      flow.values = {};
      flow.tags = new SliceTags();
      flow.checkpoints = [];
    };

    _proto16.reset = function reset(options, total) {
      var startManualy = this.options.startManualy;

      if (total) {
        this.state.reset().enter('preparation');
        SliceTrigger.run(this.actions.totalReset, this);
        this.preset = {};
        this.prepareReset(this.preset);

        if (this.state.not('completed')) {
          if (this.iteration <= this.maxIterations) {
            if (startManualy) {
              this.stop(true);
            } else {
              this.start(options);
            }
          } else {
            this.cancel();
          }
        }
      } else if (this.canStart()) {
        var restart = this.state.not('started');
        this.state.reset().enter('preparation');
        SliceTrigger.run(this.actions.reset, this);

        if (restart) {
          if (!startManualy || this.iteration > 1 && !this.options.repeatManualy) {
            return this.start();
          }
        } else {
          this.state.enter('started');
        }

        if (options !== null && typeof options !== 'undefined') {
          this.adjustSettings(this.preset, options);
        }

        this.flow = {};
        this.prepareStart(this.flow, this.preset);
        this.update();
      }

      return this;
    };

    _proto16.canStart = function canStart() {
      return this.state.not('failed');
    };

    _proto16.start = function start(options) {
      if (this.canStart()) {
        this.flow = {};
        this.state.reset().enter('started');

        if (options !== null && typeof options !== 'undefined') {
          this.adjustSettings(this.preset, options);
        }

        this.prepareStart(this.flow, this.preset);
        SliceTrigger.run(this.actions.start, this);
        this.update();
      }

      return this;
    };

    _proto16.repeat = function repeat(options) {
      var state = this.state;

      if (state.is('triggered')) {
        if (this.canRepeat()) {
          this.preset = {};
          this.state.reset().enter('preparation');
          SliceTrigger.run(this.actions.repeat, this);
          this.prepareRepeat(this.preset);
          this.start(options);
        } else {
          this.finish();
        }
      } else {
        this.reset(options);
      }

      return this;
    };

    _proto16.stop = function stop(silent) {
      if (this.canStart()) {
        this.flow = {};
        this.state.reset();
        this.prepareStart(this.flow, this.preset);

        if (!silent) {
          SliceTrigger.run(this.actions.stop, this);
        }

        this.update();
      }

      return this;
    };

    _proto16.isReady = function isReady() {
      return this.flow.ready;
    };

    _proto16.prepareReady = function prepareReady(flow) {
      flow.ready = true;
    };

    _proto16.update = function update(ready) {
      if (this.state.is('started')) {
        this.flow.values = {};

        if (ready) {
          this.prepareReady(this.flow);
        }

        SliceTrigger.run(this.actions.update, this);
        this.updateCheckpoints();

        if (this.isReady()) {
          this.state.reset().enter('ready');
          this.trigger();
        }
      }

      return this;
    };

    _proto16.trigger = function trigger(data) {
      var state = this.state;

      if (state.is('ready')) {
        ++this.iteration;
        state.exit('ready').enter('triggered');
        this.do(data);

        if (!this.options.repeatManualy) {
          this.repeat();
        } else if (!this.canRepeat()) {
          this.finish();
        }
      }

      return this;
    };

    _proto16.canRepeat = function canRepeat() {
      return this.options.repeat && this.iteration < this.maxIterations;
    };

    _proto16.do = function _do() {
      SliceTrigger.run(this.actions.trigger, this);
      return this;
    };

    _proto16.cancel = function cancel() {
      if (this.state.is('completed')) {
        return false;
      }

      this.state.reset().enter('canceled');
      SliceTrigger.run(this.actions.cancel, this);
      return true;
    };

    _proto16.finish = function finish() {
      if (this.state.is('started')) {
        this.flow.values = {};
        this.state.reset().enter('done');
        SliceTrigger.run(this.actions.done, this);
        return true;
      }

      return false;
    };

    _proto16.addAction = function addAction(action, type, execute, data) {
      var builtAction = SliceAction.create(action);
      var builtType = type || 'trigger';
      var actions = this.actions[builtType];

      if (!actions) {
        actions = {};
        this.actions[builtType] = actions;
      }

      if (builtAction.id === null) {
        builtAction.id = Slice.uniqueId(actions);
      }

      actions[builtAction.id] = builtAction;

      if (execute || this.state.is('done')) {
        builtAction.do(data);
      }

      return this;
    };

    _proto16.removeAction = function removeAction(action, type, execute, data) {
      var actionType = type || 'trigger';
      var bindAction = this.getAction(action, actionType);

      if (bindAction) {
        delete this.actions[actionType][bindAction.id];

        if (execute) {
          bindAction.do(data);
        }
      }

      return this;
    };

    _proto16.getAction = function getAction(action, type) {
      var actions = this.actions[type || 'trigger'];

      if (actions) {
        for (var id in actions) {
          if (actions[id].is(action)) {
            return actions[id];
          }
        }
      }

      return null;
    };

    _proto16.setQuickValue = function setQuickValue(name, value) {
      this.quickValues[name] = typeof value === 'function' ? this.quickValues[name] = value : function () {
        return value;
      };
      return this;
    };

    _proto16.removeQuickValue = function removeQuickValue(name) {
      if (this.quickValues.hasOwnProperty(name)) {
        delete this.quickValues[name];
      }

      if (this.flow.values.hasOwnProperty(name)) {
        delete this.flow.values[name];
      }

      return this;
    };

    _proto16.get = function get(name) {
      var _this21 = this;

      var flow = this.flow;

      if (typeof name === 'undefined') {
        Slice.objectEach(this.quickValues, function (quickValue, quickName) {
          if (!flow.values.hasOwnProperty(quickName)) {
            flow.values[quickName] = quickValue(_this21);
          }
        });
        return flow.values;
      }

      if (this.quickValues.hasOwnProperty(name)) {
        if (!flow.values.hasOwnProperty(name)) {
          flow.values[name] = this.quickValues[name](this);
        }

        return flow.values[name];
      }

      return flow.values.hasOwnProperty(name) ? flow.values[name] : null;
    };

    _proto16.addCheckpoint = function addCheckpoint(value, type, update) {
      if (this.state.is('started')) {
        var checkpoint = value instanceof SliceCheckpoint ? value : SliceCheckpoint.create(SliceCheckpointData.create(value, this.checkpointType, this.getCheckData()), type || this.checkpointType);
        this.flow.checkpoints.push(checkpoint);

        if (update) {
          this.updateCheckpoints();
        }
      }

      return this;
    };

    _proto16.removeCheckpoint = function removeCheckpoint(value) {
      if (this.state.is('started')) {
        var flow = this.flow;

        if (typeof value === 'undefined') {
          flow.checkpoints = [];
        } else if (value instanceof SliceCheckpoint) {
          Slice.some(flow.checkpoints, function (checkpoint, i) {
            if (checkpoint === value) {
              flow.checkpoints.splice(i, 1);
              return true;
            }
          });
        } else {
          var builtKeys = SliceTags.fromValue(value, true);

          if (builtKeys.length) {
            var checkpoints = flow.checkpoints;
            var i = checkpoints.length - 1;

            for (; i >= 0; i--) {
              if (checkpoints[i].hasTags(builtKeys)) {
                checkpoints.splice(i, 1);
              }
            }
          }
        }
      }

      return this;
    };

    _proto16.addTags = function addTags(tags) {
      if (this.state.is('started')) {
        this.flow.tags.add(tags);
        this.state.enter('updateTags');
      }

      return this;
    };

    _proto16.updateCheckpoints = function updateCheckpoints() {
      var _this22 = this;

      if (this.state.is('updateCheckpoints') && this.flow.checkpoints.length) {
        var checkpoints = this.getCheckpoints();
        var checkData = SliceCheckpointData.create(this.getCheckData(), this.checkpointType);
        Slice.some(checkpoints.slice(), function (checkpoint) {
          if (!_this22.checkCheckpoint(checkpoint, checkData)) {
            return true;
          }
        });
      }

      return this;
    };

    _proto16.getCheckpoints = function getCheckpoints() {
      return this.state.is('started') ? this.flow.checkpoints : [];
    };

    _proto16.getCheckData = function getCheckData() {
      return {
        tags: this.flow.tags
      };
    };

    _proto16.checkCheckpoint = function checkCheckpoint(checkpoint, checkData) {
      if (checkpoint.isBefore(checkData)) {
        SliceTrigger.run(this.actions.checkpoint, this, {
          checkpoint: checkpoint
        });
        this.removeCheckpoint(checkpoint);
      }

      return true;
    };

    _proto16.remove = function remove() {
      this.cleanup();
      this.state.enter('removed');
    };

    return SliceTrigger;
  }();

  _defineProperty(SliceTrigger, "defaults", {});

  SliceTrigger.register('base, core, direct', SliceTrigger);
  Slice.Trigger = SliceTrigger;
  var progressDefaults = {
    ease: 'linear',
    easeArguments: false,
    ready: false,
    repeatReady: false
  };
  var progressQuickValuesDefaults = {
    progress: function progress(trigger) {
      return trigger.state.is('started') ? trigger.flow.progress : 0;
    }
  };

  var progressBuildSettings = function progressBuildSettings(settings, progress) {
    settings.progress = SliceEase.toNumber(progress);
    settings.ready = settings.progress === 1;
  };

  var SliceProgress = /*#__PURE__*/function (_SliceTrigger) {
    _inheritsLoose(SliceProgress, _SliceTrigger);

    function SliceProgress() {
      var _this23;

      for (var _len12 = arguments.length, args = new Array(_len12), _key12 = 0; _key12 < _len12; _key12++) {
        args[_key12] = arguments[_key12];
      }

      _this23 = _SliceTrigger.call.apply(_SliceTrigger, [this].concat(args)) || this;

      _defineProperty(_assertThisInitialized(_this23), "checkpointType", 'progress');

      return _this23;
    }

    var _proto17 = SliceProgress.prototype;

    _proto17.getDefaults = function getDefaults() {
      return _extends({}, _SliceTrigger.prototype.getDefaults.call(this), progressDefaults, SliceProgress.defaults);
    };

    _proto17.getDefaultQuickValues = function getDefaultQuickValues() {
      return _extends({}, _SliceTrigger.prototype.getDefaultQuickValues.call(this), progressQuickValuesDefaults, SliceProgress.quickValuesDefaults);
    };

    _proto17.updateOptions = function updateOptions(options) {
      this.setEase('percent', SliceEase.create([0, 100], options.ease, options.easeArguments, true));

      _SliceTrigger.prototype.updateOptions.call(this, options);
    };

    _proto17.init = function init() {
      this.easings = {};
      this.state.add('updateProgress', 'updateCheckpoints');

      _SliceTrigger.prototype.init.call(this);
    };

    _proto17.isReady = function isReady() {
      return this.flow.progress === 1;
    };

    _proto17.prepareReset = function prepareReset(preset) {
      this.iteration = 1;
      progressBuildSettings(preset, this.options.ready);
    };

    _proto17.prepareRepeat = function prepareRepeat(preset) {
      progressBuildSettings(preset, this.options.repeatReady);
    };

    _proto17.adjustSettings = function adjustSettings(settings, value) {
      if (typeof value === 'object') {
        if (value.hasOwnProperty('progress')) {
          progressBuildSettings(settings, value.progress);
        }
      } else {
        progressBuildSettings(settings, value);
      }
    };

    _proto17.update = function update(progress) {
      return this.setProgress(progress);
    };

    _proto17.setProgress = function setProgress(progress) {
      if (this.state.is('started')) {
        var flow = this.flow;

        if (typeof progress === 'number') {
          flow.progress = SliceEase.toNumber(progress);
          this.state.enter('updateProgress');
        }

        var ready = this.isReady();

        _SliceTrigger.prototype.update.call(this, ready);

        return ready;
      }

      return false;
    };

    _proto17.setEase = function setEase(name, ease) {
      if (ease instanceof SliceEase) {
        this.easings[name] = ease;

        if (this.quickValues.hasOwnProperty(name)) {
          delete this.quickValues[name];
        }
      }

      return this;
    };

    _proto17.removeEase = function removeEase(name) {
      if (this.easings.hasOwnProperty(name)) {
        delete this.easings[name];
      }

      if (this.flow.values.hasOwnProperty(name)) {
        delete this.flow.values[name];
      }

      return this;
    };

    _proto17.setQuickValue = function setQuickValue(name, value) {
      if (value instanceof SliceEase) {
        return this.setEase(name, value);
      }

      if (this.easings.hasOwnProperty(name)) {
        this.removeEase(name);
      }

      return _SliceTrigger.prototype.setQuickValue.call(this, name, value);
    };

    _proto17.get = function get(name) {
      var flow = this.flow;

      if (typeof name === 'undefined') {
        _SliceTrigger.prototype.get.call(this);

        Slice.objectEach(this.easings, function (ease, easeName) {
          if (!flow.values.hasOwnProperty(easeName)) {
            flow.values[easeName] = ease.progress(flow.progress);
          }
        });
        return flow.values;
      }

      if (this.easings.hasOwnProperty(name)) {
        if (!flow.values.hasOwnProperty(name)) {
          flow.values[name] = this.easings[name].progress(flow.progress);
        }

        return flow.values[name];
      }

      return _SliceTrigger.prototype.get.call(this, name);
    };

    _proto17.getCheckData = function getCheckData() {
      return _extends({}, _SliceTrigger.prototype.getCheckData.call(this), {
        progress: this.flow.progress,
        updateProgress: this.state.is('updateProgress'),
        updateTags: this.state.is('updateTags')
      });
    };

    _proto17.addCheckpoint = function addCheckpoint(value, type, update) {
      if (this.state.is('started')) {
        this.flow.checkpointsASC = false;

        _SliceTrigger.prototype.addCheckpoint.call(this, value, type, update);
      }

      return this;
    };

    _proto17.getCheckpoints = function getCheckpoints() {
      var checkpoints = [];

      if (this.state.is('started')) {
        var flow = this.flow;
        checkpoints = flow.checkpoints;

        if (!flow.checkpointsASC) {
          checkpoints.sort(function (item1, item2) {
            return item1 instanceof SliceProgressCheckpoint ? item2 instanceof SliceProgressCheckpoint && item1.isBefore(item2) ? -1 : 1 : item2 instanceof SliceProgressCheckpoint ? -1 : 0;
          });
          flow.checkpointsASC = true;
        }
      }

      return checkpoints;
    };

    _proto17.checkCheckpoint = function checkCheckpoint(checkpoint, checkData) {
      if (checkData instanceof SliceProgressCheckpoint) {
        if (checkpoint.isCrossedBy(checkData)) {
          SliceTrigger.run(this.actions.checkpoint, this, {
            checkpoint: checkpoint
          });
          this.removeCheckpoint(checkpoint);
          return true;
        } else if (checkpoint.isBefore(checkData)) {
          return true;
        }

        return false;
      } else if (!checkData.data.updateTags) {
        return true;
      }

      return _SliceTrigger.prototype.checkCheckpoint.call(this, checkpoint, checkData);
    };

    _proto17.finish = function finish() {
      if (this.state.is('started')) {
        this.flow.progress = 1;
        return _SliceTrigger.prototype.finish.call(this);
      }

      return false;
    };

    _proto17.cleanup = function cleanup() {
      if (_SliceTrigger.prototype.cleanup.call(this)) {
        this.easings = null;
        return true;
      }

      return false;
    };

    return SliceProgress;
  }(SliceTrigger);

  _defineProperty(SliceProgress, "defaults", {});

  _defineProperty(SliceProgress, "quickValuesDefaults", {});

  SliceTrigger.register('progress, percent', SliceProgress);
  Slice.Progress = SliceProgress;
  var delayDefaults = {
    delay: false,
    fps: 60,
    repeatDelay: false
  };
  var delayQuickValuesDefaults = {
    delayLeft: function delayLeft(trigger) {
      return trigger.state.is('waiting') ? Math.max(trigger.flow.delay - trigger.flow.delayTime, 0) : 0;
    },
    delayTime: function delayTime(trigger) {
      return trigger.state.is('waiting') ? Math.min(trigger.flow.delayTime, trigger.flow.delay) : trigger.state.is('playing') ? trigger.flow.delay : 0;
    },
    tickPass: function tickPass(trigger) {
      return trigger.state.is('playing') ? trigger.flow.tickPass : 0;
    }
  };

  var delayTick = function delayTick(trigger) {
    SliceDelay.removeTimeout(trigger);
    var flow = trigger.flow;
    var now = Date.now();
    var passed = now - flow.tickTime;
    flow.values = {};
    flow.tickPass = passed;
    flow.delayTime += passed;
    flow.passedTime += passed;
    flow.tickTime = now;

    if (flow.delayTime < flow.delay) {
      SliceTrigger.run(trigger.actions.wait, trigger);
      SliceDelay.addTimeout(trigger, function () {
        delayTick(trigger);
      });
    } else {
      trigger.state.exit('waiting').enter('playing');
      flow.delay = Math.min(flow.delayTime, flow.delay);
      flow.delayTime = flow.delay;
      SliceTrigger.run(trigger.actions.play, trigger);
      trigger.startPlay();
    }
  };

  var delayHoldTick = function delayHoldTick(trigger, action, fn) {
    SliceDelay.removeTimeout(trigger);
    var flow = trigger.flow;
    var now = Date.now();
    flow.values = {};
    flow.holdTime += now - flow.tickHoldTime;
    flow.tickHoldTime = now;

    if (flow.holdTime < flow.continueDelay) {
      if (action) {
        SliceTrigger.run(trigger.actions[action], trigger);
      }

      SliceDelay.addTimeout(trigger, function () {
        delayHoldTick(trigger, action, fn);
      });
    } else {
      fn();
    }
  };

  var delayBuildSettings = function delayBuildSettings(settings, delay) {
    settings.delay = SliceDelay.toDuration(delay);
  };

  var SliceDelay = /*#__PURE__*/function (_SliceProgress) {
    _inheritsLoose(SliceDelay, _SliceProgress);

    function SliceDelay() {
      return _SliceProgress.apply(this, arguments) || this;
    }

    SliceDelay.fromTime = function fromTime(time, isDate) {
      if (typeof time === 'number') {
        return Math.max(isDate || isDate !== false && time > 31556952000 ? time - Date.now() : time, 0);
      }

      return 0;
    };

    SliceDelay.modifyDuration = function modifyDuration(duration, value, substract) {
      var time = SliceDelay.toDuration(duration);

      switch (typeof value) {
        case 'object':
          if (value instanceof Date) {
            return SliceDelay.fromTime(value.getTime(), true);
          }

          if (moment.isDuration(value)) {
            return value.asMilliseconds();
          }

          if (moment.isMoment(value)) {
            return SliceDelay.fromTime(value.valueOf(), true);
          }

          time = Slice.getObjectValue(value, 'getTime');

          if (time === null) {
            time = Slice.getObjectValue(value, 'milliseconds');
          }

          break;

        case 'number':
          time = value;
          break;
      }

      return SliceDelay.toDuration(duration) + (substract ? -time : time);
    };

    SliceDelay.toDuration = function toDuration(value) {
      var time;

      switch (typeof value) {
        case 'object':
          if (value instanceof Date) {
            return SliceDelay.fromTime(value.getTime(), true);
          }

          if (moment.isDuration(value)) {
            return value.asMilliseconds();
          }

          if (moment.isMoment(value)) {
            return SliceDelay.fromTime(value.valueOf(), true);
          }

          time = Slice.getObjectValue(value, 'getTime');

          if (time === null) {
            time = Slice.getObjectValue(value, 'milliseconds');
          }

          break;

        case 'number':
          time = value;
          break;
      }

      return SliceDelay.fromTime(time);
    };

    SliceDelay.removeTimeout = function removeTimeout(trigger) {
      if (trigger.timeout) {
        clearTimeout(trigger.timeout);
        trigger.timeout = false;
      }
    };

    SliceDelay.addTimeout = function addTimeout(trigger, timeFunc) {
      if (trigger.timeout) {
        clearTimeout(trigger.timeout);
      }

      trigger.timeout = setTimeout(function () {
        trigger.timeout = false;
        timeFunc();
      }, trigger.fpsTime || 0);
    };

    var _proto18 = SliceDelay.prototype;

    _proto18.updateOptions = function updateOptions(options) {
      this.fpsTime = 1000 / options.fps;

      _SliceProgress.prototype.updateOptions.call(this, options);
    };

    _proto18.init = function init() {
      this.state.add('waiting', 'pending').add('playing', 'pending').add('paused', 'pending');

      _SliceProgress.prototype.init.call(this);
    };

    _proto18.getDefaults = function getDefaults() {
      return _extends({}, _SliceProgress.prototype.getDefaults.call(this), delayDefaults, SliceDelay.defaults);
    };

    _proto18.getDefaultQuickValues = function getDefaultQuickValues() {
      return _extends({}, _SliceProgress.prototype.getDefaultQuickValues.call(this), delayQuickValuesDefaults, SliceDelay.quickValuesDefaults);
    };

    _proto18.extendDelay = function extendDelay(value) {
      if (this.state.is('waiting')) {
        this.flow.delay = SliceDelay.modifyDuration(this.flow.duration, value);
        this.update();
      }

      return this;
    };

    _proto18.reduceDelay = function reduceDelay(value) {
      if (this.state.is('waiting')) {
        this.flow.delay = SliceDelay.modifyDuration(this.flow.duration, value, true);
        this.update();
      }

      return this;
    };

    _proto18.prepareReset = function prepareReset(preset) {
      SliceDelay.removeTimeout(this);
      this.iteration = 1;
      this.startTime = Date.now();
      delayBuildSettings(preset, this.options.delay);
    };

    _proto18.prepareRepeat = function prepareRepeat(preset) {
      SliceDelay.removeTimeout(this);
      delayBuildSettings(preset, this.options.repeatDelay === false ? this.options.delay : this.options.repeatDelay);
    };

    _proto18.adjustSettings = function adjustSettings(settings, value) {
      if (typeof value === 'object') {
        if (value.hasOwnProperty('delay')) {
          delayBuildSettings(settings, value.delay);
        }
      } else {
        delayBuildSettings(settings, value);
      }
    };

    _proto18.prepareStart = function prepareStart(flow, preset) {
      SliceDelay.removeTimeout(this);

      _SliceProgress.prototype.prepareStart.call(this, flow, preset);

      var now = Date.now();
      flow.iterationTime = now;
      flow.tickPass = 0;
      flow.passedTime = 0;
      flow.delayTime = 0;
      flow.tickTime = now;
      flow.progress = 0;
      flow.delayProgress = 0;
      flow.playTime = 0;
    };

    _proto18.prepareReady = function prepareReady() {
      SliceDelay.removeTimeout(this);
    };

    _proto18.update = function update() {
      var state = this.state;

      if (state.is('started')) {
        if (state.is('paused')) {
          _SliceProgress.prototype.update.call(this);
        } else if (state.is('playing')) {
          this.startPlay();
        } else {
          if (state.not('waiting')) {
            state.enter('waiting');
          }

          delayTick(this);
        }
      }

      return this;
    };

    _proto18.preparePlay = function preparePlay(flow) {
      flow.playTime = 0;
      flow.delay = Math.min(flow.delayTime, flow.delay);
      flow.delayTime = flow.delay;
    };

    _proto18.startPlay = function startPlay() {
      SliceDelay.removeTimeout(this);
    };

    _proto18.play = function play() {
      if (this.state.is('started') && this.state.not('paused')) {
        this.state.exit('waiting').enter('playing');
        this.flow.tickTime = Date.now();
        this.preparePlay(this.flow);
        SliceTrigger.run(this.actions.play, this);
        this.startPlay();
      }

      return this;
    };

    _proto18.pause = function pause() {
      SliceDelay.removeTimeout(this);

      if (this.state.is('started') && this.state.not('paused')) {
        this.flow.pauseTime = Date.now();
        this.state.enter('paused');
        SliceTrigger.run(this.actions.pause, this);
      }

      return this;
    };

    _proto18.continue = function _continue(delay) {
      var _this24 = this;

      if (this.state.is('paused')) {
        var delayVal = SliceDelay.toDuration(delay);
        var flow = this.flow;
        flow.continueDelay = delayVal;

        if (delay) {
          flow.tickHoldTime = Date.now();
          flow.holdTime = 0;
          SliceTrigger.run(this.actions.continueDelay, this);
          delayHoldTick(this, 'holdContinue', function () {
            _this24.continue(0);
          });
        } else {
          SliceTrigger.run(this.actions.continue, this);
          this.flow.tickTime = Date.now();
          this.state.exit('paused');
          this.update();
        }
      }

      return this;
    };

    _proto18.finish = function finish() {
      if (this.state.is('started')) {
        SliceDelay.removeTimeout(this);
        this.flow.delayTime = this.flow.delay;
        return _SliceProgress.prototype.finish.call(this);
      }

      return false;
    };

    _proto18.cleanup = function cleanup() {
      if (_SliceProgress.prototype.cleanup.call(this)) {
        SliceDelay.removeTimeout(this);
        return true;
      }

      return false;
    };

    return SliceDelay;
  }(SliceProgress);

  _defineProperty(SliceDelay, "defaults", {});

  _defineProperty(SliceDelay, "quickValuesDefaults", {});

  Slice.Delay = SliceDelay;
  var distanceDefaults = {
    distance: false,
    repeatDistance: false,
    repeatSpeed: false,
    speed: false
  };
  var distanceQuickValuesDefaults = {
    distanceLeft: function distanceLeft(trigger) {
      return trigger.state.is('playing') ? Math.max(trigger.flow.distance - trigger.flow.distancePass, 0) : trigger.state.is('waiting') ? trigger.flow.distance : 0;
    },
    distancePass: function distancePass(trigger) {
      return trigger.state.is('playing') ? Math.min(trigger.flow.distancePass, trigger.flow.distance) : 0;
    },
    playLeft: function playLeft(trigger) {
      return trigger.state.is('playing') ? Math.max(trigger.flow.distanceTime - trigger.flow.playTime, 0) : trigger.state.is('waiting') ? trigger.flow.distanceTime : 0;
    },
    playTime: function playTime(trigger) {
      return trigger.state.is('playing') ? Math.min(trigger.flow.playTime, trigger.flow.distanceTime) : 0;
    }
  };

  var distanceBuildSettings = function distanceBuildSettings(settings, distance, speed) {
    settings.distance = distance;

    if (typeof speed !== 'undefined') {
      settings.speed = Math.max(speed || 0, 0);
    }

    settings.distanceTime = settings.speed ? settings.distance / settings.speed : Infinity;
  };

  var distanceTick = function distanceTick(trigger) {
    SliceDelay.removeTimeout(trigger);
    var flow = trigger.flow;
    var now = Date.now();
    var passed = now - flow.tickTime;
    flow.tickPass = passed;
    flow.passedTime += passed;
    flow.playTime += passed;
    flow.tickDistance = passed * flow.speed;
    flow.distancePass += flow.tickDistance;
    flow.tickTime = now;
    SliceDelay.addTimeout(trigger, function () {
      distanceTick(trigger);
    });
    trigger.setProgress(flow.distancePass / flow.distance);
  };

  var SliceDistance = /*#__PURE__*/function (_SliceDelay) {
    _inheritsLoose(SliceDistance, _SliceDelay);

    function SliceDistance() {
      var _this25;

      for (var _len13 = arguments.length, args = new Array(_len13), _key13 = 0; _key13 < _len13; _key13++) {
        args[_key13] = arguments[_key13];
      }

      _this25 = _SliceDelay.call.apply(_SliceDelay, [this].concat(args)) || this;

      _defineProperty(_assertThisInitialized(_this25), "checkpointType", 'distance');

      return _this25;
    }

    var _proto19 = SliceDistance.prototype;

    _proto19.getDefaults = function getDefaults() {
      return _extends({}, _SliceDelay.prototype.getDefaults.call(this), distanceDefaults, SliceDistance.defaults);
    };

    _proto19.getDefaultQuickValues = function getDefaultQuickValues() {
      return _extends({}, _SliceDelay.prototype.getDefaultQuickValues.call(this), distanceQuickValuesDefaults, SliceDistance.quickValuesDefaults);
    };

    _proto19.extendDistance = function extendDistance(value) {
      if (this.state.is('started')) {
        this.flow.distance += value;
        this.update();
      }

      return this;
    };

    _proto19.reduceDuration = function reduceDuration(value) {
      if (this.state.is('started')) {
        this.flow.duration -= value;
        this.update();
      }

      return this;
    };

    _proto19.prepareReset = function prepareReset(preset) {
      _SliceDelay.prototype.prepareReset.call(this, preset);

      distanceBuildSettings(preset, this.options.distance, this.options.speed);
    };

    _proto19.prepareRepeat = function prepareRepeat(preset) {
      _SliceDelay.prototype.prepareRepeat.call(this, preset);

      distanceBuildSettings(preset, this.options.repeatDistance === false ? this.options.distance : this.options.repeatDistance, this.options.repeatSpeed === false ? this.options.speed : this.options.repeatSpeed);
    };

    _proto19.adjustSettings = function adjustSettings(settings, value) {
      if (typeof value === 'object') {
        _SliceDelay.prototype.adjustSettings.call(this, settings, value);

        distanceBuildSettings(settings, value.hasOwnProperty('distance') ? value.distance : settings.distance, value.hasOwnProperty('speed') ? value.speed : settings.speed);
      } else {
        distanceBuildSettings(settings, value);
      }
    };

    _proto19.prepareStart = function prepareStart(flow, preset) {
      _SliceDelay.prototype.prepareStart.call(this, flow, preset);

      flow.distancePass = 0;
    };

    _proto19.prepareReady = function prepareReady(flow) {
      _SliceDelay.prototype.prepareReady.call(this, flow);

      flow.distancePass = flow.distance;
      flow.playTime = flow.distanceTime;
    };

    _proto19.setPassDistance = function setPassDistance(value) {
      var state = this.state;

      if (state.is('started')) {
        var flow = this.flow;
        var distance = Math.max(value, 0);
        flow.distancePass = Math.min(distance, flow.distance);

        if (state.is('playing') && state.not('paused')) {
          var now = Date.now();
          var passed = now - flow.tickTime;
          flow.tickPass = passed;
          flow.passedTime += passed;
          flow.playTime += passed;
          flow.tickTime = now;
          flow.tickDistance = distance - flow.distancePass;
        }

        flow.distanceTime = flow.speed ? flow.playTime + (flow.distance - flow.distancePass) / flow.speed : Infinity;
        this.setProgress(flow.distancePass / flow.distance);
      }

      return this;
    };

    _proto19.preparePlay = function preparePlay(flow) {
      _SliceDelay.prototype.preparePlay.call(this, flow);

      flow.distancePass = 0;
    };

    _proto19.startPlay = function startPlay() {
      if (this.flow.distance) {
        if (this.flow.speed) {
          distanceTick(this);
        }
      } else {
        _SliceDelay.prototype.setProgress.call(this, 1);
      }
    };

    _proto19.getCheckData = function getCheckData() {
      var flow = this.flow;
      return _extends({}, _SliceDelay.prototype.getCheckData.call(this), {
        distance: flow.distancePass,
        distanceLimit: flow.distance,
        timeLimit: flow.distanceTime,
        timePass: flow.playTime
      });
    };

    _proto19.finish = function finish() {
      if (this.state.is('started')) {
        var flow = this.flow;
        flow.passedTime = flow.delay + flow.distanceTime;
        flow.distancePass = flow.distance;
        flow.playTime = flow.distanceTime;
        return _SliceDelay.prototype.finish.call(this);
      }

      return false;
    };

    return SliceDistance;
  }(SliceDelay);

  _defineProperty(SliceDistance, "defaults", {});

  _defineProperty(SliceDistance, "quickValuesDefaults", {});

  SliceTrigger.register('distance, speed, distancePass, distancepass', SliceDistance);
  Slice.Distance = SliceDistance;
  var timerDefaults = {
    duration: false,
    repeatDuration: false
  };
  var timerQuickValuesDefaults = {
    playLeft: function playLeft(trigger) {
      return trigger.state.is('playing') ? Math.max(trigger.flow.duration - trigger.flow.playTime, 0) : trigger.state.is('waiting') ? trigger.flow.duration : 0;
    },
    playTime: function playTime(trigger) {
      return trigger.state.is('playing') ? Math.min(trigger.flow.playTime, trigger.flow.duration) : 0;
    }
  };

  var timerBuildSettings = function timerBuildSettings(settings, duration) {
    settings.duration = SliceDelay.toDuration(duration);
  };

  var timerTick = function timerTick(trigger) {
    SliceDelay.removeTimeout(trigger);
    var flow = trigger.flow;
    var now = Date.now();
    var passed = now - flow.tickTime;
    flow.tickPass = passed;
    flow.passedTime += passed;
    flow.playTime += passed;
    flow.tickTime = now;
    SliceDelay.addTimeout(trigger, function () {
      timerTick(trigger);
    });
    trigger.setProgress(flow.playTime / flow.duration);
  };

  var SliceTimer = /*#__PURE__*/function (_SliceDelay2) {
    _inheritsLoose(SliceTimer, _SliceDelay2);

    function SliceTimer() {
      var _this26;

      for (var _len14 = arguments.length, args = new Array(_len14), _key14 = 0; _key14 < _len14; _key14++) {
        args[_key14] = arguments[_key14];
      }

      _this26 = _SliceDelay2.call.apply(_SliceDelay2, [this].concat(args)) || this;

      _defineProperty(_assertThisInitialized(_this26), "checkpointType", 'timePass');

      return _this26;
    }

    var _proto20 = SliceTimer.prototype;

    _proto20.getDefaults = function getDefaults() {
      return _extends({}, _SliceDelay2.prototype.getDefaults.call(this), timerDefaults, SliceTimer.defaults);
    };

    _proto20.getDefaultQuickValues = function getDefaultQuickValues() {
      return _extends({}, _SliceDelay2.prototype.getDefaultQuickValues.call(this), timerQuickValuesDefaults, SliceTimer.quickValuesDefaults);
    };

    _proto20.extendDuration = function extendDuration(value) {
      if (this.state.is('started')) {
        this.flow.duration = SliceDelay.modifyDuration(this.flow.duration, value);
        this.update();
      }

      return this;
    };

    _proto20.reduceDuration = function reduceDuration(value) {
      if (this.state.is('started')) {
        this.flow.duration = SliceDelay.modifyDuration(this.flow.duration, value, true);
        this.update();
      }

      return this;
    };

    _proto20.prepareReset = function prepareReset(preset) {
      _SliceDelay2.prototype.prepareReset.call(this, preset);

      timerBuildSettings(preset, this.options.duration);
    };

    _proto20.prepareRepeat = function prepareRepeat(preset) {
      _SliceDelay2.prototype.prepareRepeat.call(this, preset);

      timerBuildSettings(preset, this.options.repeatDuration === false ? this.options.duration : this.options.repeatDuration);
    };

    _proto20.adjustSettings = function adjustSettings(settings, value) {
      if (typeof value === 'object') {
        _SliceDelay2.prototype.adjustSettings.call(this, settings, value);

        if (value.hasOwnProperty('duration')) {
          timerBuildSettings(settings, value.duration);
        }
      } else {
        timerBuildSettings(settings, value);
      }
    };

    _proto20.prepareReady = function prepareReady(flow) {
      _SliceDelay2.prototype.prepareReady.call(this, flow);

      flow.playTime = flow.duration;
    };

    _proto20.startPlay = function startPlay() {
      if (this.flow.duration) {
        timerTick(this);
      } else {
        _SliceDelay2.prototype.setProgress.call(this, 1);
      }

      return this;
    };

    _proto20.getCheckData = function getCheckData() {
      return _extends({}, _SliceDelay2.prototype.getCheckData.call(this), {
        timeLimit: this.flow.duration,
        timePass: this.flow.playTime
      });
    };

    _proto20.finish = function finish() {
      if (this.state.is('started')) {
        var flow = this.flow;
        flow.passedTime = flow.delay + flow.duration;
        flow.playTime = flow.duration;
        return _SliceDelay2.prototype.finish.call(this);
      }

      return false;
    };

    return SliceTimer;
  }(SliceDelay);

  _defineProperty(SliceTimer, "defaults", {});

  _defineProperty(SliceTimer, "quickValuesDefaults", {});

  SliceTrigger.register('duration, delay, timer, timepass, timePass', SliceTimer);
  Slice.Timer = SliceTimer;
  window.Slice = Slice;
});
//# sourceMappingURL=slice-core.js.map
