/* global jQuery, Slice */

/**
 * Slice Slider: Autosize Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Enable/disable automatic slide size calculation
        autosize: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        setRunnableType = factory.setRunnableType,
        addRunnable = factory.addRunnable;

    /**
     * Requirement for all workers and runnables.
     */
    var requirements = 'autosize';

    /**
     * Autosize plugin constructor.
     *
     * @class SliceAutosize
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceAutosize (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceAutosize.prototype.setup = function () {
        var settings = this.slice.settings;
        // Check if carousel plugin is loaded and enable it with autosize
        if (settings.autosize && fnSlice.Plugins.SliceCarousel) {
            settings.workers.autosize = true;
            settings.workers.fixedsize = false;
            settings.workers.fixedslides = false;
            settings.workers.single = false;
            settings.workers.carousel = true;
        }
    };

    /**
     * Get number of slides that fit in size.
     *
     * @param {number} size - Size to fit.
     * @param {Array} sizes - Array of slides sizes.
     * @param {number} max - Max slides.
     * @param {number} spacing - Slides spacing.
     * @param {boolean} reverse - Do reverse check.
     * @returns {number} Number of slides.
     */
    function getFitCount (size, sizes, max, spacing, reverse) {
        var i = reverse
                ? max
                : 1,
            start = i,
            end = reverse
                ? 1
                : max,
            step = reverse
                ? -1
                : 1,
            count = 0,
            expanded = 0,
            totalSize = size + spacing;
        totalSize = Math.max(totalSize, 0);
        while (expanded < totalSize) {
            expanded += sizes[i] + spacing;
            ++count;
            if (i === end) {
                if (expanded === 0) {
                    return sizes.length;
                }
                i = start;
            } else {
                i += step;
            }
        }
        return count;
    }

    /**
     * Add workers and runnables for autosize mode only.
     */
    factory.addRequirement(requirements);

    /**
     * Check for re-update slider going throught edges.
     */
    addWorker('size', function (stage, cache) {
        var context = this;
        if (cache.list.all) {
            return;
        }
        if (!cache.list.edges) {
            context.invalidate('edges');
            cache.reUpdate = true;
        }
    }, -100, '!fixedslides');

    /**
     * Set slides to show and step.
     */
    addWorker('settings', function (stage) {
        stage.slidesToShow = stage.slidesToShow || 1;
        stage.slidesStep = stage.slidesStep || stage.slidesToShow;
    }, 1, 'carousel');

    /**
     * Calculate slides sizes.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.slidesSizes = {};
        var $slides = cache.$slides || stage.$originalSlides || stage.$slides,
            workers = this.settings.workers,
            canResize = workers.horizontal || workers.fixedheight || !workers.adaptiveHeight,
            $slide, slideSize;
        for (var i = 0; i < $slides.length; i++) {
            // Don't allow slide size exceed work space size
            $slide = $slides.eq(i).data('sliceSizeIndex', i + 1);
            slideSize = stage.getSize($slide);
            if (canResize && slideSize > stage.workSpace) {
                slideSize = stage.workSpace;
            }
            stage.setSize($slide, slideSize);
            cache.slidesSizes[i + 1] = slideSize;
        }
    }, 30);

    /**
     * Calculate minimal slides length.
     */
    addWorker('edges, slides, settings', function (stage, cache) {
        var size = cache.minViewSize || stage.viewSize,
            slidesCount = (cache.$slides || stage.$originalSlides || stage.$slides).length;
        stage.minSlides = getFitCount(size, cache.slidesSizes, slidesCount, stage.slideSpacing);
    }, 30, 'carousel, !fixedslides');

    /**
     * Calculate clones count.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        var clonesCountPrev = stage.minSlides,
            clonesCountNext = 1 + getFitCount(
                stage.viewSize * 2,
                cache.slidesSizes,
                stage.$originalSlides.length,
                stage.slideSpacing,
                true
            );
        stage.clonesCount = clonesCountPrev > clonesCountNext
            ? clonesCountPrev
            : clonesCountNext;
    }, 40, 'loop');

    /**
     * Calculate clones count.
     */
    addWorker('size, slides, settings', function (stage) {
        var size = 0;
        stage.$slides.each(function (i, el) {
            size += stage.getSize($(el));
        });
        stage.size = size;
    }, 50);

    /**
     * Calculate slides coordinates.
     */
    addWorker('size, slides, settings', function (stage) {
        var size = stage.$slides.length,
            iterator = 0,
            base = {},
            coordinates = {};
        base[1] = 0;
        coordinates[1] = 0 - stage.displaceFirst;
        while (++iterator < size) {
            base[iterator + 1] = base[iterator] -
                (stage.getSize(stage.$slides.eq(iterator - 1)) + stage.displacePerSlide);
            coordinates[iterator + 1] = base[iterator + 1] - stage.displaceFirst;
        }
        stage.baseCoordinates = base;
        stage.coordinates = coordinates;
    }, 85);

    /**
     * Calculate start and last slides.
     */
    addWorker('edges, slides, settings', function (stage) {
        if (!stage.enoughSlides) {
            stage.lastSlide = stage.startSlide;
            return;
        }
        var lastCoordinates = stage.viewSize - stage.size,
            lastSlide = 1;
        while (stage.coordinates[lastSlide + 1] > lastCoordinates) {
            ++lastSlide;
        }
        if (stage.coordinates[lastSlide] !== lastCoordinates && stage.slidesCount > lastSlide) {
            ++lastSlide;
        }
        stage.coordinates[lastSlide] = lastCoordinates;
        stage.lastSlide = lastSlide;
    }, 110, 'carousel, !fixedslides, !loop, showFit');

    /**
     * Get size runnables.
     */
    setRunnableType('size');

    /**
     * Calculate slider view height.
     */
    addRunnable('viewHeight', function (stage, cache) {
        cache.slideSizes.sort(function (a, b) {
            return b - a;
        });
        cache.viewSize = 0;
        var count = stage.slidesToShow;
        for (var i = 0; i < cache.slideSizes.length && i < count; i++) {
            cache.viewSize += cache.slideSizes[i];
        }
    }, 20, 'carousel, vertical');

    /**
     * Calculate active slides height.
     */
    addRunnable('activeHeight', function (stage, cache) {
        cache.viewSize = 0;
        cache.$slides.each(function (i, el) {
            cache.viewSize += $(el)[fnSlice.cssProps.height]();
        });
    }, 20, 'carousel, vertical');

    /**
     * Calculate in view, visible slides height.
     */
    addRunnable('inViewHeight', function (stage, cache) {
        cache.viewSize = cache.slideSizes.reduce(function (accumulator, currentValue) {
            return accumulator + currentValue;
        }, 0);
    }, 20, 'carousel, vertical');

    /**
     * Set slide position runnables.
     */
    setRunnableType('setPosition');

    /**
     * Calculate the end position of active slides.
     */
    addRunnable('current, extraCurrent', function (stage, cache) {
        var endPos = cache.start,
            endCoords = stage.coordinates[cache.start] - stage.viewSize;
        while (stage.$slides.length > endPos && stage.coordinates[endPos + 1] > endCoords) {
            ++endPos;
        }
        cache.end = endPos;
    }, 20, 'carousel, !displace, !fixedslides');

    /**
     * Get slide position by coordinates runnables.
     */
    setRunnableType('closest');

    /**
     * Set precise function.
     */
    addRunnable('prev+preciseSlide', function (stage, cache) {
        var precise = cache.precise || 0,
            offset = cache.prevSlideOffset || 1,
            spacing = stage.slideSpacing;
        cache.getPrecise = function (position) {
            return spacing + stage.getSize(stage.$slides.eq(position - offset)) * precise;
        };
    }, 10);
    addRunnable('next+preciseSlide', function (stage, cache) {
        var precise = cache.precise || 0,
            offset = cache.nextSlideOffset || 1,
            spacing = stage.slideSpacing;
        cache.getPrecise = function (position) {
            return spacing +
                Math.min(stage.getSize(stage.$slides.eq(position + offset - 1)), stage.viewportSize) * precise;
        };
    }, 10);

    /**
     * Calculate speed runnables.
     */
    setRunnableType('speed');

    /**
     * Calculate speed index.
     */
    addRunnable('slide, slidefixed', function (stage, cache) {
        var coords, i, prop, prevSize, realLast,
            len = stage.$slides.length,
            list = {
                end: 'to',
                start: 'from'
            };
        for (prop in list) {
            if (list.hasOwnProperty(prop) && (!Slice.hasProperty(cache, prop) || cache[prop] === stage.lastSlide)) {
                coords = cache[list[prop]];
                for (i = 1; i <= len && stage.coordinates[i] > coords; i++) {
                    // All stuff is done at for condition
                }
                cache[prop] = i;
                if (i === stage.lastSlide) {
                    if (stage.lastSlide > 1) {
                        prevSize = stage.getSize(stage.$slides.eq(i - 2)) + stage.slideSpacing;
                        realLast = stage.coordinates[i - 1] - prevSize;
                        if (realLast > coords) {
                            cache[prop] += (realLast - coords) / stage.getSize(stage.$slides.eq(i - 1));
                        } else {
                            cache[prop] -= 1 - (stage.coordinates[i - 1] - coords) / prevSize;
                        }
                    }
                } else if (stage.coordinates[i] !== coords) {
                    cache[prop] += (stage.coordinates[i] - coords) /
                        (stage.getSize(stage.$slides.eq(i - 1)) + cache.spacing);
                }
            }
        }
        cache.index = Math.abs(cache.end - cache.start);
    }, 20);

    /**
     * Check slide visibility.
     */
    setRunnableType('slideVisibility');

    /**
     * Set slide size.
     */
    addRunnable(true, function (stage, cache) {
        cache.slideSize = stage.getSize(cache.$slide);
    }, 30);

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    factory.removeRequirement(requirements);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceAutosize = SliceAutosize;

    /**
     * Add getFitCount function.
     */
    fnSlice.fn.getFitCount = getFitCount;
}));
