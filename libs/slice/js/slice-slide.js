/* global jQuery, Slice */

/**
 * Slice Slider: Slide Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 * @typedef {object} SlicePostponed Slice postponed class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Class for current resized element
        currentResizedClass: 'slice-current-resized',
        // Current slide align of non sticked slides, false - same as 'slideAlign'
        currentSlideAlign: false,
        // Current slide size responding to other slides, allowed units %, px
        currentSlideSize: false,
        // Predict slide size, for smooth animation
        predictSlideSize: true,
        // Slide align of non sticked slides
        slideAlign: 'center',
        // Current slide align in case of it's smaller than other slides
        slideEasing: 'swing',
        // Slides aspect ration
        slideRatio: false,
        // Don't allow extra space between slides
        stickSlides: true
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable,
        setRunnableType = factory.setRunnableType,
        cssProps = fnSlice.cssProps,
        addRequirement = factory.addRequirement,
        removeRequirement = factory.removeRequirement;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'fixedsize';

    /**
     * Requirements for some cases.
     */
    var tempRequirement,
        subRequirement;

    /**
     * Count height by ratio.
     *
     * @param {number} val - Width.
     * @param {number} ratio - Ratio.
     * @returns {number} Height.
     */
    function countHeightRatio (val, ratio) {
        return val * ratio;
    }

    /**
     * Count width by ratio.
     *
     * @param {number} val - Height.
     * @param {number} ratio - Ratio.
     * @returns {number} Width.
     */
    function countWidthRatio (val, ratio) {
        return val / ratio;
    }

    /**
     * Get slide styles.
     *
     * @param {jQuery} $slide - Slide element.
     * @param {object} stage - Slider stage.
     * @param {object} settings - Slider settings.
     * @param {boolean} isCurrent - Slide is current.
     * @param {boolean} forceReverse - Forse to calcualte reverse size.
     * @returns {object} Slide styles.
     */
    function getSlideStyles ($slide, stage, settings, isCurrent, forceReverse) {
        var name = 'slice_slideStyles' + (isCurrent
                ? '_current'
                : ''),
            css = $slide.data(name);
        if (css) {
            return css;
        }
        css = {};
        $slide.data(name, css);
        var size;
        if (isCurrent) {
            size = stage.currentSlideSize;
            if (stage.currentSlideSpacingStyle) {
                $.extend(css, stage.currentSlideSpacingStyle);
            }
        } else {
            size = stage.slideSize;
            if (stage.slideSpacingStyle) {
                $.extend(css, stage.slideSpacingStyle);
            }
        }
        css[stage.sizeProp] = size;
        if (stage.slideRatio) {
            css[stage.revSizeProp] = stage.ratioFn(size, stage.slideRatio);
        } else if (forceReverse || settings.predictSlideSize) {
            var storeSize = $slide.css(stage.sizeProp),
                storedRev = $slide.css(stage.revSizeProp);
            $slide
                .css(stage.sizeProp, size)
                .css(stage.revSizeProp, '');
            Slice.redraw($slide[0]);
            css[stage.revSizeProp] = parseFloat($slide[stage.revSizeProp]());
            $slide
                .css(stage.sizeProp, storeSize)
                .css(stage.revSizeProp, storedRev);
        }
        return css;
    }

    /**
     * Animate slide.
     *
     * @param {jQuery} $slide - Slide element.
     * @param {object} stage - Slider stage.
     * @param {object} settings - Slider settings.
     * @param {boolean} isCurrent - Slide is current.
     * @param {number} speed - Animation speed.
     * @param {number} delay - Animation delay.
     * @param {boolean} isPositioned - Slide is positioned instead of moving.
     * @returns {SlicePostponed} Postponed animation object.
     */
    function animateSlide ($slide, stage, settings, isCurrent, speed, delay, isPositioned) {
        var builtSpeed = speed
                ? speed
                : 0,
            animateProps = getSlideStyles($slide, stage, settings, isCurrent),
            postponed = new Slice.Postponed();
        postponed.fail(function () {
            $slide
                .stop('fx', true)
                .css(animateProps);
        });
        postponed
            .add(function () {
                if (!isPositioned) {
                    $slide.stop('fx', true);
                }
                if (delay) {
                    $slide.delay(delay);
                }
                $slide.animate(animateProps, builtSpeed, settings.slideEasing, function () {
                    postponed.resolve();
                });
                return postponed;
            });
        return postponed;
    }

    /**
     * Count styles by progress.
     *
     * @param {object} styles - Start styles.
     * @param {object} toStyles - Final styles.
     * @param {number} progress - Progress percent 0 -> 1.
     * @returns {object} Result styles.
     */
    function getStyleProgress (styles, toStyles, progress) {
        var progressStyles = $.extend({}, styles),
            value, toValue;
        for (var prop in toStyles) {
            if (progressStyles.hasOwnProperty(prop)) {
                value = progressStyles[prop];
                toValue = toStyles[prop];
                if (value !== toValue && typeof value === 'number' && typeof toValue === 'number') {
                    progressStyles[prop] = value + (toValue - value) * progress;
                } else {
                    progressStyles[prop] = toValue;
                }
            }
        }
        return progressStyles;
    }

    /**
     * Set slide styles.
     *
     * @param {object} stage - Stage object.
     * @param {object} settings - Settings object.
     * @param {jQuery} $slide - Slide element.
     * @param {number} progress - Progress percent 0 -> 1.
     */
    function setSlideStyles (stage, settings, $slide, progress) {
        var slideProgress = isNaN(progress)
                ? 0
                : Math.min(1, Math.max(0, progress)),
            currentStyles = getSlideStyles($slide, stage, settings, true),
            genericStyles = getSlideStyles($slide, stage, settings);
        if (slideProgress === 0) {
            $slide.css(genericStyles);
        } else if (slideProgress === 1) {
            $slide.css(currentStyles);
        } else {
            $slide.css(getStyleProgress(genericStyles, currentStyles, slideProgress));
        }
    }

    /**
     * Slide plugin constructor.
     *
     * @class SliceSlide
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceSlide (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceSlide.prototype.setup = function () {
        var settings = this.slice.settings,
            stage = this.slice.stage,
            ratioType = typeof settings.slideRatio,
            ratio;
        settings.workers.currentSlideSize = !!settings.currentSlideSize;
        settings.workers.stickSlides = !!settings.stickSlides;
        settings.workers.predictSlideSize = !!settings.predictSlideSize;
        switch (ratioType) {
        case 'number':
            ratio = settings.slideRatio;
            break;
        case 'string':
            ratio = settings.slideRatio.split('by');
            if (ratio.length === 2) {
                ratio = parseInt(ratio[1]) / parseInt(ratio[0]);
            }
            break;
        }
        if (ratio && !isNaN(ratio)) {
            stage.slideRatio = ratio;
            stage.ratioFn = countHeightRatio;
            settings.workers.slideRatio = true;
        }
    };

    /**
     * Add workers and runnables for fixed slide size mode only.
     */
    addRequirement(requirement);

    /**
     * Add workers for slide ratio only.
     */
    tempRequirement = 'slideRatio';
    addRequirement(tempRequirement);

    /**
     * Set 'vertical' ratio function.
     */
    addWorker('settings', function (stage) {
        stage.ratioFn = countWidthRatio;
    }, 0, 'vertical');

    /**
     * Apply slide ratio.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.$slides.css(stage.revSizeProp, stage.ratioFn(stage.slideSize, stage.slideRatio));
    }, 70);
    removeRequirement(tempRequirement);

    /**
     * Add workers for current slide size mode only.
     */
    tempRequirement = 'currentSlideSize';
    addRequirement(tempRequirement);

    /**
     * Calculate current slide units and set spacings.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.currentSlideUnits = Slice.Unit.toUnitType(this.settings.currentSlideSize);
        stage.currentSlideSpacing = false;
        stage.extraSlideSpacing = false;
        stage.currentSlideSpacingStyle = {};
        stage.slideSpacingStyle = {};
    }, 1);

    /**
     * Add workers for non-sticked slides mode only.
     */
    subRequirement = '!stickSlides';
    addRequirement(subRequirement);

    /**
     * Calculate default and current slide sizes and spacings.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        switch (stage.currentSlideUnits.type) {
        case '%':
            var scale = stage.currentSlideUnits.scale;
            if (scale < 1) {
                stage.currentSlideSize = cache.slideSize * scale;
                stage.currentSlideSpacing = cache.slideSize - stage.currentSlideSize;
            } else {
                stage.currentSlideSize = cache.slideSize;
                cache.slideSize /= scale;
                stage.extraSlideSpacing = Math.max(stage.currentSlideSize - cache.slideSize, 0);
            }
            break;
        case 'px':
            stage.currentSlideSize = stage.currentSlideUnits.value > stage.workSpace
                ? stage.workSpace
                : stage.currentSlideUnits.value;
            stage.currentSlideSpacing = Math.max(cache.slideSize - stage.currentSlideSize, 0);
            if (stage.slidesToShow && stage.currentSlideSize > cache.slideSize) {
                cache.slideSize = (stage.workSpace - stage.currentSlideSize) / (stage.slidesToShow - 1);
            }
            break;
        }
    }, 31);

    /**
     * Create slide spacing styles.
     *
     * @param {object} stage - Slider stage.
     * @param {number} [value] - Slide spacing value.
     * @param {string} [align] - Slide align.
     * @returns {object} Spacing styles.
     */
    function createSlideSpacings (stage, value, align) {
        var props = {},
            prev = stage.slideSpacing,
            spacing = value || 0,
            next = 0;
        // Set current slide spacings.
        switch (align) {
        case 'prev':
            next += spacing;
            break;
        case 'next':
            prev += spacing;
            break;
        default:
            next += spacing / 2;
            prev += spacing / 2;
            break;
        }
        props[stage.spacingProps[0]] = prev;
        props[stage.spacingProps[1]] = next;
        return props;
    }

    /**
     * Distribute spacings to corresponding alignments.
     * Adjust displacement per slide, used to calculate slides coordinates.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.currentSlideSpacingStyle = createSlideSpacings(
            stage, stage.currentSlideSpacing,
            this.settings.currentSlideAlign || this.settings.slideAlign
        );
        stage.slideSpacingStyle = createSlideSpacings(
            stage, stage.extraSlideSpacing,
            this.settings.slideSpacing
        );
        if (stage.extraSlideSpacing) {
            stage.size += stage.extraSlideSpacing * (stage.$slides.length - 1);
            stage.displacePerSlide += stage.extraSlideSpacing;
        }
        stage.$slides.css(stage.slideSpacingStyle);
    }, 61);
    removeRequirement(subRequirement);

    /**
     * Add workers for sticked slides mode only.
     * Calculate default and current slide sizes.
     */
    subRequirement = 'stickSlides';
    addRequirement(subRequirement);
    addWorker('size, slides, settings', function (stage, cache) {
        switch (stage.currentSlideUnits.type) {
        case '%':
            var scale = stage.currentSlideUnits.scale;
            if (scale < 1) {
                stage.currentSlideSize = cache.slideSize * scale;
            } else {
                stage.currentSlideSize = cache.slideSize;
                cache.slideSize /= scale;
            }
            break;
        case 'px':
            stage.currentSlideSize = stage.currentSlideUnits.value > stage.workSpace
                ? stage.workSpace
                : stage.currentSlideUnits.value;
            break;
        }
    }, 31, 'single');
    addWorker('size, slides, settings', function (stage, cache) {
        switch (stage.currentSlideUnits.type) {
        case '%':
            var scale = stage.currentSlideUnits.scale;
            cache.slideSize = stage.workSpace / (stage.slidesToShow + scale - 1);
            stage.currentSlideSize = scale * cache.slideSize;
            break;
        case 'px':
            stage.currentSlideSize = stage.currentSlideUnits.value;
            cache.slideSize = (stage.workSpace - stage.currentSlideSize) / (stage.slidesToShow - 1);
            break;
        }
    }, 31, 'carousel');

    /**
     * Adjust stage displacement for slider.
     */
    addWorker('size, slides, settings', function (stage) {
        if (stage.currentExtraSize > 0) {
            stage.displaceFirst -= stage.currentExtraSize / 2;
        }
    }, 65, 'single');

    /**
     * Set current displacement.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.currentDisplacement += stage.currentExtraSize;
    }, 65);
    removeRequirement(subRequirement);

    /**
     * Calculate current slide extra size and displacement.
     */
    addWorker('size, slides, settings', function (stage) {
        // Reset slides sizes
        stage.$slides
            .data('slice_slideStyles', null)
            .data('slice_slideStyles_current', null);
        stage.currentExtraSize = stage.currentSlideSize +
            (stage.currentSlideSpacing || 0) - (stage.slideSize + (stage.extraSpacing || 0));
        if (stage.currentExtraSize > 0) {
            stage.size += stage.currentExtraSize;
        }
    }, 62);

    /**
     * Adjust stage displacement for slider and center view mode.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.displacement += stage.currentExtraSize / 2;
    }, 65, ['centerView', 'stickSlides, single']);

    /**
     * Adjust current shift.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.currentShift += stage.currentExtraSize;
    }, 65, '!centerView, !single');

    /**
     * Animate previously resized slides to the default sizes.
     */
    addWorker('reset, move', function (stage, cache) {
        var context = this,
            currentResizedClass = context.settings.currentResizedClass,
            $slides = stage.$slides.filter('.' + currentResizedClass).removeClass(currentResizedClass);
        cache.slideSizeSpeed = stage.positioAnimation && !context.moved
            ? 0
            : cache.baseSpeed;
        $slides.each(function (i, slide) {
            var $slide = $(slide);
            if (cache.currentPosition === stage.$slides.index($slide) + 1) {
                return;
            }
            cache.animation.add(animateSlide(
                $slide, stage, context.settings,
                false,
                cache.slideSizeSpeed,
                0,
                stage.positioAnimation && !context.moved
            ));
        });
    }, 350);

    /**
     * Animate current slide.
     */
    addWorker('reset, move', function (stage, cache) {
        var context = this,
            $slide = stage.$slides.eq(cache.currentPosition - 1).addClass(context.settings.currentResizedClass);
        cache.animation.add(animateSlide(
            $slide, stage, context.settings,
            true,
            cache.slideSizeSpeed,
            stage.positioAnimation && !context.moved
                ? 0
                : (cache.moveSpeed || 0) - (cache.baseSpeed || 0),
            stage.positioAnimation && !context.moved
        ));
    }, 350);

    /**
     * Get size runnables.
     */
    setRunnableType('size');

    /**
     * Set current slides - slides that will be set as current to calculate size.
     */
    addRunnable('viewExplicit', function (stage, cache) {
        cache.$current = stage.$slides;
    });

    /**
     * Set current slides - slides that will be set as current to calculate size.
     * Set default slides.
     */
    addRunnable('activeHeight, inViewExplicit, visibleExplicit', function (stage, cache) {
        cache.$current = cache.$slides.filter('.' + this.settings.currentClass);
        cache.$slides = cache.$slides.not(cache.$current);
    });

    /**
     * Calculate slides sizes.
     */
    addRunnable('explicitHeight', function (stage, cache) {
        var context = this,
            settings = context.settings,
            removeClasses = settings.currentResizedClass + ' ' + settings.currentClass + ' ' + settings.activeClass;
        cache.$slides.each(function (i, el) {
            var $slide = $(el),
                classes = $slide.attr('class'),
                storeHeight = $slide.css(cssProps.height),
                storeWidth = $slide.css(cssProps.width),
                width, size;
            if (storeHeight === '0px') {
                storeHeight = false;
            }
            if (storeWidth === '0px') {
                storeWidth = false;
            }
            $slide
                .removeClass(removeClasses)
                .css(cssProps.height, '');
            if (settings.workers.vertical) {
                $slide.css(cssProps.width, '');
                width = $slide[cssProps.width]();
            } else {
                $slide.css(cssProps.width, stage.slideSize);
                width = stage.slideSize;
            }
            size = stage.slideRatio
                ? countHeightRatio(width, stage.slideRatio)
                : $slide[cssProps.height]();
            $slide.attr('class', classes);
            if (storeWidth) {
                $slide.css(cssProps.width, storeWidth);
            }
            if (storeHeight) {
                $slide.css(cssProps.height, storeHeight);
            }
            cache.slideSizes.push(size);
        });
        cache.$slides = $([]);
    }, 5);

    /**
     * Calculate current slides sizes.
     */
    addRunnable('explicitHeight', function (stage, cache) {
        var context = this,
            settings = context.settings,
            addClasses = settings.currentResizedClass + ' ' + settings.currentClass + ' ' + settings.activeClass;
        cache.$current.each(function (i, el) {
            var $slide = $(el),
                classes = $slide.attr('class'),
                storeWidth = $slide.css(cssProps.width),
                storeHeight = $slide.css(cssProps.height),
                width, size;
            $slide
                .addClass(addClasses)
                .css(cssProps.height, '')
                .css(cssProps.width, stage.currentSlideSize);
            width = stage.currentSlideSize;
            size = stage.slideRatio
                ? countHeightRatio(width, stage.slideRatio)
                : $slide[cssProps.height]();
            $slide
                .attr('class', classes)
                .css(cssProps.height, storeHeight)
                .css(cssProps.width, storeWidth);
            cache.slideSizes.push(size);
        });
    }, 5, 'horizontal');

    /**
     * Add runnables only for vertical.
     */
    subRequirement = 'vertical';
    addRequirement(subRequirement);

    /**
     * Calculate current slide size.
     */
    addRunnable('explicitHeight', function (stage, cache) {
        switch (stage.currentSlideUnits.type) {
        case '%':
            var scale = stage.currentSlideUnits.scale;
            if (scale < 1) {
                cache.currentSlideSize = cache.slideSize * scale;
            } else {
                cache.currentSlideSize = cache.slideSize;
                cache.slideSize /= scale;
            }
            break;
        case 'px':
            cache.currentSlideSize = stage.currentSlideUnits.value;
            break;
        }
    }, 10);

    /**
     * Calculate slide size.
     */
    addRunnable('explicitHeight', function (stage, cache) {
        cache.slideSize = Math.max(cache.currentSlideSize, cache.slideSize);
    }, 10, ['!carousel', 'carousel, !stickSlides']);

    /**
     * Adjust view size.
     */
    addRunnable('explicitHeight', function (stage, cache) {
        cache.viewSize += cache.currentSlideSize - cache.slideSize;
    }, 21, 'carousel, stickSlides');

    removeRequirement(subRequirement);

    /**
     * Set slide position runnables.
     */
    setRunnableType('setPosition');

    /**
     * Set previously resized slides to the default sizes.
     */
    addRunnable('live', function (stage, cache) {
        var context = this,
            currentResizedClass = context.settings.currentResizedClass,
            $slides = stage.$slides.filter('.' + currentResizedClass);
        $slides.each(function (i, slide) {
            var $slide = $(slide),
                position = stage.$slides.index($slide) + 1;
            if (cache.current === position || cache.realFrom === position) {
                return;
            }
            $slide
                .css(getSlideStyles($slide, stage, context.settings))
                .removeClass(currentResizedClass);
        });
    }, 8);

    /**
     * Set current and from slides styles.
     */
    addRunnable('live, extraCurrent', function (stage, cache) {
        var settings = this.settings;
        // Set current slide styles.
        setSlideStyles(
            stage,
            settings,
            stage.$slides.eq(cache.current - 1).addClass(settings.currentResizedClass),
            Slice.hasProperty(cache, 'percent')
                ? cache.percent
                : 1
        );
        // Set from slide styles.
        if (cache.realFrom && cache.realFrom !== cache.current) {
            setSlideStyles(
                stage,
                settings,
                stage.$slides.eq(cache.realFrom - 1).addClass(settings.currentResizedClass),
                Slice.hasProperty(cache, 'percentFrom')
                    ? cache.percentFrom
                    : 0
            );
        }
    }, 8);

    /**
     * Resize runnables.
     */
    setRunnableType('resize');

    /**
     * Check slide size.
     */
    addRunnable('check', function (stage, cache) {
        cache.$changed.filter('.' + this.settings.slideClass)
            .each(function (i, slide) {
                var $slide = $(slide),
                    width = $slide.css(cssProps.width),
                    height = $slide.css(cssProps.height);
                $slide.css({
                    height: '',
                    width: ''
                });
                if (Math.abs(parseFloat(width) - $slide.width()) > 1 ||
                    Math.abs(parseFloat(height) - $slide.height()) > 1) {
                    cache.result = true;
                    return false;
                }
                $slide.css({
                    height: height,
                    width: width
                });
            });
        if (cache.result) {
            return false;
        }
    }, 0, 'predictSlideSize');

    /**
     * Check slide visibility runnables.
     * Set slide size to current slide size.
     */
    setRunnableType('slideVisibility');
    addRunnable('current', function (stage, cache) {
        cache.slideSize = stage.currentSlideSize;
    }, 30);
    addRunnable('slide', function (stage, cache) {
        var normalizeStyle;
        if (cache.position === cache.current) {
            cache.slideSize = stage.currentSlideSize;
            normalizeStyle = stage.currentSlideSpacingStyle;
        } else {
            normalizeStyle = stage.slideSpacingStyle;
        }
        cache.start -= normalizeStyle[stage.spacingProps[0]];
        cache.end -= normalizeStyle[stage.spacingProps[0]];
    }, 30, '!stickSlides');

    /**
     * Get slides.
     */
    setRunnableType('slide');

    /**
     * Check if has coordinates.
     * Set current slide position.
     */
    addRunnable('visible, inView', function (stage, cache) {
        cache.currentSpacing = stage.currentSlideSpacingStyle[stage.spacingProps[0]] - stage.slideSpacing;
        var space = stage.slideSpacingStyle[stage.spacingProps[0]] - stage.slideSpacing;
        cache.start -= space;
        cache.end += space;
    }, 0, '!stickSlides');

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    removeRequirement(tempRequirement);
    removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceSlide = SliceSlide;
}));
