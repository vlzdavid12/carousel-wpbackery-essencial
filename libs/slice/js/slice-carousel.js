/* global jQuery, Slice */

/**
 * Slice Slider: Carousel Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';
    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Carousel class
        carouselClass: 'slice-carousel',
        // Slides step, when false - scrolls the same ammount of slides as 'slidesToShow'
        slidesToScroll: false,
        // Number of slides to show, when set to greater that 1 enables carousel
        slidesToShow: false,
        // Makes it always show 'slidesToShow' slides at viewport
        slidesToShowFit: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable,
        setRunnableType = factory.setRunnableType,
        addRequirement = factory.addRequirement,
        removeRequirement = factory.removeRequirement;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'carousel';

    /**
     * Requirements for some cases.
     */
    var tempRequirement;

    /**
     * Normalize slide position in carousel.
     *
     * @param {number} position - Slide position.
     * @param {object} stage - Stage object.
     * @returns {number} Normalized position.
     */
    function normalizePosition (position, stage) {
        if (position >= stage.lastSlide) {
            return stage.lastSlide;
        } else if (position <= stage.startSlide) {
            return stage.startSlide;
        }
        var checkPosition = position > stage.startSlide
                ? position - (stage.startSlide - 1)
                : 1,
            pagePosition = stage.startSlide + stage.slidesStep * (Math.ceil(checkPosition / stage.slidesStep) - 1);
        if (pagePosition > stage.lastSlide) {
            pagePosition = stage.lastSlide;
        } else if (pagePosition < stage.startSlide) {
            pagePosition = stage.startSlide;
        }
        return pagePosition;
    }

    /**
     * Carousel plugin constructor.
     *
     * @class SliceCarousel
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceCarousel (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceCarousel.prototype.setup = function () {
        var settings = this.slice.settings;
        if (settings.slidesToShow && settings.slidesToShow > 1) {
            // Enable carousel
            settings.workers.single = false;
            settings.workers.carousel = true;
        }
        this.slice.$element.toggleClass(settings.carouselClass, settings.slidesToShow && settings.slidesToShow > 1);
        settings.workers.showFit = !!settings.slidesToShowFit;
    };

    /**
     * Add workers and runnables for carousel mode only.
     */
    addRequirement(requirement);

    /**
     * Set slides to show, slides step and minimum slides lenght.
     */
    addWorker('settings', function (stage) {
        stage.slidesToShow = this.settings.slidesToShow;
        stage.slidesStep = this.settings.slidesToScroll || stage.slidesToShow;
        stage.minSlides = stage.slidesToShow;
    });

    /**
     * Calculate work space size.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.workSpace -= stage.slideSpacing * (stage.slidesToShow - 1);
    }, 21, 'fixedslides, spaced');

    /**
     * Calculate slide size.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.slideSize = stage.workSpace / stage.slidesToShow;
    }, 30, 'fixedsize');

    /**
     * Add workers only for fixed size slides.
     * Calculate last slide position.
     */
    tempRequirement = 'fixedslides';
    addRequirement(tempRequirement);
    addWorker('edges, slides, settings', function (stage) {
        stage.lastSlide = stage.startSlide +
            stage.slidesStep * (Math.ceil((stage.lastSlide - stage.startSlide + 1) / stage.slidesStep) - 1);
    }, 110, '!showFit');
    addWorker('edges, slides, settings', function (stage) {
        stage.lastSlide = stage.lastSlide - stage.slidesToShow + 1;
    }, 110, '!loop, showFit');
    addWorker('edges, slides, settings', function (stage) {
        stage.lastSlide = stage.lastSlide - stage.slidesStep + 1;
    }, 110, 'loop, showFit');
    removeRequirement(tempRequirement);

    /**
     * Get size runnables.
     * Calculate view size.
     */
    setRunnableType('size');
    addRunnable('activeHeight, viewHeight', function (stage, cache) {
        cache.viewSize = cache.slideSize * stage.slidesToShow;
    }, 20, 'fixedsize, vertical');
    addRunnable('activeHeight, viewHeight', function (stage, cache) {
        cache.slideSpacing = (cache.slideSpacing || 0) +
            Slice.Unit.revPx(this.settings.spacing, cache.viewSize);
        cache.viewSize += cache.slideSpacing * (stage.slidesToShow - 1);
    }, 25, 'spaced, vertical');

    /**
     * Get slide position runnables.
     * Normalize position.
     */
    setRunnableType('position');
    addRunnable('normalize', function (stage, cache) {
        cache.normalized = normalizePosition(cache.normalized, stage);
    }, 60);

    /**
     * Set slide position runnables.
     * Adjust active slides end position.
     */
    setRunnableType('setPosition');
    addRunnable('current, extraCurrent', function (stage, cache) {
        cache.end += stage.slidesToShow - 1;
    }, 20, 'fixedslides');

    /**
     * Get previous/next slide position runnables.
     */
    setRunnableType('movePosition');
    addRunnable('next', function (stage, cache) {
        cache.to += stage.slidesStep;
        // Check if next position crossed last slide position
        if (cache.to > stage.lastSlide && cache.position < stage.lastSlide) {
            cache.to = stage.lastSlide;
        }
    }, 20);
    addRunnable('prev', function (stage, cache) {
        if (cache.to === stage.lastSlide) {
            cache.to -= 1;
        } else {
            cache.to -= stage.slidesStep;
        }
    }, 20);


    /**
     * Get slide position by coordinates runnables.
     * Normalize position.
     */
    setRunnableType('closest');
    addRunnable('next', function (stage, cache) {
        var current = stage.position,
            normalized = cache.normalized;
        if (current === normalized && cache.position > current) {
            cache.normalized = this.run('getNextStrict', {position: current});
        }
    }, 40);
    addRunnable('prev', function (stage, cache) {
        var current = stage.position,
            normalized = cache.normalized;
        if (cache.position < current && current === normalized) {
            cache.normalized = this.run('getPrevStrict', {position: current});
        }
    }, 40);

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceCarousel = SliceCarousel;
}));
