/* global jQuery */

/**
 * Slice Slider: Minimalistic Theme.
 *
 * @author shininglab
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    $.fn.sliceSlider.addTheme('minimalistic', {
        defaults: {
            arrowsLayer: 4,
            dotsLayer: 5,
            navClass: 'st-nav-auto',
            nextArrow: '<a href="#" class="slice-next"><i class="st-chevron-next"><span></span><span></span></i></a>',
            prevArrow: '<a href="#" class="slice-prev"><i class="st-chevron-prev"><span></span><span></span></i></a>'
        },
        sliderClass: 'slice-theme-minimalistic'
    });
}));
