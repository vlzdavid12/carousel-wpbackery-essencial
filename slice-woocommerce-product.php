<?php
/*
Plugin Name:  Slice Carousel Woocommerce Products
Plugin URI:   https://www.ideoviral.com.co/
Description:  This plugin carousel general for WpBakery
Version:      1.0
Author:       David Fernando Valenzuela Pardo
Author URI:   https://www.behance.net/vlzdavid127aa9
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wpbakery-carousel
*/

if (!defined('ABSPATH')) exit;


if (!class_exists("WPBakeryShortCode")) {
    $plugin = plugin_basename(__FILE__) . '/slice-woocommerce-product/slice-woocommerce-product.php';
    deactivate_plugins($plugin);
}

// Element Class
class vcCarouselWpBakery extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        register_activation_hook(__FILE__, array($this, 'active_carousel_plugin'));
        add_action('admin_enqueue_scripts', array($this, 'script_admin_wpbackerry'), 10, 1);
        add_action('wp_enqueue_scripts', array($this, 'quizbook_frontend_styles'));
        add_action('admin_notices', array($this, 'test_vc_map_dependencies'));
        add_action('init', array($this, 'vc_infobox_mapping'));
        add_shortcode('vc_infobox', array($this, 'vc_infobox_html'));
        vc_add_shortcode_param('my_param', array($this, 'my_param_settings_field'), plugins_url('./assets/js/script.js', __FILE__));
    }


    public function script_admin_wpbackerry($hook)
    {
        wp_enqueue_style('css_wpbackerry_smart_addons', plugins_url('./assets/css/smart-addons.css', __FILE__));
        wp_enqueue_style('css_wpbackerry_smart_forms', plugins_url('./assets/css/smart-forms.css', __FILE__));
        wp_enqueue_script('script_wpbackerry_backend_select', plugins_url('./assets/js/select2.full.min.js', __FILE__), array('jquery'), 1.0, true);
    }

    public function quizbook_frontend_styles()
    {
        wp_enqueue_style('css_slice_bootstrap', plugins_url('./libs/bootstrap/css/bootstrap.css', __FILE__));
        wp_enqueue_style('css_slice_style', plugins_url('./libs/slice/css/slice.css', __FILE__));
        wp_enqueue_style('css_slice_banner', plugins_url('./libs/slice/css/slice-banner.css', __FILE__));
        wp_enqueue_style('css_slice_minimalistic_theme', plugins_url('./libs/slice/theme/minimalistic/slice-theme-minimalistic.css', __FILE__));


        wp_enqueue_script('js_slice_core', plugins_url('./libs/slice/js/slice-core.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_slider', plugins_url('./libs/slice/js/slice-slider.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_control_keys', plugins_url('./libs/slice/js/slice-control-keys.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_drag', plugins_url('./libs/slice/js/slice-drag.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_navegation', plugins_url('./libs/slice/js/slice-navigation.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_carousel', plugins_url('./libs/slice/js/slice-carousel.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_center', plugins_url('./libs/slice/js/slice-center.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_adaptive', plugins_url('./libs/slice/js/slice-adaptive.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_vertical', plugins_url('./libs/slice/js/slice-vertical.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_autoplay', plugins_url('./libs/slice/js/slice-autoplay.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_sloop', plugins_url('./libs/slice/js/slice-loop.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_responsive', plugins_url('./libs/slice/js/slice-responsive.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_autosize', plugins_url('./libs/slice/js/slice-autosize.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_autosize_center', plugins_url('./libs/slice/js/slice-autosize-center.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_autosize_stage', plugins_url('./libs/slice/js/slice-stage.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_autosize_slide', plugins_url('./libs/slice/js/slice-slide.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_autosize_cover', plugins_url('./libs/slice/js/slice-cover.js', __FILE__), array(), 1.0, true);
        wp_enqueue_script('js_slice_theme_minimalistic', plugins_url('./libs/slice/theme/minimalistic/slice-theme-minimalistic.js', __FILE__), array(), 1.0, true);
    }


    public function test_vc_map_dependencies()
    {
        if (!defined('WPB_VC_VERSION')) {
            $plugin_data = get_plugin_data(__FILE__);
            echo '
        <div class="updated">
          <p>' . sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'wpbakery-carousel'), $plugin_data['Name']) . '</p>
        </div>';
        }
    }


    public function my_param_settings_field($settings, $value)
    {
        global $post;

        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => 'publish',
        );

        $loop = new WP_Query($args);
        if ($loop->have_posts()) {
            $my_list = explode(',', $value);

            $item = '<select class="example-basic-multiple smart-select2"  name="mymultiselect[]" multiple="multiple">';

            while ($loop->have_posts()) : $loop->the_post();
                $active = in_array(get_the_ID(), $my_list) ? selected : '';
                $item .= '<option ' . $active . ' value="' . get_the_ID() . '">' . get_the_title() . '</option>';
            endwhile;

            $item .= '</select>
              <div class="my_param_block">'
                . '<input  name="' . esc_attr($settings['param_name']) . '" class="wpb_vc_param_value wpb-textinput ' .
                esc_attr($settings['param_name']) . ' ' .
                esc_attr($settings['type']) . '_field text-34567" type="hidden" value="' . esc_attr($value) . '" />'
                . '</div> ';

            return $item;
        }

        wp_reset_query();

    }

    // Element Mapping
    public function vc_infobox_mapping()
    {
        if (class_exists("WPBakeryShortCode")) {
            if (!defined('WPB_VC_VERSION')) {
                return;
            }

            // Map the block with vc_map()
            vc_map(
                array(
                    'name' => __('Woocommerce Products Slider', 'twpbakery-carousel'),
                    'base' => 'vc_infobox',
                    'description' => __('Slider products for woocommerce', 'wpbakery-carousel'),
                    'category' => __('Content', 'wpbakery-carousel'),
                    'icon' => get_template_directory_uri() . "/vc_extend/vc_icon.png",
                    'params' => array(

                        array(
                            "type" => "my_param",
                            "heading" => __("Flipping text", "js_composer"),
                            "param_name" => "mymultiselect",
                            "description" => __("Select products ofert", 'wpbakery-carousel'),
                            'group' => __('General', 'wpbakery-carousel'),
                        ),


                        array(
                            'type' => 'css_editor',
                            'heading' => __('Css', 'wpbakery-carousel'),
                            'param_name' => 'css',
                            'group' => __('Design options', 'wpbakery-carousel'),
                        ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'Columns Desktop', 'wpbakery-carousel' ),
                            'param_name' => 'columnsdesktop',
                            'value' => 4,
                            'group' => __('General', 'wpbakery-carousel'),
                        ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'Columns Mobil', 'wpbakery-carousel' ),
                            'param_name' => 'columnsmobil',
                            'value' => 2,
                            'group' => __('General', 'wpbakery-carousel'),
                        ),

                        array(
                            'type' => 'colorpicker',
                            'heading' => __( 'Color Ofert', 'wpbakery-carousel' ),
                            'param_name' => 'colorofert',
                            'value' => '#e0040b',
                            'description' => __( 'Color Ofert Pointer', 'wpbakery-carousel' ),
                            'group' => __('General', 'wpbakery-carousel'),
                        ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'playStartDelay', 'wpbakery-carousel' ),
                            'param_name' => 'playstartdelay',
                            'value' => 1000,
                            'group' => __('General', 'wpbakery-carousel'),
                        ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'autoplaySpeed', 'wpbakery-carousel' ),
                            'param_name' => 'autoplayspeed',
                            'value' => 3000,
                            'group' => __('General', 'wpbakery-carousel'),
                        ),

                        array(
                            'type' => 'colorpicker',
                            'heading' => __( 'Color Btn', 'wpbakery-carousel' ),
                            'param_name' => 'btnbgcolor',
                            'value' => '#e0040b',
                            'description' => __( 'Color Btn', 'wpbakery-carousel' ),
                            'group' => __('General', 'wpbakery-carousel'),
                        ),

                        array(
                            'type' => 'colorpicker',
                            'heading' => __( 'Color Btn Hover', 'wpbakery-carousel' ),
                            'param_name' => 'btnbgcolorhover',
                            'value' => '#e0040b',
                            'description' => __( 'Color Hover Btn', 'wpbakery-carousel' ),
                            'group' => __('General', 'wpbakery-carousel'),
                        ),

                    ),
                )
            );
        }
    }

    public function load_custom_css($css) {

        wp_enqueue_style( 'vc_shortcode-custom', plugins_url('./libs/slice/theme/minimalistic/slice-theme-minimalistic.css', __FILE__));
        wp_add_inline_style( 'vc_shortcode-custom', $css );

    }


    // Element HTML
    public function vc_infobox_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mymultiselect' => [],
                    'colorofert'=>'#e0040b',
                    'columnsdesktop'=>'4',
                    'columnsmobil'=>'2',
                    'playstartdelay'=>'1000',
                    'autoplayspeed'=>'2000',
                    'btnbgcolor'=>'',
                    'btnbgcolorhover'=>'',
                    'css' => '',
                ),
                $atts
            )
        );

        $css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '), $this->settings['base'], $atts);
        $my_list = explode(',', $mymultiselect);


        $args = array(
            'numberposts' => 12,
            'post_type' => 'product',
            'post_status' => 'publish',
            'order' => 'DESC',
            'include' => $my_list,
        );

        $news_posts = get_posts($args);

        // Fill $html var with data
        $html = '
        <div   class="vc-infobox-wrap ' . esc_attr($css_class) . '">
         <div class="slice-slider"
     data-slice-slider="loop;forceLoop;slidesToScroll:1;arrows;arrowsLayer:4;navClass:st-arrows-sides;slidesToShow:1;slidesToShowFit;spacing:2px;theme:minimalistic;autoplay;pauseOnHover;playStartDelay:'.$playstartdelay.';autoplaySpeed:'.$autoplayspeed.';"
     data-slice-slider-576="slidesToShow:'.$columnsmobil.';"
     data-slice-slider-768="slidesToShow:'.$columnsdesktop.';">';

        foreach ($news_posts as $item){

            $product = wc_get_product($item->ID);
            $image_id = get_post_meta($item->ID, '_thumbnail_id', true);
            $image = wp_get_attachment_url($image_id);

            //Price Ofert
            $sale = get_post_meta($item->ID, '_sale_price', true);

            if(!empty($sale)){
                $ofert = '<span  class="ofert" style="background: '.$colorofert.'" >Oferta</span>';
            }else{
                $ofert = '';
            }

            if(strlen($item->post_title) <= 25){
                $title = $item->post_title;
            }else{
                $title = substr($item->post_title, 0, 25). " [...]";
            }


            $style = '.slice-theme-minimalistic .st-chevron-prev, .slice-theme-minimalistic .st-chevron-next{ background:'.$btnbgcolor.';} 
            .slice-theme-minimalistic .slice-arrow:not(.slice-disabled):hover .st-chevron-prev span, .slice-theme-minimalistic .slice-arrow:not(.slice-disabled):focus .st-chevron-prev span, .slice-theme-minimalistic .slice-arrow:not(.slice-disabled):hover .st-chevron-next span, .slice-theme-minimalistic .slice-arrow:not(.slice-disabled):focus .st-chevron-next span{ background-color:#fff;}
            .slice-theme-minimalistic .st-chevron-prev:hover, .slice-theme-minimalistic .st-chevron-next:hover{ background-color:'.$btnbgcolorhover.';}';

            $this->load_custom_css($style);


            $html .= '<!--Card Image-->
       <div class="card">
           <div style="position: relative">
                   <a href="'.get_permalink($item->ID).'" target="_blank">
                            '.$ofert.'
                        <img class="card-img-top" src="'.$image.'" alt=""/>
                   </a>
           </div>
           <div class="card-body">
            <h5 class="card-title" ><a href="'.get_permalink($item->ID).'" target="_blank">'.$title.'</a></h5>
               <div class="card-text">
                <p class="price" >'. $product->get_price_html().'</p>
               </div>
            </div>
        </div>
    <!--End Card Image-->';

        }

        $html .= '</div>
        </div>';

        return $html;

    }

    public function active_carousel_plugin()
    {

        flush_rewrite_rules();
    }


} // End Element Class


// Element Class Init
new vcCarouselWpBakery();