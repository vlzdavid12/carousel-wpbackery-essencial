/* global jQuery, Slice */

/**
 * Slice Block Cover Plugin.
 * Slice Block Parallax Plugin.
 *
 * @author shininglab
 */

(function (fn) {
    'use strict';
    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery, window, document);
}(function ($, window, document) {
    'use strict';

    /**
     * Slice block cover default options.
     */
    var sliceCoverDefaults = {
        // Center area bottom position
        centerBottom: false,
        // Center area left position
        centerLeft: false,
        // Center area right position
        centerRight: false,
        // Center area top position
        centerTop: false,
        // Overflow area bottom position
        overflowBottom: false,
        // Overflow area left position
        overflowLeft: false,
        // Overflow area right position
        overflowRight: false,
        // Overflow area top position
        overflowTop: false,
        // Viewport element
        viewport: 'auto'
    };

    /**
     * Slice block parallax default options.
     */
    var sliceParallaxDefaults = {
        // Allow/disallow empty space while animating
        animationEmptySpace: false,
        // Animation frames per second
        animationFps: 60,
        // Maximal animation duration
        animationMaxDuration: 500,
        // Minimal animation duration
        animationMinDuration: false,
        // Animation speed
        animationSpeed: 1000,
        // Base distance to calculate animation speed
        animationSpeedBase: 800,
        // Synchronize X and Y axis animations
        animationSyncAxis: true,
        // Cursor area element: self|window|document|selector|jQuery|HTMLElement
        cursorArea: 'window',
        // Set cursor parallax position when cursor leaves viewport
        cursorOnLeave: 'start',
        // Reverse direction of X axis
        cursorReverseX: false,
        // Reverse direction of Y axis
        cursorReverseY: false,
        // Parallax cursor Y axis speed
        cursorSpeedX: 0,
        // Parallax cursor Y axis speed
        cursorSpeedY: 0,
        // Cursor parallax start position
        cursorStart: 'center',
        // Enable cursor parallax for touch, but will replace cursor viewport for window|document to self
        cursorTouch: false,
        // Reverse direction of Y axis
        reverseY: false,
        // Parallax Y axis speed
        speedY: [0.2, 1]
    };

    var ids = {};

    var fnSlice = $.fn.sliceSlider;

    /**
     * Check if device has cursor - mouse.
     */
    var hasCursor = window.matchMedia('(pointer:fine)').matches;

    /**
     * Get displacement value.
     *
     * @param {string} axis - Axis.
     * @param {number} scale - Scale value.
     * @param {number} currentSize - Element current size.
     * @param {number} viewSize - View size.
     * @param {object} center - Center square object.
     * @param {object} overflow - Overflow square object.
     * @returns {number} Displacement value.
     */
    function getDisplacementValue (axis, scale, currentSize, viewSize, center, overflow) {
        // Calculate min and max displacement
        var overMax = overflow[axis + 'Start'] * scale,
            overMin = overflow[axis + 'End'] * scale - viewSize,
            maxDisplacement = Math.min(Math.max(overMax, overMin), currentSize - viewSize),
            minDisplacement = Math.max(Math.min(overMax, overMin), 0);
        // Calculate displacement
        var displacement = center[axis + 'Center'] * scale - viewSize / 2,
            displacementStart = Math.min(center[axis + 'Start'] * scale, maxDisplacement),
            displacementEnd = Math.max(center[axis + 'End'] * scale - viewSize, minDisplacement);
        // If cover image centered area is overflown by one side try to move it to viewport
        if (!(displacement > displacementStart && displacement < displacementEnd)) {
            if (displacement > displacementStart) {
                displacement = displacementStart;
            } else if (displacement < displacementEnd) {
                displacement = displacementEnd;
            }
        }
        return displacement;
    }

    /**
     * Calculate square positions for provided axis.
     *
     * @param {string} axis - Axis.
     * @param {number} naturalSize - Natural size.
     * @param {number} posStart - Position start.
     * @param {number} posEnd - Position end.
     * @param {boolean} calculateCenter - Whether to calculate center position or not.
     * @returns {object} Square positions for provided axis.
     */
    function calculateSquareAxis (axis, naturalSize, posStart, posEnd, calculateCenter) {
        var start = posStart === false
                ? false
                : Slice.Unit.toPx(posStart, naturalSize),
            end = posEnd === false
                ? false
                : Slice.Unit.toPx(posEnd, naturalSize);
        if (end === false) {
            end = start;
        } else {
            end = naturalSize - end;
        }
        if (start === false) {
            start = end;
        }
        var square = {},
            key = axis || '';
        square[key + 'Start'] = Math.min(start, end);
        square[key + 'End'] = Math.max(start, end);
        if (calculateCenter) {
            square[key + 'Center'] = square[key + 'Start'] + (square[key + 'End'] - square[key + 'Start']) / 2;
        }
        return square;
    }

    /**
     * Get breakpoint.
     *
     * @param {Array} breakpoints - List of breakpoints.
     * @param {number} resolution - Current resolution.
     * @returns {object} Breakpoint.
     */
    function getBreakpoint (breakpoints, resolution) {
        for (var i = breakpoints.length - 1; i >= 0; i--) {
            if (resolution >= breakpoints[i].breakpoint) {
                return fnSlice.buildBreakPoint(breakpoints, i);
            }
        }
        return fnSlice.buildBreakPoint(breakpoints, 0);
    }

    /**
     * Get options for current breakpoint.
     *
     * @param {Array} breakpoints - List of breakpoints.
     * @param {number} resolution - Current resolution.
     * @returns {object} Breakpoint options.
     */
    function getOptions (breakpoints, resolution) {
        var breakpoint = getBreakpoint(breakpoints, resolution);
        return breakpoint.options;
    }

    /**
     * Slice block cover plugin.
     *
     * @class SliceCover
     * @param {HTMLElement|jQuery} element - The element to create the cover for.
     * @param {object} [options] - The options.
     */
    function SliceCover (element, options) {
        var context = this;
        context.id = Slice.uniqueId(ids, 'cover');
        ids[context.id] = true;
        // Cover element.
        context.$element = $(element);
        context.$element.data(context.dataAttr, context);
        // Set breakpoints.
        var attrName = options.optionsProvider || context.optionsProvider,
            defaults = $.extend(
                context.getDefaults(),
                options || {},
                fnSlice.parseOptions(context.$element.data(attrName))
            ),
            responsive = fnSlice.getResponsiveOptions(context.$element, attrName);
        context.breakpoints = fnSlice.createBreakpoints(defaults, responsive);
        // Set options for current window size
        context.options = getOptions(context.breakpoints, $(window).width());
        // Create cover elements
        context.createCover(context.$element);
        // Load cover image
        context.complete = false;
        context.$image.on('load.slice.cover fail.slice.cover', function () {
            context.setImageSize();
            context.buildOptions();
            context.redraw();
        });
        if (!context.complete && context.image.complete) {
            context.setImageSize();
            context.buildOptions();
            context.redraw();
        }
        // Add resize event
        context.$element
            .on('contentresize.slice.cover', function (e, sizes) {
                context.resize(sizes.width);
            });
    }

    // Data attribute to store SliceCover instance
    SliceCover.prototype.dataAttr = 'slice.cover';
    // Data attribute to get options from
    SliceCover.prototype.optionsProvider = 'sliceCover';
    // Cover image element class
    SliceCover.prototype.imageClass = 'slice-cover-image';
    // Cover overflow element class
    SliceCover.prototype.overflowClass = 'slice-cover-overflow';

    /**
     * Get default options.
     *
     * @returns {object} Options.
     */
    SliceCover.prototype.getDefaults = function () {
        return $.extend({}, sliceCoverDefaults, $.fn.sliceCover.Defaults || {});
    };

    /**
     * Create elements structure.
     */
    SliceCover.prototype.createCover = function () {
        var context = this,
            $element = context.$element;
        context.src = $element.data('sliceCoverSrc') || $element.data('src') || $element.attr('src');
        context.$image = $element;
        context.image = context.$image[0];
        context.$overflow = $('<span/>');
        context.$image.after(context.$overflow);
        context.$overflow.append(context.$image);
        context.$image.addClass(context.imageClass);
        context.$overflow.addClass(context.overflowClass);
    };

    /**
     * Get image size.
     *
     * @returns {SliceCover} Current SliceCover instance.
     */
    SliceCover.prototype.setImageSize = function () {
        var context = this,
            image = context.image;
        if (!image.complete) {
            context.failed = true;
            return context;
        }
        context.complete = true;
        // Set natural sizes
        var naturalHeight = image.naturalHeight,
            naturalWidth = image.naturalWidth;
        context.naturalHeight = image.naturalHeight;
        context.naturalWidth = image.naturalWidth;
        // Check if cover block can be worked with
        var failed = naturalHeight === 0 || naturalWidth === 0;
        context.failed = failed;
        context.$overflow.toggleClass('failed', failed);
        return context;
    };

    /**
     * Build options.
     *
     * @returns {SliceCover} Current SliceCover instance.
     */
    SliceCover.prototype.buildOptions = function () {
        var context = this;
        if (context.failed) {
            return context;
        }
        context.square = {};
        var square = context.square,
            options = context.options;
        if (options.centerLeft === false && options.centerRight === false) {
            options.centerLeft = 0;
        }
        if (options.centerTop === false && options.centerBottom === false) {
            options.centerTop = 0;
        }
        if (options.overflowLeft === false) {
            options.overflowLeft = 0;
        }
        if (options.overflowRight === false) {
            options.overflowRight = 0;
        }
        if (options.overflowTop === false) {
            options.overflowTop = 0;
        }
        if (options.overflowBottom === false) {
            options.overflowBottom = 0;
        }
        // Calculate center square
        square.center = $.extend(
            calculateSquareAxis('x', context.naturalWidth, options.centerLeft, options.centerRight, true),
            calculateSquareAxis('y', context.naturalHeight, options.centerTop, options.centerBottom, true)
        );
        // Calculate overflow square
        square.overflow = $.extend(
            calculateSquareAxis('x', context.naturalWidth, options.overflowLeft, options.overflowRight, true),
            calculateSquareAxis('y', context.naturalHeight, options.overflowTop, options.overflowBottom, true)
        );
        square.overflow.xSize = square.overflow.xEnd - square.overflow.xStart;
        square.overflow.ySize = square.overflow.yEnd - square.overflow.yStart;
        if (options.viewport) {
            switch (options.viewport) {
            case 'window':
                context.$viewport = $(window);
                break;
            default:
                var $viewport = options.viewport
                    ? $(options.viewport)
                    : [];
                if ($viewport.length) {
                    context.$viewport = $viewport.eq(0);
                } else {
                    context.$viewport = context.$overflow;
                }
                break;
            }
        }
        return context;
    };

    /**
     * Resize.
     *
     * @param {number} resolution - Current resolution.
     * @returns {SliceCover} Current SliceCover instance.
     */
    SliceCover.prototype.resize = function (resolution) {
        var context = this;
        context.options = getOptions(context.breakpoints, resolution);
        context.buildOptions();
        if (context.resizeTimeout) {
            clearTimeout(context.resizeTimeout);
        }
        context.resizeTimeout = setTimeout(function () {
            context.resizeTimeout = false;
            context.redraw();
        }, 50);
        return context;
    };

    /**
     * Get view size.
     *
     * @returns {object} View height and width.
     */
    SliceCover.prototype.getViewSize = function () {
        var context = this,
            limitWidth = context.$overflow.width(),
            limitHeight = context.$overflow.height(),
            totalWidth, totalHeight;
        if (context.$viewport) {
            totalWidth = context.$viewport.width();
            totalHeight = context.$viewport.height();
        } else {
            totalWidth = limitWidth;
            totalHeight = limitHeight;
        }
        return {
            height: totalHeight,
            width: totalWidth
        };
    };

    /**
     * Build options.
     *
     * @returns {SliceCover} Current SliceCover instance.
     */
    SliceCover.prototype.redraw = function () {
        var context = this;
        if (context.failed || context.$element.is(':hidden')) {
            return context;
        }
        if (context.resizeTimeout) {
            clearTimeout(context.resizeTimeout);
            context.resizeTimeout = false;
        }
        var naturalHeight = context.naturalHeight,
            naturalWidth = context.naturalWidth;
        // Calculate viewport size
        var viewSize = context.getViewSize(),
            viewWidth = viewSize.width,
            viewHeight = viewSize.height;
        // Calculate scale
        var overflow = context.square.overflow,
            scale;
        // Check for upscale or downscale
        if (overflow.xSize < viewWidth && overflow.ySize < viewHeight) {
            if (naturalWidth >= viewWidth && naturalHeight >= viewHeight) {
                scale = 1;
            } else {
                scale = Math.min(viewWidth / naturalWidth, viewHeight / naturalHeight);
            }
        } else {
            scale = Math.min(viewWidth / overflow.xSize, viewHeight / overflow.ySize);
        }
        // Normalize scale
        var scaleSize = scale * naturalWidth;
        if (scaleSize < viewWidth) {
            scale += (viewWidth - scaleSize) / naturalWidth;
        }
        scaleSize = scale * naturalHeight;
        if (scaleSize < viewHeight) {
            scale += (viewHeight - scaleSize) / naturalHeight;
        }
        var css = {
            height: Math.ceil(naturalHeight * scale),
            left: 0,
            position: 'absolute',
            top: 0,
            width: Math.ceil(naturalWidth * scale)
        };
        // Calculate center
        var center = context.square.center;
        if (css.width > Math.ceil(viewWidth)) {
            css.left = -getDisplacementValue('x', scale, css.width, viewWidth, center, overflow);
        }
        if (css.height > Math.ceil(viewHeight)) {
            css.top = -getDisplacementValue('y', scale, css.height, viewHeight, center, overflow);
        }
        center.x = center.xCenter * scale + css.left;
        center.y = center.yCenter * scale + css.top;
        context.$image.css(css);
        context.drawn = true;
        return context;
    };

    /**
     * Calculate minimal displacement.
     *
     * @param {number} offset - Current offset.
     * @param {number} viewHeight - View height.
     * @param {number} min - Minimal offset.
     * @param {number} max - Maximum offset.
     * @returns {object} Minimal displacement.
     */
    function calculateDisplacemenMin (offset, viewHeight, min, max) {
        return Math.min(offset, max) - Math.max(min + viewHeight, 0);
    }

    /**
     * Create parallax styles.
     *
     * @param {number} left - Left position.
     * @param {number} top - Top position.
     * @returns {object} Parallax styles.
     */
    function createParallaxCSS (left, top) {
        return {
            transform: 'translate3d(' + left + 'px, ' + top + 'px, 0)'
        };
    }

    /**
     * Parse speed option.
     *
     * @param {number|Array} value - Speed value.
     * @returns {number|Array} Parsed speed value.
     */
    function parseSpeed (value) {
        if (typeof value === 'number') {
            return Math.max(value, 0);
        } else if (Array.isArray(value)) {
            var ln = value.length,
                speedStart = ln > 0
                    ? Math.max(value[0], 0)
                    : 0,
                speedEnd = ln > 1
                    ? Math.max(value[1], speedStart)
                    : Infinity;
            return [speedStart, speedEnd];
        }
        return value;
    }

    /**
     * Calculate cursor speed.
     *
     * @param {number|Array} optionsSpeed - Speed value.
     * @param {number} naturalOpositeSize - Natural oposite size.
     * @param {number} naturalSize - Natural size.
     * @param {number} parallaxOpositeSize - Parallax oposite size.
     * @param {number} parallaxSize - Parallax size.
     * @param {number} overflowOpositeSize - Overflow square oposite size.
     * @param {number} overflowSize - Overflow square size.
     * @param {number} cursorViewSize - Cursor view size.
     * @param {number} viewSize - View size.
     * @returns {number|Array} Parsed speed value.
     */
    function calculateCursorSpeed (
        optionsSpeed,
        naturalOpositeSize, naturalSize,
        parallaxOpositeSize, parallaxSize,
        overflowOpositeSize, overflowSize,
        cursorViewSize, viewSize
    ) {
        var speed, size;
        if (typeof optionsSpeed === 'number') {
            speed = optionsSpeed;
        } else {
            size = Math.max(overflowOpositeSize < parallaxOpositeSize
                ? parallaxOpositeSize / naturalOpositeSize * naturalSize
                : parallaxOpositeSize / overflowOpositeSize * overflowSize, parallaxSize);
            var scrollSize = parallaxSize + cursorViewSize - viewSize;
            speed = scrollSize === 0
                ? 0
                : (size - parallaxSize) / scrollSize;
            if (Array.isArray(optionsSpeed)) {
                speed = Math.min(Math.max(speed, optionsSpeed[0]), optionsSpeed[1]);
            }
        }
        return speed;
    }

    /**
     * Slice block parallax plugin.
     *
     * @class SliceParallax
     * @param {HTMLElement|jQuery} element - The element to create the parallax for.
     * @param {object} [options] - The options.
     */
    function SliceParallax (element, options) {
        var context = this;
        context.scroll = {
            position: {
                left: window.scrollX || window.pageXOffset || 0,
                top: window.scrollY || window.pageYOffset || 0
            }
        };
        context.cursor = {
            position: {
                left: 0,
                top: 0
            }
        };
        SliceCover.call(this, element, options);
        window.addEventListener('scroll', function () {
            var scroll = context.scroll,
                cursor = context.cursor,
                scrollPosition = {
                    left: window.scrollX || window.pageXOffset || 0,
                    top: window.scrollY || window.pageYOffset || 0
                };
            if (cursor.on && cursor.normalizeScroll) {
                cursor.position.left -= scroll.position.left - scrollPosition.left;
                cursor.position.top -= scroll.position.top - scrollPosition.top;
            }
            context.scroll.position = scrollPosition;
            context.buildLimits();
            context.position();
        });
    }

    /**
     * SliceParallax extends SliceCover.
     */
    SliceParallax.prototype = Object.create(SliceCover.prototype, {});
    SliceParallax.prototype.constructor = SliceParallax;

    // Data attribute to store SliceParallax instance
    SliceParallax.prototype.dataAttr = 'slice.parallax';
    // Data attribute to get options from
    SliceParallax.prototype.optionsProvider = 'sliceParallax';
    // Parallax overflow element class
    SliceCover.prototype.parallaxClass = 'slice-parallax-overflow';

    /**
     * Get default options.
     *
     * @returns {object} Options.
     */
    SliceParallax.prototype.getDefaults = function () {
        return $.extend({}, sliceCoverDefaults, sliceParallaxDefaults, $.fn.sliceParallax.Defaults || {});
    };

    SliceParallax.prototype.createCover = function () {
        var context = this;
        SliceCover.prototype.createCover.call(context);
        context.$parallax = $('<span/>')
            .addClass(context.parallaxClass)
            .append(context.$image)
            .appendTo(context.$overflow);
    };

    /**
     * Get touch data.
     *
     * @param {event} event - Touch event object.
     * @returns {object} Touch data.
     */
    function getTouchData (event) {
        return event.touches && event.touches.length
            ? event.touches[0]
            : event.changedTouches && event.changedTouches.length
                ? event.changedTouches[0]
                : event;
    }

    /**
     * Set cursor viewport sizes.
     */
    function setCursorSizeDefault () {
        var cursor = this.cursor;
        cursor.viewHeight = cursor.$viewport.height();
        cursor.viewWidth = cursor.$viewport.width();
    }

    /**
     * Cursor move default handler.
     *
     * @param {object} e - Mouse move event.
     */
    function cursorMoveDefault (e) {
        if (!hasCursor) {
            e.preventDefault();
        }
        var offset = this.cursor.$viewport.offset(),
            data = getTouchData(e.originalEvent);
        this.position(false, {
            left: data.pageX - offset.left,
            top: data.pageY - offset.top
        });
    }

    /**
     * Set cursor viewport sizes for document.
     */
    function setCursorSizeDocument () {
        var context = this,
            view = context.view,
            cursor = context.cursor,
            offset = view.offset;
        cursor.viewYStart = Math.max(offset.top - context.windowHeight, 0);
        cursor.viewYEnd = Math.min(offset.top + view.height + context.windowHeight, context.scrollHeight);
        cursor.viewXStart = Math.max(offset.left - context.windowWidth, 0);
        cursor.viewXEnd = Math.min(offset.left + view.width + context.windowWidth, context.scrollWidth);
        cursor.viewHeight = cursor.viewYEnd - cursor.viewYStart;
        cursor.viewWidth = cursor.viewXEnd - cursor.viewXStart;
    }

    /**
     * Cursor move on document handler.
     *
     * @param {object} e - Mouse move event.
     */
    function cursorMoveDocument (e) {
        if (!hasCursor) {
            e.preventDefault();
        }
        var data = getTouchData(e.originalEvent);
        this.position(false, {
            left: data.pageX - this.cursor.viewXStart,
            top: data.pageY - this.cursor.viewYStart
        });
    }

    /**
     * Set cursor viewport sizes for window.
     */
    function setCursorSizeWindow () {
        var context = this,
            cursor = context.cursor;
        cursor.viewHeight = context.windowHeight;
        cursor.viewWidth = context.windowWidth;
    }

    /**
     * Cursor move on window handler.
     *
     * @param {object} e - Mouse move event.
     */
    function cursorMoveWindow (e) {
        if (!hasCursor) {
            e.preventDefault();
        }
        var data = getTouchData(e.originalEvent);
        this.position(false, {
            left: data.pageX - (window.pageXOffset || document.scrollLeft || 0) - (document.clientLeft || 0),
            top: data.pageY - (window.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0)
        });
    }

    /**
     * Create elements structure.
     */
    SliceParallax.prototype.buildOptions = function () {
        SliceCover.prototype.buildOptions.call(this);
        var context = this,
            $w = $(window),
            scroll = context.scroll,
            cursor = context.cursor,
            options = context.options,
            animation;
        context.windowWidth = $w.width();
        context.windowHeight = $w.height();
        // Animation.
        animation = {
            base: options.animationSpeedBase,
            interval: 1000 / options.animationFps,
            max: options.animationMaxDuration || 1,
            min: options.animationMinDuration || 1,
            space: options.animationEmptySpace,
            speed: options.animationSpeed,
            sync: options.animationSyncAxis
        };
        animation.step = animation.base / animation.speed;
        context.animation = animation;
        // Scroll.
        scroll.speedY = parseSpeed(options.speedY);
        scroll.multY = context.options.reverseY
            ? -1
            : 1;
        // Cursor.
        cursor.speedX = parseSpeed(options.cursorSpeedX);
        cursor.speedY = parseSpeed(options.cursorSpeedY);
        cursor.multX = context.options.cursorReverseX
            ? -1
            : 1;
        cursor.multY = context.options.cursorReverseY
            ? -1
            : 1;
        cursor.move = cursorMoveDefault.bind(context);
        cursor.setViewSize = setCursorSizeDefault.bind(context);
        cursor.normalizeScroll = true;
        cursor.enabled = true;
        var cursorArea = options.cursorArea;
        if (cursor.$events) {
            cursor.$events.off('.slice.parallax.' + context.id);
        }
        if (!hasCursor) {
            if (!options.cursorTouch) {
                cursor.enabled = false;
                cursorArea = $([]);
            } else if (['window', 'document'].indexOf(cursorArea) > -1) {
                cursorArea = 'self';
            }
        }
        switch (cursorArea) {
        case 'self':
            cursor.$viewport = context.$overflow;
            cursor.$events = cursor.$viewport;
            break;
        case 'window':
            cursor.$viewport = $(window);
            cursor.move = cursorMoveWindow.bind(context);
            cursor.setViewSize = setCursorSizeWindow.bind(context);
            cursor.normalizeScroll = false;
            cursor.$events = $(document);
            break;
        case 'document':
            cursor.$viewport = $(document);
            cursor.move = cursorMoveDocument.bind(context);
            cursor.setViewSize = setCursorSizeDocument.bind(context);
            cursor.$events = cursor.$viewport;
            cursor.on = true;
            break;
        default:
            cursor.$viewport = context.$overflow.find(options.cursorArea);
            if (!cursor.$viewport.length) {
                cursor.$viewport = $(options.cursorArea);
            }
            if (!cursor.$viewport.length) {
                cursor.$viewport = context.$overflow;
            }
            cursor.$events = cursor.$viewport;
            break;
        }
        cursor.$events
            .on('mouseenter.slice.parallax.' + context.id +
                ' touchstart.slice.parallax.' + context.id, function (e) {
                context.cursor.on = true;
                cursor.move(e);
            })
            .on('mouseleave.slice.parallax.' + context.id +
                ' touchend.slice.parallax.' + context.id, function () {
                var curs = context.cursor;
                curs.on = false;
                curs.position = calculateCursorPosition(
                    context,
                    context.options.cursorOnLeave,
                    curs.position
                );
                context.position();
            })
            .on('mousemove.slice.parallax.' + context.id +
                ' touchmove.slice.parallax.' + context.id, cursor.move);
    };

    /**
     * Check paralax limits.
     *
     * @param {SliceParallax} parallax - SliceParallax instance.
     * @param {number} value - Value to check.
     * @param {string} axis - Axis name.
     * @returns {number} Normalized value.
     */
    function checkLimits (parallax, value, axis) {
        if (parallax.animation.space) {
            return value;
        }
        var limits = parallax.limits;
        return Math.min(Math.max(value, limits.min[axis]), limits.max[axis]);
    }

    /**
     * Build paralax limits.
     *
     * @returns {SliceParallax} Current SliceParallax instance.
     */
    SliceParallax.prototype.buildLimits = function () {
        var context = this,
            view = context.view,
            scroll = context.scroll;
        if (context.drawn) {
            context.limits = {
                max: {
                    left: Math.max(
                        0,
                        scroll.position.left - view.offset.left
                    ),
                    top: Math.max(
                        0,
                        scroll.position.top - view.offset.top
                    )
                },
                min: {
                    left: Math.min(
                        view.width,
                        scroll.position.left + context.windowWidth - view.offset.left
                    ) - context.parallaxWidth,
                    top: Math.min(
                        view.height,
                        scroll.position.top + context.windowHeight - view.offset.top
                    ) - context.parallaxHeight
                }
            };
        }
        return this;
    };

    /**
     * Get view size.
     *
     * @returns {object} View height and width size.
     */
    SliceParallax.prototype.getViewSize = function () {
        var context = this,
            options = context.options,
            scroll = context.scroll,
            cursor = context.cursor,
            viewSize = SliceCover.prototype.getViewSize.call(context),
            offset = context.$viewport.offset(),
            viewWidth = viewSize.width,
            viewHeight = viewSize.height,
            displacementYMin = false,
            parallaxHeight = viewHeight,
            parallaxWidth = viewWidth,
            optionsSpeed = scroll.speedY,
            overflow = context.square.overflow,
            offsetYmin = offset.top - context.windowHeight,
            offsetYmax = $(document).height() - context.windowHeight,
            offsetYEnd = Math.min(offset.top + viewHeight, offsetYmax),
            spaceYend = Math.max(offsetYEnd - offset.top, 0),
            speed;
        context.view = {
            height: viewHeight,
            offset: offset,
            width: viewWidth
        };
        context.lastYScreen = offsetYmax;
        context.lastXScreen = $(document).width() - context.windowWidth;
        scroll.offsetYStart = Math.max(offsetYmin, 0);
        scroll.height = offsetYEnd - scroll.offsetYStart;
        if (typeof optionsSpeed === 'number') {
            speed = optionsSpeed;
        } else {
            parallaxHeight = Math.max(overflow.xSize < viewWidth
                ? viewWidth / context.naturalWidth * context.naturalHeight
                : viewWidth / overflow.xSize * overflow.ySize, viewHeight);
            if (options.reverseY) {
                speed = (parallaxHeight - viewHeight) / scroll.height;
            } else {
                displacementYMin = calculateDisplacemenMin(offset.top, viewHeight, offsetYmin, offsetYmax);
                speed = displacementYMin === 0
                    ? 0
                    : (parallaxHeight - viewHeight) / displacementYMin;
                if (speed > 1) {
                    speed = (parallaxHeight - scroll.offsetYStart + offsetYmin + spaceYend) /
                        scroll.height;
                }
            }
            if (Array.isArray(optionsSpeed)) {
                speed = Math.min(Math.max(speed, optionsSpeed[0]), optionsSpeed[1]);
            }
        }
        var scrollable = scroll.height * speed;
        if (options.reverseY) {
            parallaxHeight = viewHeight + scrollable;
            scroll.startY = 0;
        } else {
            scroll.startY = spaceYend * Math.min(speed, 1) - scrollable;
            if (speed > 1) {
                var fixTop = scroll.offsetYStart - offsetYmin;
                parallaxHeight = fixTop - scroll.startY;
            } else {
                if (displacementYMin === false) {
                    displacementYMin = calculateDisplacemenMin(offset.top, viewHeight, offsetYmin, offsetYmax);
                }
                parallaxHeight = viewHeight + displacementYMin * speed;
            }
        }
        scroll.speedY = speed;
        // Calculate cursor parallax effect.
        if (cursor.enabled) {
            cursor.setViewSize();
            // Calculate cursor Y-axis.
            speed = calculateCursorSpeed(
                cursor.speedY,
                context.naturalWidth, context.naturalHeight,
                parallaxWidth, parallaxHeight,
                overflow.xSize, overflow.ySize,
                cursor.viewHeight, viewHeight
            );
            cursor.extraY = (parallaxHeight + cursor.viewHeight - viewHeight) * speed;
            cursor.startY = options.cursorReverseY
                ? 0
                : cursor.extraY;
            parallaxHeight += cursor.extraY;
            cursor.speedY = speed;
            // Calculate cursor X-axis.
            speed = calculateCursorSpeed(
                cursor.speedX,
                context.naturalHeight, context.naturalWidth,
                parallaxHeight, parallaxWidth,
                overflow.ySize, overflow.xSize,
                cursor.viewWidth, viewWidth
            );
            cursor.extraX = parallaxWidth * speed + (cursor.viewWidth - viewWidth) * speed;
            cursor.startX = options.cursorReverseX
                ? 0
                : cursor.extraX;
            parallaxWidth += cursor.extraX;
            cursor.speedX = speed;
        }
        context.$parallax.css({
            height: parallaxHeight,
            width: parallaxWidth
        });
        context.parallaxHeight = parallaxHeight;
        context.parallaxWidth = parallaxWidth;
        return {
            height: parallaxHeight,
            width: parallaxWidth
        };
    };

    /**
     * Calculate cursor position.
     *
     * @param {SliceParallax} parallax - SliceParallax instance.
     * @param {string} positionType - Position type.
     * @param {object} defaultPosition - Default position object.
     * @returns {SliceParallax} Current SliceParallax instance.
     */
    function calculateCursorPosition (parallax, positionType, defaultPosition) {
        var cursorPosition,
            type = positionType,
            cursor = parallax.cursor;
        if (type === 'start') {
            type = parallax.options.cursorStart;
        }
        switch (type) {
        case 'center':
            var center = parallax.square.center;
            cursorPosition = {
                left: cursor.viewWidth *
                    (Math.max(cursor.multX, 0) - cursor.multX * center.x / parallax.parallaxWidth),
                top: cursor.viewHeight *
                    (Math.max(cursor.multY, 0) - cursor.multY * center.y / parallax.parallaxHeight)
            };
            break;
        case 'parallaxCenter':
            cursorPosition = {
                left: cursor.viewWidth / 2,
                top: cursor.viewHeight / 2
            };
            break;
        case 'none':
        default:
            cursorPosition = defaultPosition || {
                left: 0,
                top: 0
            };
            break;
        }
        return cursorPosition;
    }

    /**
     * Redraw.
     *
     * @returns {SliceParallax} Current SliceParallax instance.
     */
    SliceParallax.prototype.redraw = function () {
        var context = this,
            cursor = context.cursor;
        context.scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        context.scrollWidth = Math.max(
            document.body.scrollWidth, document.documentElement.scrollWidth,
            document.body.offsetWidth, document.documentElement.offsetWidth,
            document.body.clientWidth, document.documentElement.clientWidth
        );
        SliceCover.prototype.redraw.call(context);
        scroll.position = {
            left: window.scrollX || window.pageXOffset || 0,
            top: window.scrollY || window.pageYOffset || 0
        };
        context.buildLimits();
        if (cursor.enabled) {
            cursor.defaultPosition = calculateCursorPosition(context, context.options.cursorStart);
            if (!cursor.on) {
                cursor.position = cursor.defaultPosition;
            }
        }
        context.position(false, false, true);
        return context;
    };

    /**
     * Clear parallax animation delay.
     *
     * @returns {SliceParallax} Current SliceParallax instance.
     */
    SliceParallax.prototype.clearTick = function () {
        if (this.timeout) {
            clearTimeout(this.timeout);
            this.timeout = false;
        }
        return this;
    };

    var testVariables = {
        ge: function (val1, val2) {
            return val1 >= val2;
        },
        le: function (val1, val2) {
            return val1 <= val2;
        }
    };

    /**
     * Run parallax animation tick.
     *
     * @returns {SliceParallax} Current SliceParallax instance.
     */
    SliceParallax.prototype.tick = function () {
        var context = this,
            dest = context.dest,
            current = context.current,
            setNext = false,
            time = Date.now(),
            timeDiff = time - (context.lastTick || time);
        context.clearTick();
        context.lastTick = time;
        current.left = checkLimits(context, current.left + dest.leftMult * timeDiff * dest.leftStep, 'left');
        if (testVariables[dest.leftTest](current.left, dest.left)) {
            current.left = dest.left;
        } else {
            setNext = true;
        }
        current.top = checkLimits(context, current.top + dest.topMult * timeDiff * dest.topStep, 'top');
        if (testVariables[dest.topTest](current.top, dest.top)) {
            current.top = dest.top;
        } else {
            setNext = true;
        }
        context.$parallax.css(createParallaxCSS(current.left, current.top));
        if (setNext) {
            context.timeout = setTimeout(function () {
                context.timeout = false;
                context.tick();
            }, context.animation.interval);
        }
        return context;
    };

    /**
     * Move parallax element to coordinates.
     *
     * @param {number} rawLeft - Left coordinate.
     * @param {number} rawTop - Top coordinate.
     * @param {object|boolean} speedData - Speed data or true to run instantly.
     * @returns {SliceParallax} Current SliceParallax instance.
     */
    SliceParallax.prototype.move = function (rawLeft, rawTop, speedData) {
        var context = this,
            left = checkLimits(context, rawLeft, 'left'),
            top = checkLimits(context, rawTop, 'top'),
            dest = {
                left: left,
                top: top
            },
            runInstant = speedData === true;
        context.dest = dest;
        if (!runInstant) {
            var animation = $.extend({}, context.animation, speedData || {}),
                current = context.current,
                leftDistance = Math.abs(dest.left - current.left),
                leftDuration = Math.max(
                    Math.min(leftDistance / animation.step, animation.max),
                    animation.min
                ),
                topDistance = Math.abs(dest.top - current.top),
                topDuration = Math.max(
                    Math.min(topDistance / animation.step, animation.max),
                    animation.min
                ),
                maxDuration = Math.max(leftDuration, topDuration);
            if (maxDuration > animation.interval) {
                if (animation.sync) {
                    leftDuration = topDuration = maxDuration;
                }
                dest.leftStep = leftDistance / leftDuration;
                if (current.left > dest.left) {
                    dest.leftMult = -1;
                    dest.leftTest = 'le';
                } else {
                    dest.leftMult = 1;
                    dest.leftTest = 'ge';
                }
                dest.topStep = topDistance / topDuration;
                if (current.top > dest.top) {
                    dest.topMult = -1;
                    dest.topTest = 'le';
                } else {
                    dest.topMult = 1;
                    dest.topTest = 'ge';
                }
                if (!context.timeout) {
                    context.lastTick = Date.now();
                }
                context.tick();
            } else {
                runInstant = true;
            }
        }
        if (runInstant) {
            context.clearTick();
            context.current = {
                left: left,
                top: top
            };
            context.$parallax.css(createParallaxCSS(left, top));
        }
        return context;
    };

    /**
     * Set parallax position.
     *
     * @param {object} scrollDest - Scroll left, right positions.
     * @param {object} cursorDest - Cursor left, right positions.
     * @param {boolean} instant - Set position instantly.
     * @returns {SliceParallax} Current SliceParallax instance.
     */
    SliceParallax.prototype.position = function (scrollDest, cursorDest, instant) {
        var context = this;
        if (context.drawn) {
            var scroll = context.scroll,
                cursor = context.cursor,
                translateTop = Math.min(Math.max(scroll.position.top - scroll.offsetYStart, 0), scroll.height),
                top = scroll.startY + scroll.multY * translateTop * scroll.speedY,
                left = 0;
            if (scrollDest) {
                scroll.position = scrollDest;
            }
            if (cursorDest) {
                cursor.position = cursorDest;
            }
            if (cursor.enabled) {
                top -= cursor.startY - cursor.multY * cursor.speedY *
                    Math.max(Math.min(cursor.position.top, cursor.viewHeight), 0);
                left -= cursor.startX - cursor.multX * cursor.speedX *
                    Math.max(Math.min(cursor.position.left, cursor.viewWidth), 0);
            }
            context.move(left, top, instant);
        }
        return context;
    };

    /**
     * Extend jQuery with Slice Block Cover plugin.
     *
     * @param {object} [opts] - Options.
     * @returns {jQuery} Provided elements.
     */
    $.fn.sliceCover = function (opts) {
        var args = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
            var $el = $(this),
                cover = $el.data('slice.cover');
            if (!cover) {
                cover = new SliceCover($el, typeof opts === 'object' && opts);
            }
            if (typeof opts === 'string' && opts.charAt(0) !== '_') {
                cover[opts].apply(cover, args);
            }
        });
    };

    /**
     * Extend jQuery with Slice Block Parallax plugin.
     *
     * @param {object} [opts] - Options.
     * @returns {jQuery} Provided elements.
     */
    $.fn.sliceParallax = function (opts) {
        var args = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
            var $el = $(this),
                parallax = $el.data('slice.parallax');
            if (!parallax) {
                parallax = new SliceParallax($el, typeof opts === 'object' && opts);
            }
            if (typeof opts === 'string' && opts.charAt(0) !== '_') {
                parallax[opts].apply(parallax, args);
            }
        });
    };

    /**
     * Enable/Disable Slice Block Cover autoinit.
     */
    $.fn.sliceCover.autoInit = true;

    /**
     * Enable/Disable Slice Block Parallax autoinit.
     */
    $.fn.sliceParallax.autoInit = true;

    /**
     * Auto-init Slice Block Cover and Parallax.
     */
    $(function () {
        if ($.fn.sliceCover.autoInit) {
            $('img[data-slice-cover]').sliceCover();
        }
        if ($.fn.sliceParallax.autoInit) {
            $('img[data-slice-parallax]').sliceParallax();
        }
    });
}));
