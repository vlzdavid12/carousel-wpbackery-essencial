/* global jQuery, Slice */

/**
 * Slice Slider: Center View  Plugin.
 * Center mode works only in carousels.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Enable/disable centered view
        centerView: false
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRequirement = factory.addRequirement,
        removeRequirement = factory.removeRequirement,
        setRunnableType = factory.setRunnableType,
        addRunnable = factory.addRunnable;

    /**
     * Requirement for all workers and runnables.
     */
    var requirements = {
        centerView: true,
        single: false
    };

    /**
     * Requirements for some cases.
     */
    var tempRequirement;

    /**
     * Normalize slide position in carousel.
     *
     * @param {number} position - Position.
     * @param {number} last - Last position.
     * @param {number} lastCentered - Last centered position.
     * @param {number} displace - Slides displace.
     * @returns {number} Normalized position.
     */
    function normalizePosition (position, last, lastCentered, displace) {
        var normalized = position;
        normalized += displace;
        // Check if moved to far
        if (normalized >= last && position < lastCentered) {
            normalized = last - 1;
        }
        return normalized;
    }

    /**
     * Calculate last centered slide position.
     *
     * @param {number} start - Start position.
     * @param {number} last - Last position.
     * @param {number} step - Step.
     * @param {number} displacement - Position displacement.
     * @returns {number} Last centered slide position.
     */
    function calculateLastCentered (start, last, step, displacement) {
        var lastCentered = start +
                step * (Math.ceil((last - start) / step) - 1) +
                displacement + 1;
        if (lastCentered > last) {
            lastCentered = last;
        }
        return lastCentered;
    }

    /**
     * Center plugin constructor.
     *
     * @class SliceCenter
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceCenter (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceCenter.prototype.setup = function () {
        var settings = this.slice.settings;
        if (settings.centerView) {
            settings.workers.centerView = true;
            settings.workers.displace = true;
        }
    };

    /**
     * Add workers and runnables for adaptive mode only.
     */
    addRequirement(requirements);

    /**
     * Calculate slides position displacements.
     */
    addWorker('slides, settings', function (stage) {
        stage.displaceActive = Math.ceil(((stage.slidesToShow || 1) - 1) / 2);
    }, 10, 'fixedslides');
    addWorker('slides, settings', function (stage) {
        stage.displacePosition = Math.ceil(((stage.slidesStep || 1) - 1) / 2);
    }, 10);

    /**
     * Adjust minimum slides length.
     */
    addWorker('edges, slides, settings', function (stage) {
        stage.minSlides -= stage.displaceActive;
    }, 20, 'fixedslides, !showFit');

    /**
     * Adjust clones count.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        cache.clonesCount += stage.workSpace / 2 / cache.slideSize;
    }, 50, 'fixedsize, loop, !showFit');

    /**
     * Adjust slides coordinates displacement.
     */
    addWorker('size, slides, settings', function (stage, cache) {
        stage.displaceFirst -= (stage.viewSize - cache.slideSize) / 2;
    }, 81, 'fixedsize');

    /**
     * Add workers only for fixed size slides.
     * Adjust start and last slide positions.
     */
    tempRequirement = 'fixedslides';
    addRequirement(tempRequirement);
    addWorker('edges, slides, settings', function (stage) {
        stage.lastSlide += stage.displacePosition;
    }, 101, '!showFit');
    addWorker('edges, slides, settings', function (stage) {
        stage.startSlide += stage.displaceActive;
        stage.lastSlide += stage.displaceActive;
    }, 101, '!loop, showFit');
    addWorker('edges, slides, settings', function (stage) {
        stage.startSlide += stage.displacePosition;
        stage.lastSlide += stage.displacePosition;
    }, 101, 'loop, showFit');
    removeRequirement(tempRequirement);

    /**
     * Calculate last centered slide position.
     */
    addWorker('edges, slides, settings', function (stage) {
        stage.lastSlideCentered = calculateLastCentered(
            stage.startSlide, stage.lastSlide,
            stage.slidesStep, stage.displacePosition
        );
    }, 111);

    /**
     * Adjust start, last dots positions.
     */
    addWorker('edges, slides, settings', function (stage, cache) {
        stage.dotsDisplace = Math.floor(((cache.dotsStep || 1) - 1) / 2);
        cache.dotsLast += stage.dotsDisplace;
    }, 111, 'dots');
    addWorker('edges, slides, settings', function (stage, cache) {
        cache.dotsStart += stage.dotsDisplace;
    }, 112, 'dots, showFit');

    /**
     * Calculate last centered dot position.
     */
    addWorker('edges, slides, settings', function (stage) {
        if (this.settings.dotsSlideToScroll) {
            stage.dotsLastCentered = calculateLastCentered(
                stage.dotsStart, stage.dotsLast,
                stage.dotsStep, stage.dotsDisplace
            );
        } else {
            stage.dotsLastCentered = stage.lastSlideCentered;
        }
    }, 130, 'dots');

    /**
     * Get slide position runnables.
     * Normalize position.
     */
    setRunnableType('position');
    addRunnable('precise', function (stage, cache) {
        cache.normalized = normalizePosition(
            cache.normalized, stage.lastSlide,
            stage.lastSlideCentered, stage.displacePosition
        );
    }, 59);

    /**
     * Set slide position runnables.
     */
    setRunnableType('setPosition');

    /**
     * Adjust active slides start and end positions.
     */
    addRunnable('current, extraCurrent', function (stage, cache) {
        cache.start -= stage.displaceActive;
        cache.end -= stage.displaceActive;
        if (cache.start < 1) {
            cache.start = 1;
        }
    }, 25, 'fixedslides');

    /**
     * Normalize active dot position.
     */
    addRunnable('current', function (stage, cache) {
        cache.activeDot = normalizePosition(
            cache.activeDot, stage.dotsLast,
            stage.dotsLastCentered, stage.dotsDisplace
        );
    }, 61, 'dots');

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    removeRequirement(requirements);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceCenter = SliceCenter;
}));
