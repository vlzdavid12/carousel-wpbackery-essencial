/* global jQuery, Slice */

/**
 * Slice Slider: Navigation Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Arrow navigation class
        arrowClass: 'slice-arrow',
        // Enable/disable arrows navigation
        arrows: false,
        // Arrows navigation container element
        arrowsContainer: false,
        // Put arrows at some slider layer
        arrowsLayer: false,
        // Number of slides to scroll, when set to false will scroll default ammount of slides
        arrowsSlidesToScroll: false,
        // Dot navigation element
        dotElement: '<a href="#" class="slice-dot"></a>',
        // Enable/disable dots navigation
        dots: false,
        // Dots navigation container element
        dotsContainer: '<div class="slice-dots"></div>',
        // Put arrows at some slider layer
        dotsLayer: false,
        // Dots step, when set to false than scroll default ammount of slides
        dotsSlidesToScroll: false,
        // Slider navigation class
        navClass: false,
        // 'Next' arrow element
        nextArrow: '<a href="#" class="slice-next">&rsaquo;</a>',
        // 'Previous' arrow element
        prevArrow: '<a href="#" class="slice-prev">&lsaquo;</a>'
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        addRunnable = factory.addRunnable,
        setRunnableType = factory.setRunnableType,
        addRequirement = factory.addRequirement,
        removeRequirement = factory.removeRequirement;

    /**
     * Requirements for some cases.
     */
    var tempRequirement;

    /**
     * Normalize slide position.
     *
     * @param {number} position - Position.
     * @param {number} start - Start position.
     * @param {number} end - End position.
     * @param {number} step - Slides step.
     * @returns {number} - Normalized position.
     */
    function normalizePosition (position, start, end, step) {
        if (position >= end) {
            return end;
        } else if (position <= start) {
            return start;
        }
        var checkPosition = position > start
                ? position - (start - 1)
                : 1,
            pagePosition = start + step * (Math.ceil(checkPosition / step) - 1);
        if (pagePosition > end) {
            pagePosition = end;
        } else if (pagePosition < start) {
            pagePosition = start;
        }
        return pagePosition;
    }

    /**
     * Navigation plugin constructor.
     *
     * @class SliceNavigation
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceNavigation (slice) {
        this.slice = slice;
        this.cache = {};
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceNavigation.prototype.setup = function () {
        var context = this,
            settings = context.slice.settings;
        // If no arrows - disable arrow check worker.
        settings.workers.arrows = !!settings.arrows;
        // If no dots navigation - disable dots navigation check worker.
        settings.workers.dots = !!settings.dots;
        if (settings.navClass) {
            context.slice.stage.sliderClasses.push(settings.navClass);
        }
    };

    /**
     * Show/hide navigation arrows.
     *
     * @param {boolean} value - Enable/disable navigation arrows.
     */
    SliceNavigation.prototype.enableArrows = function (value) {
        var context = this,
            stage = context.slice.stage,
            hiddenClass = context.slice.settings.hiddenClass;
        context.checkArrows(value);
        if (stage.$arrowsContainer) {
            stage.$arrowsContainer.toggleClass(hiddenClass, !value);
        }
        if (stage.$prevArrow) {
            stage.$prevArrow.toggleClass(hiddenClass, !value);
        }
        if (stage.$nextArrow) {
            stage.$nextArrow.toggleClass(hiddenClass, !value);
        }
    };

    /**
     * Check element by 'prop' settings and create it if needed.
     *
     * @param {string} prop - Property name to check.
     * @param {SliceNavigation} navigation - The SliceNavigation instance.
     * @param {boolean} create - Forcefuly create container.
     * @param {jQuery} $container - Container element, if any.
     * @returns {boolean} Has element.
     */
    function checkElement (prop, navigation, create, $container) {
        var $prop = '$' + prop,
            slice = navigation.slice,
            settings = slice.settings,
            apply = false,
            cache = false,
            $element;
        // Hide curent element
        if (navigation[$prop]) {
            navigation[$prop].addClass(settings.hiddenClass);
            navigation[$prop] = null;
        }
        if (!settings[prop]) {
            return false;
        }
        // Check cache.
        if (navigation.cache[prop]) {
            for (var i = 0; i < navigation.cache[prop].length; i++) {
                if (navigation.cache[prop][i].value === settings[prop]) {
                    $element = navigation.cache[prop][i].$element;
                    apply = cache = true;
                }
            }
        }
        // Check and create if needed.
        if (cache) {
            ($container || navigation.slice.$element).append($element);
        } else {
            $element = $(settings[prop]);
            if (!(apply = $('body').find($element).length) && create) {
                ($container || navigation.slice.$element).append($element);
            }
        }
        // Store element if created.
        if (apply || create) {
            // Add to cache.
            if (!cache) {
                if (!navigation.cache[prop]) {
                    navigation.cache[prop] = [];
                }
                navigation.cache[prop].push({
                    $element: $element,
                    value: settings[prop]
                });
            }
            navigation[$prop] = $element;
            slice.stage[$prop] = $element;
            return $element;
        }
    }

    /**
     * Check arrow element by 'prop' settings and create it if needed.
     *
     * @param {string} prop - Property name to check.
     * @param {SliceNavigation} navigation - The SliceNavigation instance.
     * @param {boolean} create - Forcefuly create.
     * @param {Function} handler - Click event handler.
     */
    function checkArrow (prop, navigation, create, handler) {
        var slice = navigation.slice,
            container = slice.stage.$arrowsContainer && slice.stage.$arrowsContainer.length
                ? slice.stage.$arrowsContainer
                : slice.settings.arrowsLayer
                    ? slice.layers[slice.settings.arrowsLayer].$element
                    : false;
        if (checkElement(prop, navigation, create, container)) {
            slice.stage['$' + prop]
                .addClass(slice.settings.arrowClass)
                .data('slice.slider', slice)
                .off('.slice.navigation')
                .on('click.slice.navigation', handler);
        }
    }

    /**
     * Check navigation arrows settings and create navigation arrows if needed.
     *
     * @param {boolean} create - Forcefuly create navigation arrows.
     */
    SliceNavigation.prototype.checkArrows = function (create) {
        var context = this,
            slice = context.slice,
            settings = slice.settings;
        if (!settings) {
            return;
        }
        if (settings.arrowsContainer === true) {
            settings.arrowsContainer = '<div class="slice-arrows"/>';
        }
        // Check arrows container
        checkElement('arrowsContainer', context, create, settings.arrowsLayer
            ? slice.layers[settings.arrowsLayer].$element
            : false);
        // Check previous arrow
        checkArrow('prevArrow', context, create, function (e) {
            e.preventDefault();
            var $el = $(this);
            if (!$el.hasClass(settings.disabledClass)) {
                var slider = $el.data('slice.slider');
                slider.run('userActive');
                if (slider.settings.arrowsSlidesToScroll) {
                    slider.to(slider.currentPosition - slider.settings.arrowsSlidesToScroll);
                } else {
                    slider.prev();
                }
            }
        });
        // Check next arrow
        checkArrow('nextArrow', context, create, function (e) {
            e.preventDefault();
            var $el = $(this);
            if (!$el.hasClass(settings.disabledClass)) {
                var slider = $el.data('slice.slider');
                slider.run('userActive');
                if (slider.settings.arrowsSlidesToScroll) {
                    slider.to(slider.currentPosition + slider.settings.arrowsSlidesToScroll);
                } else {
                    slider.next();
                }
            }
        });
    };

    /**
     * Show/hide dots navigation.
     *
     * @param {boolean} value - Enable/disable dots navigation.
     */
    SliceNavigation.prototype.enableDots = function (value) {
        var context = this,
            stage = context.slice.stage;
        context.checkDots(value);
        if (stage.$dotsContainer) {
            stage.$dotsContainer.toggleClass(context.slice.settings.hiddenClass, !value);
        }
    };

    /**
     * Check dots navigation settings and create dots navigation if needed.
     *
     * @param {boolean} create - Forcefuly create dots navigation.
     */
    SliceNavigation.prototype.checkDots = function (create) {
        var context = this,
            slice = context.slice,
            settings = slice.settings,
            stage = slice.stage;
        checkElement('dotsContainer', context, create, settings.dotsLayer
            ? slice.layers[settings.dotsLayer].$element
            : false);
        if (stage.$dotsContainer) {
            stage.$dotsContainer.data('slice.slider', context.slice);
            stage.$dotElement = $(settings.dotElement).detach();
        }
    };

    /**
     * Add workers.
     *
     * Set navigations layers.
     */
    addWorker('settings', function (stage, cache) {
        cache.layers = cache.layers || {};
        if (this.settings.arrowsLayer) {
            cache.layers['slice-arrows-layer'] = this.settings.arrowsLayer;
        }
        if (this.settings.dotsLayer) {
            cache.layers['slice-dots-layer'] = this.settings.dotsLayer;
        }
    }, -10);

    /**
     * Enable/disable navigation.
     */
    addWorker('slides, settings', function () {
        var plugin = this.plugins.sliceNavigation;
        plugin.enableArrows(this.settings.arrows);
        plugin.enableDots(this.settings.dots);
    });

    /**
     * Add workers and runnables for arrows navigation only.
     */
    tempRequirement = 'arrows';
    addRequirement(tempRequirement);

    /**
     * Add arrows to navigation elements list.
     * Disable arrows if not enought slides.
     */
    addWorker('edges, slides, settings', function (stage) {
        var cssClass = this.settings.disabledClass,
            isDisabled = !stage.enoughSlides;
        stage.$nav = stage.$nav || $([]);
        if (stage.$arrowsContainer) {
            stage.$arrowsContainer.toggleClass(cssClass, isDisabled);
        }
        if (stage.$prevArrow) {
            stage.$nav = stage.$nav.add(stage.$prevArrow);
            stage.$prevArrow.toggleClass(cssClass, isDisabled);
        }
        if (stage.$nextArrow) {
            stage.$nav = stage.$nav.add(stage.$nextArrow);
            stage.$nextArrow.toggleClass(cssClass, isDisabled);
        }
    }, 90);

    /**
     * Set slide position runnables.
     * Disable previous/next arrow if reached start/end.
     */
    setRunnableType('setPosition');
    addRunnable('current', function (stage) {
        var context = this,
            settings = context.settings,
            arrowsSlidesToScroll = settings.arrowsSlidesToScroll;
        if (!stage.enoughSlides) {
            return;
        }
        if (stage.$prevArrow) {
            stage.$prevArrow.toggleClass(settings.disabledClass, arrowsSlidesToScroll
                ? context.currentPosition - arrowsSlidesToScroll <= 0
                : stage.isFirstSlide);
        }
        if (stage.$nextArrow) {
            stage.$nextArrow.toggleClass(settings.disabledClass, arrowsSlidesToScroll
                ? stage.slidesCount < context.currentPosition + arrowsSlidesToScroll
                : stage.isLastSlide);
        }
    }, 60, 'checkSides');
    removeRequirement(tempRequirement);

    /**
     * Add workers and runnables for dots navigation only.
     */
    addRequirement('dots');

    /**
     * Add dots container to navigation elements.
     */
    addWorker('settings', function (stage) {
        stage.$nav = (stage.$nav || $([])).add(stage.$dotsContainer);
    }, 90);

    /**
     * Set dots step, start and last position.
     */
    addWorker('edges, slides, settings', function (stage, cache) {
        var context = this;
        cache.dotsStep = context.settings.dotsSlidesToScroll || stage.slidesStep || 1;
        cache.dotsStart = 1;
        cache.dotsLast = stage.slidesCount;
    }, 110);

    /**
     * Check dots start and last position.
     */
    addWorker('edges, slides, settings', function (stage, cache) {
        if (this.settings.dotsSlidesToScroll) {
            cache.dotsLast = cache.dotsStart +
                cache.dotsStep * (Math.ceil((cache.dotsLast - cache.dotsStart + 1) / cache.dotsStep) - 1);
        } else {
            cache.dotsStart = stage.startSlide;
            cache.dotsLast = stage.lastSlide;
        }
        if (cache.dotsStart < 1) {
            cache.dotsStart = 1;
        }
        if (cache.dotsLast > stage.slidesCount) {
            cache.dotsLast = stage.slidesCount;
        }
        stage.dotsStep = cache.dotsStep;
        stage.dotsStart = cache.dotsStart;
        stage.dotsLast = cache.dotsLast;
    }, 120);

    /**
     * Create and add dots elements to dots container.
     */
    addWorker('edges, slides, settings', function (stage) {
        var context = this,
            $dot = stage.$dotElement,
            cssClass = this.settings.disabledClass,
            isDisabled = !stage.enoughSlides;

        /**
         * Dot click event.
         *
         * @param {event} event - Click event.
         */
        function handler (event) {
            event.preventDefault();
            var $el = $(this);
            if (!$el.hasClass(context.settings.currentClass)) {
                var slice = $el.data('slice.slider');
                slice.run('userActive');
                slice.to($el.data('sliceIndex'));
            }
        }

        // Remove old dots elements
        stage.$dotsContainer
            .toggleClass(cssClass, isDisabled)
            .empty();
        // Create and append dots.
        for (var i = stage.dotsStart; i < stage.dotsLast; i += stage.dotsStep) {
            $dot.clone(true, true)
                .attr('data-slice-index', i)
                .data('slice.slider', context)
                .on('click.slice.navigation', handler)
                .appendTo(stage.$dotsContainer);
        }
        $dot.clone(true, true)
            .attr('data-slice-index', stage.dotsLast)
            .data('slice.slider', context)
            .on('click.slice.navigation', handler)
            .appendTo(stage.$dotsContainer);
    }, 130);

    /**
     * Set slide position runnables.
     * Calculate current slide dot position and add current class to it.
     */
    setRunnableType('setPosition');
    addRunnable('current', function (stage, cache) {
        cache.activeDot = this.currentPosition;
    }, 60);
    addRunnable('current', function (stage, cache) {
        cache.activeDot = normalizePosition(cache.activeDot, stage.dotsStart, stage.dotsLast, stage.dotsStep);
        stage.$dotsContainer.find('.' + this.settings.currentClass)
            .removeClass(this.settings.currentClass)
            .trigger('blur');
        stage.$dotsContainer.find('[data-slice-index="' + cache.activeDot + '"]')
            .addClass(this.settings.currentClass);
    }, 70);

    /**
     * Cleanup.
     */
    removeRequirement('dots');

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceNavigation = SliceNavigation;
}));
