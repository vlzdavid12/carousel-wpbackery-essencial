/* global jQuery, Slice */

/**
 * Slice Slider: Drag Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';

    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery, window, document);
}(function ($, window, document) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Enable/disable stage slides move while dragging.
        dragSlide: true,
        // Drag speed, custom move speed after dragging.
        dragSpeed: false,
        // Drag distance in pixels, that will be taken with speedType = 'uniform' or 'uniformFixed'.
        dragSpeedBase: false,
        // Drag speed type, i.e. base distance to calculate speed, check available types in documentation.
        dragSpeedType: false,
        // Threshold to advance slides, i.e. user should drag a distance of (1/dragThreshold) * the size of the slide.
        dragThreshold: 0.25,
        // Type of treeshold: slide, static, viewport.
        dragThresholdType: 'slide',
        // Allow users to drag directly to a slide, when disabled will drag to next, previous slide only
        dragToSlide: true,
        // Enable/disable dragging.
        draggable: true,
        // Resistance when swiping edges in non-loop slider.
        edgeFriction: 0.15,
        // Enable/disable free drag, i.e. stay at the dragged position, requires 'dragSlide' to be enabled.
        freeDrag: false,
        // Enable/disable applying of slide status, i.e. current|active slides classes, etc.
        liveDrag: false,
        // Enabled/disabled mouse dragging.
        mouseDrag: true,
        // Enabled/disabled touch dragging.
        touchDrag: true
    };

    /**
     * Quick access.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addRunnable = factory.addRunnable,
        setRunnableType = factory.setRunnableType,
        setQuickRun = factory.setQuickRun;

    /**
     * Drag plugin constructor.
     *
     * @class SliceDrag
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceDrag (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
        slice.state.add('dragging', 'busy, hold');
    }

    /**
     * Setup settings.
     */
    SliceDrag.prototype.setup = function () {
        var settings = this.slice.settings,
            workers = settings.workers;
        if (settings.draggable) {
            workers.drag = true;
            workers.mouseDrag = !!settings.mouseDrag;
            workers.touchDrag = !!settings.touchDrag;
            workers.dragToSlide = !!settings.dragToSlide;
            workers.dragSlide = !!settings.dragSlide;
            workers.freeDrag = !!settings.freeDrag && workers.dragSlide;
            settings.dragKeys = ['stageMove', 'edgeFriction'];
            var key;
            switch (settings.dragThresholdType) {
            case 'static':
                key = 'preciseStatic';
                break;
            case 'viewport':
                key = 'preciseViewport';
                break;
            default:
                key = 'preciseSlide';
                break;
            }
            settings.dragTreesholdKey = key;
            if (settings.liveDrag === 'auto' && workers.dragSlide) {
                workers.liveDrag = true;
            } else {
                workers.liveDrag = !!settings.liveDrag;
            }
            if (workers.liveDrag) {
                settings.dragKeys.push('apply');
            }
            if (workers.freeDrag) {
                settings.dragKeys.push('final');
            }
        }
    };

    /**
     * Get touch data.
     *
     * @param {event} event - Touch event object.
     * @returns {object} Touch data.
     */
    function getTouchData (event) {
        return event.touches && event.touches.length
            ? event.touches[0]
            : event.changedTouches && event.changedTouches.length
                ? event.changedTouches[0]
                : event;
    }

    /**
     * Prevent event default.
     *
     * @param {event} event - Event object.
     */
    function preventDefault (event) {
        var e = event || window.event;
        if (e.preventDefault) {
            e.preventDefault();
        }
        e.returnValue = false;
    }

    /**
     * Add workers.
     *
     * Bind mouse and touch drag events.
     */
    factory.addWorker('slides, settings', function () {
        var context = this,
            settings = context.settings,
            $stage = context.$stage;
        if (context.state.is('dragging')) {
            context.state.leave('dragging');
        }
        document.removeEventListener('touchmove', preventDefault, {passive: false});
        context.$viewport.off('.slice.dragging');
        $stage.off('.drag .dragging');
        if (settings.mouseDrag) {
            $stage.on('mousedown.slice.drag', function (e) {
                this.run('mouseStart', {event: e});
            }.bind(context));
        }
        if (settings.touchDrag) {
            $stage.on('touchstart.slice.drag', function (e) {
                this.run('touchStart', {event: e});
            }.bind(context));
        }
    }, 90, 'drag');

    /**
     * Move stage runnables.
     * Add edge friction.
     */
    setRunnableType('move');
    addRunnable('edgeFriction', function (stage, cache) {
        var distance = cache.moveDistance,
            moveCoord = cache.moveCoordinate,
            start = cache.list.apply
                ? stage.coordinates[stage.startSlide] - stage.displacement
                : stage.startCoordinate,
            end = cache.list.apply
                ? stage.coordinates[stage.lastSlide] - stage.displacement
                : stage.endCoordinate;
        if (moveCoord >= start && distance < 0 ||
            moveCoord <= end && distance > 0) {
            var currentCoord = cache.currentCoord,
                toEnd = moveCoord >= start && distance < 0
                    ? Math.min(currentCoord - start, 0)
                    : Math.max(currentCoord - end, 0);
            distance = toEnd + (distance - toEnd) * cache.edgeFriction;
            cache.moveCoordinate = currentCoord - distance;
        }
    }, 29, '!loop');

    /**
     * Drag runnables.
     */
    setRunnableType('dragState');

    // Mouse: start dragging
    setQuickRun('mouseStart', 'mouseStart, mouse, start');
    // Mouse: move stage
    setQuickRun('mouseMove', 'mouseMove, mouse, move');
    // Mouse: end dragging
    setQuickRun('mouseEnd', 'mouseEnd, mouse, end');
    // Touch: start dragging
    setQuickRun('touchStart', 'touchStart, touch, start');
    // Touch: move stage
    setQuickRun('touchMove', 'touchMove, touch, move');
    // Touch: end dragging
    setQuickRun('touchEnd', 'touchEnd, touch, end');

    /**
     * Don't start dragging if slider is busy.
     */
    addRunnable('start', function () {
        if (this.state.is('busy')) {
            return false;
        }
    });

    /**
     * Mark that slider was moved.
     */
    addRunnable('move', function (stage) {
        stage.stageMoves = true;
        stage.skipFocus = true;
    }, 10);

    /**
     * Set coordinates.
     */
    addRunnable(true, function (stage, cache) {
        cache.x = cache.x || 0;
        cache.y = cache.y || 0;
    });

    /**
     * Handle mouse and touch events.
     */
    addRunnable('mouse', function (stage, cache) {
        if (cache.event) {
            cache.x = event.clientX;
            cache.y = event.clientY;
        }
    }, 10, 'mouseDrag');
    addRunnable('touch', function (stage, cache) {
        if (cache.event) {
            var touch = getTouchData(event);
            cache.x = touch.pageX;
            cache.y = touch.pageY;
        }
    }, 10, 'touchDrag');

    /**
     * Move stage.
     */
    addRunnable('move', function (stage, cache) {
        this.run(this.settings.dragKeys, {
            distance: {
                left: stage.dragDistanceX - cache.x,
                top: stage.dragDistanceY - cache.y
            },
            edgeFriction: this.settings.edgeFriction
        }, 'move');
        stage.dragDistanceX = cache.x;
        stage.dragDistanceY = cache.y;
    }, 20, 'dragSlide');

    /**
     * If moving stage than don't go further.
     */
    addRunnable('move', function () {
        return false;
    }, 30);

    /**
     * Cleanup events after ending dragging.
     * Leave dragging state.
     */
    addRunnable('touchEnd', function () {
        document.removeEventListener('touchmove', preventDefault, {passive: false});
    }, 40, 'touchDrag');
    addRunnable('end', function () {
        this.$stage.off('.slice.dragging');
        this.$viewport.off('.slice.dragging');
        $(window).off('.slice.dragging');
        this.state.leave('dragging');
    }, 50);

    /**
     * Do nothing if stage didnt moved.
     * Calculate dragged distance.
     */
    addRunnable('end', function (stage, cache) {
        if (!stage.stageMoves) {
            return false;
        }
        stage.dragX -= cache.x;
        stage.dragY -= cache.y;
        cache.threshold = this.settings.dragThreshold;
        cache.coordinate = stage.getPosition(false, {
            left: stage.dragX,
            top: stage.dragY
        });
    }, 60);

    /**
     * When freeDrag is enabled don't go further.
     */
    addRunnable('end', function (stage, cache) {
        var coordinate = stage.coordinate - cache.coordinate - stage.displacement;
        if (coordinate > stage.startCoordinate) {
            cache.moveToSlide = stage.startSlide;
        } else if (coordinate < stage.endCoordinate) {
            cache.moveToSlide = stage.lastSlide;
        }
    }, 70, 'freeDrag, !loop');

    /**
     * Calculate slide position.
     */
    addRunnable('end', function (stage, cache) {
        var runnable = [
                'normalize',
                this.settings.dragTreesholdKey,
                cache.coordinate > 0
                    ? 'next'
                    : 'prev'
            ],
            coordinate = this.settings.workers.dragSlide
                ? stage.getPosition(this.$stage)
                : stage.coordinate - cache.coordinate;
        cache.moveToSlide = this.run(runnable, {
            coordinate: coordinate,
            precise: cache.threshold
        }, 'closest').normalized;
        if (stage.position === cache.moveToSlide) {
            cache.moveToSlide = this.currentPosition;
        }
    }, 70, '!freeDrag');
    addRunnable('end', function (stage, cache) {
        if (cache.moveToSlide > this.currentPosition) {
            cache.moveToSlide = this.run('getNextStrict');
        } else if (cache.moveToSlide < this.currentPosition) {
            cache.moveToSlide = this.run('getPrevStrict');
        }
    }, 70, '!dragToSlide, !freeDrag');

    /**
     * Calculate speed.
     */
    addRunnable('end', function (stage, cache) {
        if (Slice.hasProperty(cache, 'moveToSlide')) {
            var settings = this.settings,
                speedData = this.run(['to', settings.dragSpeedType || settings.speedType], {
                    baseSpeed: settings.dragSpeed || settings.speed,
                    speedBase: settings.dragSpeedBase || settings.speedBase,
                    to: cache.moveToSlide
                }, 'speed');
            cache.speed = speedData.speed;
        }
    }, 70);

    /**
     * Move to slide.
     */
    addRunnable('end', function (stage, cache) {
        if (Slice.hasProperty(cache, 'moveToSlide')) {
            this.to(cache.moveToSlide, cache.speed);
        }
    }, 80, ['!freeDrag', 'freeDrag, liveDrag']);
    addRunnable('end', function (stage, cache) {
        if (Slice.hasProperty(cache, 'moveToSlide')) {
            this.run('position', {
                position: cache.moveToSlide,
                speed: cache.speed
            }, 'move');
        }
    }, 80, 'freeDrag, !liveDrag, !loop');

    /**
     * Store base data when starting dragging.
     * Enter dragging state.
     */
    addRunnable('start', function (stage, cache) {
        stage.dragX = cache.x;
        stage.dragY = cache.y;
        stage.dragDistanceX = cache.x;
        stage.dragDistanceY = cache.y;
        stage.stageMoves = false;
        stage.coordinate = stage.getPosition(this.$stage);
        this.state.enter('dragging');
    }, 90);

    /**
     * Bind events.
     */
    addRunnable('mouseStart', function () {
        $(window)
            .on('mousemove.slice.dragging', function (e) {
                this.run('mouseMove', {event: e});
            }.bind(this))
            .on('mouseup.slice.dragging', function (e) {
                this.run('mouseEnd', {event: e});
            }.bind(this));
    }, 100, 'mouseDrag');
    addRunnable('touchStart', function () {
        $(window)
            .on('touchmove.slice.dragging', function (e) {
                this.run('touchMove', {event: e});
            }.bind(this))
            .on('touchend.slice.dragging', function (e) {
                this.run('touchEnd', {event: e});
            }.bind(this));
        document.addEventListener('touchmove', preventDefault, {passive: false});
    }, 100, 'touchDrag');

    /**
     * Cleanup.
     */
    factory.resetRunnableType();

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceDrag = SliceDrag;
}));
