/* global jQuery, Slice */

/**
 * Slice Slider: Vertical Plugin.
 *
 * @author shininglab
 * @typedef {object} SliceSlider Slice slider class instance
 */

(function (fn) {
    'use strict';
    if (typeof jQuery === 'undefined') {
        throw new Error('Requires jQuery to be loaded first');
    }
    fn(jQuery);
}(function ($) {
    'use strict';

    /**
     * Plugin default options.
     */
    var pluginDefaults = {
        // Vertical slide direction.
        vertical: false,
        // Slider class for vertical mode.
        verticalClass: 'slice-slider-vertical'
    };

    /**
     * Quick access functions.
     */
    var fnSlice = $.fn.sliceSlider,
        factory = Slice.Slider.factory,
        addWorker = factory.addWorker,
        cssProps = fnSlice.cssProps,
        addRunnable = factory.addRunnable;

    /**
     * Requirement for all workers and runnables.
     */
    var requirement = 'vertical';

    /**
     * Vertical plugin constructor.
     *
     * @class SliceVerical
     * @param {SliceSlider} slice - The Slice slider instance.
     */
    function SliceVerical (slice) {
        this.slice = slice;
        $.extend(slice.defaults, pluginDefaults);
    }

    /**
     * Setup settings.
     */
    SliceVerical.prototype.setup = function () {
        var settings = this.slice.settings;
        this.slice.$element.toggleClass(settings.verticalClass, settings.vertical);
        settings.workers.vertical = !!settings.vertical;
        settings.workers.horizontal = !settings.vertical;
    };

    /**
     * Add workers and runnables for vertical mode only.
     */
    factory.addRequirement(requirement);

    /**
     * Set dimensions and functions for it.
     */
    addWorker('settings', function (stage) {
        stage.revSizeProp = cssProps.width;
        stage.sizeProp = cssProps.height;
        stage.spacingProps = ['marginTop', 'marginBottom'];
        stage.getSize = function ($el) {
            return $el.innerHeight();
        };
        stage.setSize = function ($el, size) {
            $el.innerHeight(size);
        };
        stage.positionProp = 'top';
        stage.getPosition = function ($el, position) {
            if (position) {
                return typeof position === 'object'
                    ? position.top || 0
                    : position;
            }
            return $el.position().top;
        };
    });

    /**
     * Set viewport and view size (height).
     */
    addWorker('size, slides, settings', function (stage, cache) {
        this.$viewport[cssProps.height](this.run('getViewMaxHeight'));
        stage.viewportSize = this.$viewport[cssProps.height]();
        cache.viewSize = stage.viewportSize;
    }, 10, '!fixedheight');

    /**
     * Set viewport width.
     */
    addWorker('size, slides, settings', function (stage) {
        stage.viewportWidth = this.$viewport[cssProps.width]();
    }, 81);

    /**
     * Check if need to resize slider.
     */
    addWorker(true, function (stage, cache) {
        cache.resizeRequired = cache.resizeRequired ||
            Math.abs(stage.viewportWidth - this.$viewport[cssProps.width]()) > 1;
    }, 197);

    /**
     * Resize runnables.
     */
    factory.setRunnableType('resize');

    /**
     * Reset viewport element height.
     */
    addRunnable('check', function () {
        this.$viewport[cssProps.height]('');
    }, -10, '!fixedheight');

    /**
     * If dont need to run resize - set viewport element height to current value.
     */
    addRunnable('check', function (stage, cache) {
        if (!cache.result) {
            this.$viewport[cssProps.height](stage.viewportSize);
        }
    }, 10, '!fixedheight');

    /**
     * Cleanup.
     */
    factory.resetRunnableType();
    factory.removeRequirement(requirement);

    /**
     * Add plugin.
     */
    fnSlice.Plugins.SliceVerical = SliceVerical;
}));
